var fs = require('fs'),
    crypto = require('crypto');

exports.readFile = function(name, type) {
    var fileDef = g.Q.defer();

    fs.readFile(name, type, function(err, chunk) {
        if( err ) {
            console.log(g.color.red('Can\'t read '+ name +' file'));
            fileDef.resolve(null);
        } else {
            fileDef.resolve(type === 'binary' ? chunk : chunk.toString());
        }
    });

    return fileDef.promise;
}

exports.md5 = function(data) {
    return crypto.createHash('md5').update(data).digest('hex');
}
