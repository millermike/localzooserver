var num,
    maxRange = 10;

exports.generate = function(number) {
    var arr = [],
        stop = false;

    while( arr.length < number ) {
        num = Math.random() * maxRange >> 0;

        if( arr.indexOf(num) === -1 ) {
            arr.push(num);
        }
    }

    return arr;
}
