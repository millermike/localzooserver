/*
 **************   BASE CLASSES ****************
 */


/**
 * Class name method
 */
ClassName = function(name,parent){
  if( parent == undefined ) parent = document;
  var el = null;
  if ( parent.getElementsByClassName != undefined){
    el = parent.getElementsByClassName(name);
  }else{		
      var parent = parent || document,
      list = parent.getElementsByTagName('*'), 
      length = list.length,  
      classArray = name.split(/\s+/), 
      classes = classArray.length, 
      result = [], i,j;
      
      for(i = 0; i < length; i++) {
        for(j = 0; j < classes; j++)  {
          if(list[i].className.search('\\b' + classArray[j] + '\\b') != -1) {
            result.push(list[i]);
            break;
          };
        };
      };
      el =  result;
  };
  return el;
};


/**
 * Create Element
 */
CreateElement = function(obj){
  var font_size_option = "size-option";

  var el = document.createElement(obj.type);
  
  if( obj.class_ != undefined ) el.className = obj.class_;
    
  if( obj.id != undefined ) el.id = obj.id;

  if( obj.text != undefined ) el.innerHTML = obj.text;

  if( obj.type == "select" ){
    for( var j = 0; j < obj.options.length; j++ ){
      new CreateElement({
        type:"option",
        class_:font_size_option,
        text:obj.options[j],
        parent:el
      });
    };
  };

  if( obj.type == "input" ) el.type = obj.inputType;

  if( obj.parent != undefined ) obj.parent.appendChild(el);

  return el;
};


/**
 * AJAX Request
 */
AjaxConnect = function(obj) {
  var answer = null;
  var xmlObj = new Object();
  if (window.XMLHttpRequest) {
    xmlObj = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    xmlObj = new ActiveXObject("Microsoft.XMLHTTP");
  };

  xmlObj.open("POST",obj.url,obj.mode);
  xmlObj.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
  xmlObj.send(obj.data);

  xmlObj.onreadystatechange = function(){
    if(xmlObj.readyState == 4){
      if (xmlObj.status == 200) {
        obj.callback(xmlObj.responseText)
      } else {
        return false;
      };
    };
  };
};


/**
 *  Wrap element
 *  wrapperEl - wrapper element type
 *  el - wrapping element
 *  id - id
 *  className - class name of wrapper
 */
Wrap = function(obj){
  var wrapper = document.createElement(obj.wrapperEl);
  if( obj.className != undefined )wrapper.className = obj.className;
  if( obj.id != undefined )wrapper.id = obj.id;
  wrapper.appendChild(obj.el.cloneNode(true));
  obj.el.parentNode.replaceChild(wrapper, obj.el);
};


/**
 * Add Class
 */
AddClass = function(arr,class_){
  for( var i = 0; i < arr.length; i++ ){
    arr[i].className += class_;
  };
};


/**
 * Extend Class
 */
Extend = function(destination, source) {
  for (var property in source) {
    if (source[property] && source[property].constructor &&
     source[property].constructor === Object) {
      destination[property] = destination[property] || {};
      arguments.callee(destination[property], source[property]);
    } else {
      destination[property] = source[property];
    }
  }
  return destination;
};