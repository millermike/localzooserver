
/**
 * Get angle, vertical and horizontal speeds
 * Incoming obj get el and elTo, speed
 */

/** EXAMPLE
 *
    new getDirectionVelocity({
      el:{"x":10,"y":20},
      elTo:{"x":50,"y":120},
      speed:2
    });
 *
 */
var getDirectionVelocity = function(obj){

  this.diffX = obj.elTo.x - obj.el.x;

  this.diffY = obj.elTo.y - obj.el.y;

  //Get angle
  this.angle = Math.atan2(this.diffX, this.diffY);

  this.vx = obj.speed * Math.sin(this.angle);
  this.vy = obj.speed * Math.cos(this.angle);

  //Get hypotenuse
  this.hypotenus = Math.sqrt( Math.pow(this.diffX,2) + Math.pow(this.diffY,2) );

  //Num of iteration to click place
  this.steps = Math.floor( this.hypotenus / obj.speed );

  return {
    "vx":this.vx,
    "vy":this.vy,
    "angle":this.angle,
    "hypo":this.hypotenus,
    "steps":this.steps
  };
};