/**
 * Canvas Class
 */

CanvBanner.TEXT = function(){
    this.renderText = function(obj){
      var style = "";
      obj.font = parseInt(obj.font);

      if( obj.bold != null ) style += " bold ";
      if( obj.italic != null ) style += " italic ";

      CanvBanner.ctx.font = style+" "+obj.font+"px "+obj.fontFamily;
      CanvBanner.ctx.fillStyle = "#"+obj.color;

      var text = this.parseText(obj);
      var textY = obj.y;
      for( var i = 0; i < text.length; i++ ){
        CanvBanner.ctx.fillText(text[i], obj.x, textY );
        textY += obj.font + 3;
      };
    };

    this.parseText = function(obj){
      var textArray = [];
      var textEl = "";
      for( var i = 0; i < obj.text.length; i++){
        textEl += obj.text[i];
        if( obj.text[i] == "\n" || i == obj.text.length - 1) {
          textArray.push(textEl);
          textEl = "";
        };
      };
      return textArray;
    };
};

CanvBanner.Animation = function(el){

  var anim = this;

  this.timer = null;

  this.obj = CanvBanner.structure;

  this.setTimer = function(){
    this.timer = setInterval(
      function(){
        anim.reDraw();
      },1500
    );
  };

  this.reDraw = function(){
    for( var type in anim.obj ){
      for( var i = 0; i < anim.obj[type].length; i++ ){
        
      };
    };
  };

  this.clear = function(){
    clearInterval(this.timer);
  };

  this.move = function(){
    this.setTimer();
  };
};