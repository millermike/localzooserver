/**
 * Drag and Drop Class
 */
CanvBanner.DragDrop = function(obj){
  
    var dragEl = this;
    
    this.drag = false;
    
    var el = null;
    
    var startX = null;
    
    var startY = null;
    
    var textblockName = " draggable";
    
    var elClass = "";

    var mouseDiff = null;

    obj.el.onmousedown = function(e){

      this.getAttribute('class') != null ? elClass = this.getAttribute('class') : elClass = "";

      //Get mouse diff position
      mouseDiff = {
        "x":e.pageX - this.offsetLeft,
        "y":e.pageY - this.offsetTop
      };

      el = this;
      dragEl.drag = true;
      startX = this.offsetLeft;
      startY = this.offsetTop;

      this.className += textblockName;
      el.style.left = startX;
      el.style.top = startY;
      
      if ( obj.start != undefined ) obj.start(el);

      window.onmousemove = function(e){
        if( dragEl.drag ){

          //Check for press near the border
          if( obj.diff ){
            if( mouseDiff.y <= 10 ) mouseDiff.y = 15;
          };

          el.style.left = e.pageX - mouseDiff.x;
          el.style.top = e.pageY - mouseDiff.y;

          if ( obj.move != undefined ) obj.move(e,mouseDiff);
          
        }else{
          return false;
        };
      };
    };

    if( obj.el != null ){
      obj.el.onmouseup = function(){
        if( obj.range ){
          with(el){
            if(
              offsetLeft + offsetWidth >= obj.dropEl.offsetLeft &&
                offsetLeft <= obj.dropEl.offsetLeft + obj.dropEl.offsetWidth &&
                  offsetTop + offsetHeight >= obj.dropEl.offsetTop &&
                    offsetTop <= obj.dropEl.offsetTop + obj.dropEl.offsetHeight
            ){
              if ( obj.drop != undefined ) obj.drop(this);
            };
          };
        }else{
          if ( obj.drop != undefined ) obj.drop(this);
        };

        dragEl.drag = false;
        this.className = elClass;

        //Reset position
        if ( obj.drop == undefined ){
          el.style.left = startX;
          el.style.top = startY;
        };
      };
    };
};