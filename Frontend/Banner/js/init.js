/**
 * Check and create namespace
 */
if( CanvBanner == undefined || !CanvBanner ){
  var CanvBanner = {};
  CanvBanner.info = {
    "version":"1.01",
    "creator":"Parikmaher",
    "mail":"parikmaher534@gmail.com"
  };
};


/**
 * Initialize NO-FLASH Project :)))
 */
onload = function(){
  new CanvBanner.init();
  document.getElementById('preloader').style.display = 'none';
};