/**
 * Text CONTANS
 */
TEXT_CONST = function(){
  
  this.modify_window_class = "b-modify-window";

  this.panel_id_pref = "controll";

  this.panel_max_width = 180;

  this.wrapper_id = "canvas-wrapper";

  this.addtext_block_id = "addTextBlock";

  this.canvas_text_class = "canvas-text";

  this.text_block_tmpl =
      "<div class='apply-text'>Применить</div>"+
      "<div class='change-text'>Редак-ть</div>"+
      "<div class='delete-text'>Удалить</div>"+
      "<textarea value=''></textarea>";

  this.diffFix = {
    x:4,
    y:3
  };

  this.default_font = "Arial";

  this.default_size = 16;

  this.canvas_el = new CanvBanner.TEXT();

  this.el_animation = new CanvBanner.Animation();

  this.DOM_animation = new CanvBanner.DOMAnimation();

  this.options_size_list = [8,9,10,11,12,14,16,18,22,24,28,32,36,42];

  this.options_family_list = ["Arial","Tahoma","Verdana","Times New Roman"];

  this.options_speed = [1,2,3,4,5,6,8,10];
  
  this.animation_type = ["Один повтор","Бесконечно"];

  this.font_id = "font-list";

  this.font_family_id = "font-family-list";

  this.font_color_id = "fontColorWrapper";

  this.font_color_default = "000000";

  this.style_b = "style-b";

  this.style_i = "style-i";

  this.stop_animation_class = "b-stop-animation";

  this.measure = "b-measure-block";
};

