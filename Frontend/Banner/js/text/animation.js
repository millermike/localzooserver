/**
 * Text Animation Class
 */
TEXT_ANIMATION = function(obj){
    var th = this;
    var fontAnimationWrapper =  new CreateElement({type:"div",class_:"font-wrapper animation-block aOpacity",parent:obj.panel});

    new CreateElement({type:"label",text:"<b>Анимация:</b>",parent:fontAnimationWrapper,class_:"style-label"});
    new CreateElement({type:"div",text:"начало и конец <br/> анимации",parent:fontAnimationWrapper,class_:"b-text-move-descript"});
    new CreateElement({type:"div",text:"a",parent:fontAnimationWrapper,class_:"b-text-move-a-prev"});
    new CreateElement({type:"div",text:"b",parent:fontAnimationWrapper,class_:"b-text-move-b-prev"});

    var animeA = new CreateElement({type:"div",text:"a",parent:fontAnimationWrapper,class_:"b-text-move-a"});
    var animeB = new CreateElement({type:"div",text:"b",parent:fontAnimationWrapper,class_:"b-text-move-b"});
    var animePrev = new CreateElement({type:"div",text:"preview",parent:fontAnimationWrapper,class_:"b-animation-preview"});

    //Animation speed
    var speedWrapper =  new CreateElement({type:"div",class_:"animation-speed-wrapper",parent:fontAnimationWrapper});
    new CreateElement({type:"label",text:"Скорость",parent:speedWrapper});
    new CreateElement({
      type:"select",
      options:obj.func.options_speed,
      id:"animation-speed",
      class_:"animation-speed",
      parent:speedWrapper
    });

    //Start Timeout for text
    var startAnimateAtWrapper = new CreateElement({type:"div",class_:"animation-start-wrapper",parent:fontAnimationWrapper});
    new CreateElement({type:"label",text:"Начать двигаться после (секунды)",value:"ss",parent:startAnimateAtWrapper});
    var startTime = new CreateElement({
      type:"input",
      inputType:"text",
      id:"animation-start-time",
      parent:startAnimateAtWrapper
    });
    startTime.value = "0";


    //Animation type
    var anmationTypeWrapper = new CreateElement({type:"div",class_:"animation-type-wrapper",parent:fontAnimationWrapper});
    new CreateElement({type:"label",text:"Тип анимации",value:"ss",parent:anmationTypeWrapper});
    var animeType = new CreateElement({
      type:"select",
      options:obj.func.animation_type,
      id:"animation-type",
      class_:"animation-type",
      parent:anmationTypeWrapper
    });



    var object = new GetObjEl(obj.el.id);

    object.animation = {};

    //Set default position of A block
    animeA.style.top = (animeA.parentNode.parentNode.offsetTop - obj.el.offsetHeight)+"px";
    animeA.style.left = (animeA.parentNode.parentNode.offsetLeft - animeA.offsetWidth)+"px";

    new CanvBanner.DragDrop({
      el:animeA,
      dropEl:document.getElementById(obj.func.wrapper_id),
      range:false,
      drop:function(){
        object.animation.aX = animeA.parentNode.parentNode.offsetLeft + animeA.offsetLeft;
        object.animation.aY = animeA.parentNode.parentNode.offsetTop + animeA.offsetTop;
      }
    });

    new CanvBanner.DragDrop({
      el:animeB,
      dropEl:document.getElementById(obj.func.wrapper_id),
      range:false,
      drop:function(){
        object.animation.bX = animeB.parentNode.parentNode.offsetLeft + animeB.offsetLeft;
        object.animation.bY = animeB.parentNode.parentNode.offsetTop + animeB.offsetTop;
      }
    });


    /**
     * Animation panel show/hide
     */
    var animation_toggle = new CreateElement({type:"input",inputType:"checkbox",class_:"font-wrapper animation-toggle",parent:obj.panel});
    var animation_blocked = new CreateElement({type:"div",text:"",parent:obj.panel,class_:"animation-blocker"});
    var a_blocker = new CreateElement({type:"div",text:"",parent:obj.panel,class_:"a-blocker"});
    var b_blocker = new CreateElement({type:"div",text:"",parent:obj.panel,class_:"b-blocker"});

    //Block animation on start panel
    a_blocker.style.top = animeA.offsetTop;
    a_blocker.style.left = animeA.offsetLeft;

    //Check for showing animation
    animation_toggle.onclick = function(){
      var anime = new ClassName('animation-block',animation_toggle.parentNode);

      if( this.checked ){
        anime[0].className = anime[0].className.replace(' aOpacity', '');
        animation_blocked.style.display = "none";
      }else{
        anime[0].className = anime[0].className + ' aOpacity';
        animation_blocked.style.display = "block";

        a_blocker.style.top = animeA.offsetTop;
        a_blocker.style.left = animeA.offsetLeft;

        b_blocker.style.top = animeB.offsetTop;
        b_blocker.style.left = animeB.offsetLeft;
      };
    };



    var textareaTempClass = "";
    var windowTempClass = "";
    var elTempId = "";
    var objPosX = null;
    var objPosY = null;
    var stopAnimation = document.getElementById(obj.func.stop_animation_class);
    var textareaBlocks = new ClassName("canvas-text");
    var textareaWinBlocks = new ClassName("b-modify-window");

    //Start preview
    var animation_obj = new GetObjEl(obj.el.id);
    animePrev.onclick = function(){
      if(
          animation_obj.animation.bX != undefined &&
          animation_obj.animation.bY != undefined
      ){
        //Hide blocks
        textareaTempClass = obj.el.className;
        windowTempClass = document.getElementById(obj.func.panel_id_pref.concat(obj.el.id)).className;

        new AddClass(textareaBlocks," hide");
        new AddClass(textareaWinBlocks," hide");
        document.getElementById(obj.func.measure).style.display = "none";

        //Create animations stop button
        stopAnimation.className = "show";

        //Animation speed
        animation_obj.animation.speed = new SelectOption(document.getElementById("animation-speed")).value;

        //Animation start time
        animation_obj.animation.start = parseInt(document.getElementById("animation-start-time").value);

        //Animate object
        elTempId = obj.el.id;
        obj.el.id = "test";
        objPosX = obj.el.offsetLeft;
        objPosY = obj.el.offsetTop;

        //Set animation type to object
        animation_obj.animeType = new SelectOption(document.getElementById("animation-type")).value;

        obj.func.DOM_animation.textAnimation({
          item:obj.el,
          elObj:new GetObjEl(elTempId),
          speed:1,
          type:animation_obj.animeType,
          finish:function(){
            th.stopAnimation();
          }
        });
      }else{
        alert("Задайти начальную и конечную точки анимации");
      };
    };

    //Finish preview
    stopAnimation.onclick = function(){
      th.stopAnimation();
    };

    this.stopAnimation = function(){
      //Add old classes
      stopAnimation.className = "hide";

      var measureBlock = document.getElementById(obj.func.measure);
      if( CanvBanner.Measure == "show" ){
        measureBlock.style.display = "block";
      }else if( CanvBanner.Measure == "hide" ){
        measureBlock.style.display = "none";
      };

      new AddClass(textareaBlocks,textareaTempClass);
      new AddClass(textareaWinBlocks,windowTempClass);

      obj.el.id = elTempId;
      obj.func.DOM_animation.clear(obj.el);
      obj.el.style.left = objPosX+"px";
      obj.el.style.top = objPosY+"px";

      //Redraw controll panel after animation stop
      new TEXT_PANEL_MOVE(obj.el,obj.func)
    };
};