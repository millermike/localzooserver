/*
 * Controll panel Class
 */
TEXT_PANEL = function(func,el){
    if( document.getElementById(func.panel_id_pref.concat(el.id)) != null ){
      new TEXT_PANEL_REMOVE(el,func);
    }else{
      new TEXT_PANEL_REMOVE(el,func);

      //Main controll wrapper
      var panel = new CreateElement({
        type:"div",
        class_:func.modify_window_class,
        id:func.panel_id_pref+""+el.id,
        parent:el.parentNode
      });

      /*
       * Font size SELECT
       */
      // Font size wrapper
      var fontWrapper =  new CreateElement({type:"div",class_:"font-wrapper",parent:panel});

      // Create fot label
      new CreateElement({type:"label",text:"Размер шрифта",parent:fontWrapper});

      // Create font-size select
      new CreateElement({
        type:"select",
        options:func.options_size_list,
        id:func.font_id,
        class_:"font-select",
        parent:fontWrapper
      });

      /*
       * Font Family
       */
      // Font family wrapper
      var fontFamilyWrapper =  new CreateElement({type:"div",class_:"font-wrapper",parent:panel});

      // Create font label
      new CreateElement({type:"label",text:"Тип шрифта",parent:fontFamilyWrapper});

      // Create font-family select
      new CreateElement({
        type:"select",
        options:func.options_family_list,
        id:func.font_family_id,
        class_:"font-select",
        parent:fontFamilyWrapper
      });

      /**
       * Font color
       */
      var fontColorWrapper =  new CreateElement({type:"div",class_:"font-wrapper",parent:panel});

      // Create font label
      new CreateElement({type:"label",text:"Цвет шрифта",parent:fontColorWrapper});

      // Create font-color select
      var fontColor = new CreateElement({
        type:"input",
        inputType:"text",
        options:func.options_family_list,
        id:func.font_color_id,
        class_:"font-select color",
        parent:fontColorWrapper
      });
      var picker = new jscolor.color(fontColor);


      /**
       * Text style
       */
      var fontStyleWrapper =  new CreateElement({type:"div",class_:"font-wrapper",parent:panel});

      //Bold
      new CreateElement({type:"label",text:"<b>b</b>",parent:fontStyleWrapper,class_:"style-label"});
      new CreateElement({
        type:"input",
        inputType:"checkbox",
        id:func.style_b,
        class_:"font-chekbox",
        parent:fontStyleWrapper
      });

      //Italic
      new CreateElement({type:"label",text:"<i>i</i>",parent:fontStyleWrapper,class_:"style-label"});
      new CreateElement({
        type:"input",
        inputType:"checkbox",
        id:func.style_i,
        class_:"font-chekbox",
        parent:fontStyleWrapper
      });
    };


    //Extend Animation Class
    new Extend(func,new TEXT_ANIMATION({
        func:func,
        el:el,
        panel:panel
    }));


    // Set panel position and width
    if( panel != undefined ){
      panel.style.top = el.offsetTop + el.offsetHeight +"px";
      panel.style.left = el.offsetLeft+"px";
      panel.style.width = el.offsetWidth-2+"px";
      new TEXT_PANEL_WIDTH(el,func);
    };
};


/**
 * Remove controll panel block
 */
TEXT_PANEL_REMOVE = function(el,func){
  var controllWindows = new ClassName(func.modify_window_class);
  for( var i = 0; i < controllWindows.length; i++ ){
    document.body.removeChild(controllWindows[i]);
  };
  var panel_el = document.getElementById(func.panel_id_pref+""+el.id);
  if( panel_el != null ){
    document.body.removeChild(panel_el);
  };
};


/**
 * Set max panel size
 */
TEXT_PANEL_WIDTH = function(el,func){
  var panel_el = document.getElementById(func.panel_id_pref+""+el.id);
  if( panel_el.offsetWidth >= func.panel_max_width ){
    panel_el.style.width = func.panel_max_width+"px";
  };
};


/**
 * Move controll panel to text block
 */
TEXT_PANEL_MOVE = function(el,func){
  var controllPanelBlock = document.getElementById(func.panel_id_pref.concat(el.id));
  if( controllPanelBlock != null){
    controllPanelBlock.style.left = el.offsetLeft+"px";
    controllPanelBlock.style.top = el.offsetTop + el.offsetHeight + "px";
  };
};


