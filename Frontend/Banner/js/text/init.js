CanvBanner.TEXT_BLOCK = function(){

  var func = this;


  /**
   * Extend basic parameters
   */
  new Extend(this,new TEXT_CONST());


  /**
   *  Add text block to canvas
   */
  var text = null;

  new CanvBanner.DragDrop({
    el:document.getElementById(this.addtext_block_id),
    dropEl:document.getElementById(this.wrapper_id),
    diff:true,
    range:true,
    drop:function(){

        //Create text block
        var id = new Date().getTime();
        text = document.createElement('div');
        text.setAttribute('class',func.canvas_text_class);
        text.setAttribute('id',id);
        text.innerHTML = func.text_block_tmpl;
        text.style.left = CanvBanner.AddText.x+"px";
        text.style.top = CanvBanner.AddText.y+"px";
        document.body.appendChild(text);

        //Create its object
        var txtObj = {
          "id":id,
          "x":CanvBanner.AddText.x - CanvBanner.canvasX,
          "y":CanvBanner.AddText.y - CanvBanner.canvasY
        };

        //Add new text object to array
        CanvBanner.structure.text.push(txtObj);

        var el = document.getElementById(id);
        var textarea = el.getElementsByTagName('textarea')[0];
        var controllBlocks = el.getElementsByTagName('div');


        //Drag text block
        new CanvBanner.DragDrop({
          el:text,
          dropEl:document.getElementById(func.wrapper_id),
          range:true,
          drop:function(obj){
            el.style.left = obj.offsetLeft+"px";
            txtObj.x = text.offsetLeft - CanvBanner.canvasX;
            txtObj.y = text.offsetTop - CanvBanner.canvasY;


            /**
             * Count animation position after draging text block
             */
            var anime_obj = document.getElementById(func.panel_id_pref.concat(el.id));
            if( anime_obj != null ){
              var anime_obj_a = new ClassName("b-text-move-a",anime_obj);
              var anime_obj_b = new ClassName("b-text-move-b",anime_obj);
              var anime_obj_sruct = new GetObjEl(el.id);

              anime_obj_sruct.animation.aX = anime_obj_a[0].parentNode.parentNode.offsetLeft + anime_obj_a[0].offsetLeft;
              anime_obj_sruct.animation.aY = anime_obj_a[0].parentNode.parentNode.offsetTop + anime_obj_a[0].offsetTop;

              anime_obj_sruct.animation.bX = anime_obj_b[0].parentNode.parentNode.offsetLeft + anime_obj_b[0].offsetLeft;
              anime_obj_sruct.animation.bY = anime_obj_b[0].parentNode.parentNode.offsetTop + anime_obj_b[0].offsetTop;
            };
          },
          move:function(){
            new TEXT_PANEL_MOVE(el,func);
          }
        });


        //Set textarea size on blur
        new GetTextareaSize(id,txtObj);


        /**
         *
         *  Controll panel methods: REMOVE TEXT, APPLY TEXT, CALL CONTROLL PANEL
         *
         */

        /**
         * Remove text block
         */
        controllBlocks[2].onclick = function(){
          document.body.removeChild(el);
          new TEXT_PANEL_REMOVE(el,func);
        };

        /**
         * Modify text block
         */
        controllBlocks[1].onclick = function(){
          new TEXT_PANEL(func,el);

          if( document.getElementById(func.panel_id_pref.concat(el.id)) != null ){
            var textblock = new GetObjEl(id);

            // Get font size
            var fontBlock = document.getElementById(func.font_id);
            fontBlock.options[6].selected = true;
            fontBlock.onchange = function(){
              var el = document.getElementById(id);
              var textarea = document.getElementById(id).getElementsByTagName('textarea')[0];
              textblock.fontSize = this.options[this.selectedIndex].value;

              // Resize inputs
              textarea.style.fontSize = textblock.fontSize;

              //Redraw on new position
              document.getElementById(func.panel_id_pref.concat(el.id)).style.top = el.offsetTop + el.offsetHeight +"px";
            };

            // Get font family
            var familyBlock = document.getElementById(func.font_family_id);
            familyBlock.options[0].selected = true;
            familyBlock.onchange = function(){
              textblock.fontFamily = this.options[this.selectedIndex].value;
              document.getElementById(id).getElementsByTagName('textarea')[0].style.fontFamily = textblock.fontFamily;
            };

            //Get font color
            var color_input = document.getElementById(func.font_color_id);
            color_input.onchange = function(){
              textblock.fontColor = color_input.value;
              document.getElementById(id).getElementsByTagName('textarea')[0].style.color = "#"+color_input.value;
            };

            //Get font styles
            var bold = document.getElementById(func.style_b);
            var italic = document.getElementById(func.style_i);
            bold.onclick = function(){
              var textarea = document.getElementById(id).getElementsByTagName('textarea')[0];
              if( this.checked ){
                textblock.bold = 1;
                textarea.style.fontWeight = "bold";
              }else{
                textblock.bold = null;
                textarea.style.fontWeight = "normal";
              };
            };
            italic.onclick = function(){
              var textarea = document.getElementById(id).getElementsByTagName('textarea')[0];
              if( this.checked ){
                textblock.italic = 1;
                textarea.style.fontStyle = "italic";
              }else{
                textblock.italic = null;
                textarea.style.fontStyle = "normal";
              };
            };
          };
        };

        //Apply changes
        controllBlocks[0].onclick = function(){
          var textblock = new GetObjEl(id);

          //Rewrite text block coordinates
          textblock.x = el.offsetLeft - CanvBanner.canvasX;
          textblock.y = el.offsetTop - CanvBanner.canvasY;

          //If no default font then font size is 16px
          if( textblock.fontSize == undefined ) textblock.fontSize = func.default_size;

          //If no default font family then font family is Arial
          if( textblock.fontFamily == undefined ) textblock.fontFamily = func.default_font;

          //If no default font color then font color is black
          if( textblock.fontColor == undefined ) textblock.fontColor = func.font_color_default;

          func.canvas_el.renderText({
            "text":el.getElementsByTagName('textarea')[0].value,
            "font":textblock.fontSize,
            "fontFamily":textblock.fontFamily,
            "bold":textblock.bold,
            "italic":textblock.italic,
            "color":textblock.fontColor,
            "x":textblock.x + textarea.offsetLeft + func.diffFix.x,
            "y":textblock.y + textarea.offsetTop + parseInt(textblock.fontSize) + func.diffFix.y
          });
          
          //Get Animation info
          var object = new GetObjEl(id).animation;
          object.speed = parseInt(new SelectOption(document.getElementById("animation-speed")).value);
          object.start = parseInt(document.getElementById("animation-start-time").value);
          object.type = new SelectOption(document.getElementById("animation-type")).value;
          
          CanvBanner.data.text.push({
            "id":id,
            "text":el.getElementsByTagName('textarea')[0].value,
            "font":textblock.fontSize,
            "fontFamily":textblock.fontFamily,
            "bold":textblock.bold,
            "italic":textblock.italic,
            "color":textblock.fontColor,
            "x":textblock.x + textarea.offsetLeft + func.diffFix.x,
            "y":textblock.y + textarea.offsetTop + parseInt(textblock.fontSize) + func.diffFix.y,
            "animation":new GetObjEl(id).animation
          });

          //Remove element after apply
          el.parentNode.removeChild(el);
          new TEXT_PANEL_REMOVE(el,func);
        };

        //Get text from textarea and add to text object
        el.onkeyup = function(){
          new GetObjEl(el.id).text = textarea.value;
        };

        //Redraw on resize
        var functionality = new CanvBanner.Controll();
        window.onresize = function(){
          functionality.reDrawBlocks();
        };
    },
    move:function(e,diff){
      //Remember text block position
      CanvBanner.AddText.x = e.pageX - diff.x + 5;
      CanvBanner.AddText.y = e.pageY - diff.y + 10;

      //Remember mouse coordinates
      CanvBanner.mouseX = e.pageX;
      CanvBanner.mouseY = e.pageY;
    },
    start:function(el){
      CanvBanner.AddText.el = el;
    }
  });
};