/**
 * Get textarea object
 */
GetObjEl = function(id){
  var items = CanvBanner.structure.text;
  for( var i = 0; i < items.length; i++){
    if( id == items[i].id ){
      return items[i];
    };
  };
};

/**
 * Get textarea size
 */
GetTextareaSize = function(id,txtObj){
  var textarea = document.getElementById(id).getElementsByTagName('textarea')[0];
  textarea.onblur = function(){
    txtObj.width = this.offsetWidth;
    txtObj.height = this.offsetWidth;
  };
};

/* Get Selected option */
SelectOption = function(obj){
  var opt = obj.getElementsByTagName("option");
  var sel = null;
  for( var i = 0; i < opt.length; i++ ){
    if(opt[i].selected) sel = opt[i];
  };
  return sel;
};
