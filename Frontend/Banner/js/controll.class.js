CanvBanner.structure = {
  text:[]
};
CanvBanner.canvasName = "screen";
CanvBanner.CnvWdInput = {
  id:"canvasWidth"
};
CanvBanner.CnvHgInput = {
  id:"canvasHeight"
};
CanvBanner.CnvColorInput = {
  id:"canvasColor"
};
CanvBanner.AddText = {
  id:"addTextBlock"
};
CanvBanner.data = {
  "id":"",
  "canvas":{
    width:"350",
    height:"150",
    background:"FFFFFF"
  },
  "text":[]
};
CanvBanner.Measure = {
  status:"hide"
};


/**
 * Initialize
 */
CanvBanner.init = function(){
  "use strict";

  this.define = function(){
    CanvBanner.canvas = document.getElementById(CanvBanner.canvasName);
    CanvBanner.ctx = CanvBanner.canvas.getContext('2d');

    if( CanvBanner.ctx ){
      new CanvBanner.Controll();
      new CanvBanner.Functional();
    };
  };
  this.define();
};


/**
 * Basic Canvas controll
 */
CanvBanner.Controll = function(){

  var controll = this;

  this.init = function(){
    this.resize();
    this.CanvasPosition();
  };

  this.resize = function(){
    window.onresize = function(){
      controll.CanvasPosition();
    };
  };

  //Redraw all items in window
  this.reDrawBlocks = function(){
    for( var el in CanvBanner.structure ){
      for( var i = 0; i < CanvBanner.structure[el].length; i++ ){
        if( item != null ){
          var item = document.getElementById(CanvBanner.structure[el][i].id);
          item.style.left = CanvBanner.canvasX + CanvBanner.structure[el][i].x;
          item.style.top = CanvBanner.canvasY + CanvBanner.structure[el][i].y;
        };
      };
    };
  };

  this.CanvasPosition = function(){
     this.pr = CanvBanner.canvas.parentNode;

     document.getElementById(CanvBanner.canvasName).className = "no-margin";

     CanvBanner.canvas.style.left = ((this.pr.offsetWidth-CanvBanner.canvas.offsetWidth)/2)+"px";
     CanvBanner.canvas.style.top = ((this.pr.offsetHeight-CanvBanner.canvas.offsetHeight)/2)+"px";

     CanvBanner.canvasX = CanvBanner.canvas.parentNode.offsetLeft + CanvBanner.canvas.offsetLeft;
     CanvBanner.canvasY = CanvBanner.canvas.parentNode.offsetTop + CanvBanner.canvas.offsetTop;
  };

  document.onselectstart = function(){
    return false;
  };

  this.init();
};


/**
 * Canvas Functional
 */
CanvBanner.Functional = function(){

  var resizeObj = new CanvBanner.Controll();

  var func = this;

  /**
   * Include functional classes
   */
  this.init = function(){
    //Set size
    this.CanvasSize();

    //Set background
    this.CanvasBackground();

    //Send Ajax data to server
    this.getBanner();

    //Get ID
    this.getBannerID();

    //Measure
    this.measure();
  };


  /**
   * Canvas resizing
   */
  this.CanvasSize = function(){
    CanvBanner.CnvWdInput.el = document.getElementById(CanvBanner.CnvWdInput.id);
    CanvBanner.CnvHgInput.el = document.getElementById(CanvBanner.CnvHgInput.id);
    CanvBanner.CnvWdInput.el.onkeyup = function(){
      CanvBanner.canvas.style.width = this.value+"px";
      CanvBanner.canvas.setAttribute("width",this.value);
      CanvBanner.data.canvas.width = this.value;
      resizeObj.CanvasPosition();

      func.ReDrawElements();
    };
    CanvBanner.CnvHgInput.el.onkeyup = function(){
      CanvBanner.canvas.style.height = this.value+"px";
      CanvBanner.canvas.setAttribute("height",this.value);
      CanvBanner.data.canvas.height = this.value;
      resizeObj.CanvasPosition();

      func.ReDrawElements();
    };
  };


  /**
   * Set Canvas Background
   */
  this.CanvasBackground = function(){
    CanvBanner.CnvColorInput.el = document.getElementById(CanvBanner.CnvColorInput.id);
    CanvBanner.CnvColorInput.el.onchange = function(){
      CanvBanner.canvas.style.background = "#"+this.value;
      CanvBanner.data.canvas.background = this.value;
    };
    CanvBanner.CnvColorInput.el.onkeyup = function(){
      CanvBanner.canvas.style.background = "#"+this.value;
      CanvBanner.data.canvas.background = this.value;
    };
  };

  //Create text block object
  new CanvBanner.TEXT_BLOCK();

  this.canvas_plot = new CanvBanner.TEXT();
  this.ReDrawElements = function(){
    // Redraw Text blocks
    for( var text = 0; text < CanvBanner.data.text.length; text++){
      this.canvas_plot.renderText({
        "text":CanvBanner.data.text[text].text,
        "font":CanvBanner.data.text[text].font,
        "fontFamily":CanvBanner.data.text[text].fontFamily,
        "color":CanvBanner.data.text[text].color,
        "x":CanvBanner.data.text[text].x,
        "y":CanvBanner.data.text[text].y
      });
    };
  };


  /**
   * Measure
   */
  this.measure = function(){
    var measureBlock = document.getElementById("b-measure-block");
    document.getElementById("measure").onclick = function(){
      if( measureBlock.style.display != "block" ){
        measureBlock.style.display = "block";
        CanvBanner.Measure = "show";
      }else{
        measureBlock.style.display = "none";
        CanvBanner.Measure = "hide";
      };
    };
  };


  /**
   * Set Banner ID
   */
  this.getBannerID = function(){
    var idInput = document.getElementById("getId");
    idInput.onkeyup = function(){
      CanvBanner.data.id = this.value;
    };
  };


  this.getBanner = function(){
    document.getElementById("getBanner").onclick = function(){

        var sendData = null;
        if( JSON.stringify != undefined ){
          sendData = JSON.stringify(CanvBanner.data);
        }else{
          //Some parse
        };

        new AjaxConnect({
          url:"getCanvas.php",
          data:"data="+sendData,
          mode:true,
          callback:function(data){
            //console.log(JSON.parse(data));
          }
        });
    };
  };

  this.init();
};
