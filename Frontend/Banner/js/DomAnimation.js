CanvBanner.DOMAnimation = function(){
  
  var anim = this;
  
  this.timer = null;
  
  this.textAnimation = function(obj){

    var objAnimation = obj.elObj.animation;

    if( objAnimation.aX == undefined ){
      objAnimation.aX = obj.item.offsetLeft;
      objAnimation.aY = obj.item.offsetTop;
    };

    var vectorObj = new getDirectionVelocity({
      el:{"x":objAnimation.aX,"y":objAnimation.aY},
      elTo:{"x":objAnimation.bX,"y":objAnimation.bY},
      speed:objAnimation.speed
    });

    var a_b_side = 14;
    var txt_margin = 5;

    var moveX = objAnimation.aX + txt_margin;
    var moveY = objAnimation.aY;

    //Set start up position
    obj.item.style.left = moveX+"px";
    obj.item.style.top = moveY+"px";

    //Set Start animation time
    setTimeout(
      function(){
        anim.setTimer({
          init:function(){
            moveX += vectorObj.vx/3;
            moveY += vectorObj.vy/3;
            obj.item.style.left = moveX+"px";
            obj.item.style.top = moveY+"px";

            if( moveX >= objAnimation.bX && moveY >= objAnimation.bY &&
                moveX <= objAnimation.bX + a_b_side && moveY <= objAnimation.bY + a_b_side
              ){
              anim.clear();
              obj.finish();
            };
          }
        });
      },objAnimation.start*1000
    );
  };
  
  this.setTimer = function(obj){
    this.timer = setInterval(
      function(){
        obj.init();
      },10
    );
  };
  
  this.clear = function(obj){
    clearInterval(this.timer);
  };

};

