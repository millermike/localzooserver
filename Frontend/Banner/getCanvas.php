<?php

  //Get data
  $script_data = $_POST['data'];

  $script_data = str_replace('\\','',$script_data);

  $script = "
    <script>
    onload = function(){
      (function(){
        var canvasData = {$script_data};
        var el = document.getElementById(canvasData.id);
        var canvas = document.createElement('canvas');
        canvas.setAttribute('width',canvasData.canvas.width);
        canvas.setAttribute('height',canvasData.canvas.height);
        canvas.style.backgroundColor = '#'+canvasData.canvas.background;
        el.appendChild(canvas);

        var ctx = canvas.getContext('2d');

        if( canvasData.text.length != 0 ){
          for( var text = 0; text < canvasData.text.length; text++ ){
            var style = '';
            if( canvasData.text[text].bold != null ) style += ' bold ';
            if( canvasData.text[text].italic != null ) style += ' italic ';

            ctx.font = style+' '+canvasData.text[text].font+'px '+canvasData.text[text].fontFamily;
            ctx.fillStyle = '#'+canvasData.text[text].color;

            var textArr = parseText(canvasData.text[text].text);
            var textY = canvasData.text[text].y;

            for( var i = 0; i < textArr.length; i++ ){
              ctx.fillText(textArr[i], canvasData.text[text].x, textY);
              textY += canvasData.text[text].font + 3;
            };
          };
        };

        function parseText(obj){
          var textArray = [];
          var textEl = '';
          for( var i = 0; i < obj.length; i++){
            textEl += obj[i];
            if( obj[i] == '\n' || i == obj.length - 1) {
              textArray.push(textEl);
              textEl = '';
            };
          };
          return textArray;
        };
      })();
    };
    </script>
  ";

  $ourFileName = "banner/test.txt";
  $ourFileHandle = fopen($ourFileName, 'w') or die("can't open file");
  fclose($ourFileHandle);

  echo $script;
?>
