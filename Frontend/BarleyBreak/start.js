(function() {

    var scripts = [
        "common/js/common.js", 
        "common/js/clock/clock.js", 
        "common/js/events.js", 
        "common/js/popup.js",
        "common/js/timer.js",
        "BarleyBreak/loadhtml.js",
        "BarleyBreak/js/tmpl.js",
        "BarleyBreak/js/game.js",
        "BarleyBreak/js/graphics.js",
        "BarleyBreak/js/init.js"
    ];

    var styles = [
        "common/css/control.css",
        "BarleyBreak/css/styles.css"
    ]
   
    var indexScript = 0,
        scriptLoading = function(indexScript) {
            loadScript(scripts[indexScript], function() {
                indexScript++;
                if( indexScript < scripts.length ) {
                    scriptLoading(indexScript); 
                }
            });
        }

    var indexStyle = 0,
        styleLoading = function(indexStyle) {
            loadCSS(styles[indexStyle], function() {
                indexStyle++;
                if( indexStyle < styles.length ) {
                    styleLoading(indexStyle); 
                } else {
                    scriptLoading(indexScript);
                }
            });
        }

    styleLoading(indexStyle);

}())

