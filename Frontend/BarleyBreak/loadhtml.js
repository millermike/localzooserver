(function() {
    var HTML = "<div class='game-wrapper game_small'>"+
                    "<div class='control'>"+
                        "<div class='control__panel'>"+
                            "<div class='control__item'>"+
                                "<h4>Game type</h4>"+
                                "<label>Classic</label>"+
                                "<input type='radio' name='type' value='classic' checked='true' id='viewClassic'/>"+
                                "<label>Image</label>"+
                                "<input type='radio' name='type' value='image' id='viewImage'/>"+
                                "<div class='images-list images-list_transparent'>"+
                                    "<ul>"+
                                        "<li><img src='/BarleyBreak/img/tiger_thumb.jpg' data-image='tiger.jpg' class='current-image'/></li>"+
                                        "<li><img src='/BarleyBreak/img/trees_thumb.jpg' data-image='trees.jpg'/></li>"+
                                        "<li><img src='/BarleyBreak/img/flower_thumb.jpg' data-image='flower.jpg'/></li>"+
                                        "<li><img src='/BarleyBreak/img/fruits_thumb.jpeg' data-image='fruits.jpeg'/></li>"+
                                    "</ul>"+
                                "</div>"+
                            "</div>"+
                            "<div class='control__item control__item_small'>"+
                                "<h4>Scene size</h4>"+
                                "<input type='number' name='size' value='4' id='viewSize'/>"+
                            "</div>"+
                            "<div class='control__item control__item_small'>"+
                                "<h4>Mix deep</h4>"+
                                "<input type='number' name='mix' value='100' id='mixLevel'/>"+
                            "</div>"+
                            "<div class='control__item control__item_small all_inline'>"+
                                "<h4>Play on time</h4>"+
                                "<input type='checkbox' name='time' id='gameTimer' class='control__timer-checkbox'/>"+
                            "</div>"+
                        "</div>"+
                        "<div class='control__block' id='GameWrapper'>"+
                            "<div class='game-shadow'></div>"
                        "<div>"+
                    "</div>"+
                "</div>";

    GAMEBLOCK.innerHTML = HTML;
}());
