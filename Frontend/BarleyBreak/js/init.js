(function() {
  
    var game, storageObject, defaultGameObject,
        prevFieldsObject, fieldsObject, all, number,
        isImage = false,
        localStorageId = 'ZooGames.barley',
        transparentClass = 'images-list_transparent',
        isImage        = false,
        parent         = document.getElementById('GameWrapper'),  
        storageData    = localStorage[localStorageId],
        imagesList     = document.getElementsByClassName('images-list')[0],
        images         = imagesList.getElementsByTagName('img');
    
    var createGame = function(params) {
        defaultGameObject = {
            appendTo: parent, 
            position: 'center',
            cellSize: 50,
            size: params ? params.size : 4,
            mixDeep: params ? params.mix : 50,
            cellImg: 'BarleyBreak/img/bg.png',
            time: params ? params.time : false,
            styles: {
                fontSize: 15,
                fontWeight: 'bold',
                fontFamily: 'Arial',
                textColor: '#e2ceb9',
                borderColor: '#371d02',
                canvasBackground: '/img/bg.png',
                emptyCellBg: '#dcc6b0'
            }
        };

        if( params && params.type == 'image' && isImage ) {
            defaultGameObject.image = 'BarleyBreak/img/' + params.image;
        }

        return new Game(defaultGameObject);
    };


    /*Crate game on document load */
    if( storageData ) {

        storageObject = JSON.parse(storageData);
        
        //Set game type
        if( storageObject.type === 'image' ) {
            document.getElementById('viewImage').checked = true;
            isImage = true;
            imagesList.className = imagesList.className.replace(transparentClass, '')
        }
        else {
            document.getElementById('viewClassic').checked = true;
            isImage = false;
        }

        //Set game size
        document.getElementById('viewSize').value = storageObject.size;

        //Set game mix deep
        document.getElementById('mixLevel').value = storageObject.mix;
           
        //Set game timer 
        if( storageObject.time ) {
            document.getElementById('gameTimer').checked = true;  
        }
        
        game = createGame(storageObject);
    }
    else{
        game = createGame();
    }


    /* Control panel */
    all = document.querySelectorAll('input[type=number], input[type=radio], input[type=checkbox]');
    number = document.querySelectorAll('input[type=number]'); 

    /* Choose image */
    imagesList.addEventListener('click', function(e) {
        if( isImage && Object.keys(e.target.dataset).length ) {
            Array.prototype.slice.apply(images).forEach(function(img) {
                if( img.className.indexOf('current-image') !== -1 ) {
                    img.className = '';
                }
            });

            e.target.className = 'current-image';
        
            game.destroy();
            game = createGame(getFieldsData());
        }
    }, false);


    var getFieldsData = function() {
        return {
            size: parseInt(id('viewSize').value),
            type: id('viewClassic').checked ? 'classic' : 'image',
            image: imagesList.getElementsByClassName('current-image')[0].dataset['image'], 
            mix: parseInt(id('mixLevel').value),
            time: id('gameTimer').checked
        }
    }

    /* Listen contorlls events and update game */
    var reCreate = function(e) {

        if( e.target.type === 'radio' ) {
            if( e.target.value === 'image' ) {
                isImage = true;
                imagesList.className = imagesList.className.replace(transparentClass, '')
            } else {
                isImage = false;
                console.log(imagesList)
                imagesList.className += transparentClass;
            }
        }

        if( e.target.value ) {
            if( e.keyCode !== 9 ) {

                if( e.target.name === 'size' ) {
                    if( e.target.value > 8 ) {
                        e.target.value = 8;
                    } else if( e.target.value < 2 ) {
                        e.target.value = 2;
                    }
                }

                if( e.target.name === 'mix' ) {
                    if( e.target.value > 1000) {
                        e.target.value = 1000;
                    }
                }

                fieldsObject = getFieldsData();
               
                /* Compare 2 objects on equival */ 
                if( JSON.stringify(fieldsObject) !== JSON.stringify(prevFieldsObject) ) {
                    game.destroy();
                    game = createGame(fieldsObject);

                    prevFieldsObject = getFieldsData();

                    localStorage['ZooGames.barley'] = JSON.stringify(fieldsObject);
                }
            }
        }
    };

    Array.prototype.slice.apply(all).forEach(function(el) {
        el.addEventListener('change', reCreate, false); 
    });

    Array.prototype.slice.apply(number).forEach(function(el) {
        el.addEventListener('keyup', reCreate, false); 
    });

    //If player win the game
    EVENTER.on(Popup, 'win', function() {
        if( game.clock ) {
            game.clock.stop();
        } 
        Popup.setContent('You win. Close to start again');
        Popup.show();

        $.ajax({
            type: 'POST',
            url: 'http://localhost:8088/Games',
            dataType: 'json',
            data: 'game=BarleyBreak&finish=success',
            success: function(res){
                if( res.status === '200' ) {
                    removeClass(spinner, ' spinner_hide');
                    loadScript(data.startScript);
                }
            }
        });
    });

    //Recalc canvas position on window resize
    window.addEventListener('resize', function() {
        game.calcPosition();
    }, false);

    //Stop timer when we change focus
    window.addEventListener('blur', function() {
        if( game.clock ) {
            game.clock.stop();
        } 
    });

    //Resume game when focus is again in current window
    window.addEventListener('focus', function() {
        
        //Fix for toggle between tabs
        setTimeout(function() {
            if( game.clock ) {
                game.clock.resume();
            } 
        }, 100);
    });

    //Stop spinner
    var spinner = document.getElementsByClassName('spinner')[0];
    addClass(spinner, 'spinner_hide');

}());

