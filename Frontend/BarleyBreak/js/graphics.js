var Graphics = function(params, canvas, ctx, gamelist) {

    var fontSize        = params.styles.fontSize,
        listLength      = params.size * params.size,
        row             = -1,
        cell            = 0,
        timer           = params.timer,
        sz              = params.cellSize,
        szHalf          = sz / 2,
        szDouble        = sz * 2,
        szOneHalf       = sz + szHalf,
        bg              = new Image(),

        posX, 
        posY, 
        dir,
        setPos, 
        setAnimationX, 
        setAnimationY, 
        animationCanvas, 
        animationCtx,
        cellBuffer, 
        cellBufferCtx,
        imgResizeBuffer, 
        imgResizeBufferCtx;
    
    
    /* Create image object for cells draw */
    if( params.image ) {
        bg.src = params.image;
    }
    else {
        bg.src = params.cellImg;
    }

    /* When game background was load */
    bg.addEventListener('load', function() {

        /* Create image resize canvas */
        imgResizeBuffer = document.createElement('canvas');
        imgResizeBufferCtx = imgResizeBuffer.getContext('2d');
        imgResizeBuffer.width = params.cellSize * params.size;
        imgResizeBuffer.height = params.cellSize * params.size;
       
        if( params.image ) {
            imgResizeBufferCtx.drawImage(bg, 0, 0, params.cellSize * params.size, params.cellSize * params.size);
        }
        else {
            imgResizeBufferCtx.drawImage(bg, 0, 0, params.cellSize, params.cellSize);
        }

        EVENTER.trigger('resource.load');
    });

    /* START Animation canvas */
    animationCanvas = document.createElement('canvas');
    animationCanvas.className = 'BarleyBreakAnimation';
    params.appendTo.appendChild(animationCanvas);
    animationCtx = animationCanvas.getContext('2d');

    setAnimationX = function(x) {
        animationCanvas.style.left = x + 'px';
    };

    setAnimationY = function(y) {
        animationCanvas.style.top = y + 'px';
    };

    /* START Cell move animation helper */
    setPos = function(distance, dir) {
        if( distance > 0 ) {
            posX = szHalf;
            posY = szHalf;       
        } else {
            posX = dir === 'x' ? szOneHalf : szHalf;
            posY = dir === 'y' ? szOneHalf : szHalf;
        };
       
        animationCanvas.width = dir === 'x' ? szDouble : sz;
        animationCanvas.height = dir === 'y' ? szDouble : sz;
    };

    var strokeRect = function(data, margin) {
        margin = margin || 0;
        ctx.strokeRect(
                data.cell * params.cellSize + margin,
                data.row * params.cellSize + margin, 
                params.cellSize - (margin + 1), 
                params.cellSize - (margin + 1)
        );
    };

    return {
        QuickClear: function(c) {
            c.width = c.width;
        },

        SetCanvasStyles : function(ctx) {
            ctx.strokeStyle = params.styles.textColor;
            ctx.lineWidth = 0.5;
            ctx.textBaseline = 'middle';
            ctx.textAlign = 'center';
            ctx.font = params.styles.fontWeight +' '+ fontSize +'px ' + params.styles.fontFamily;
            ctx.fillStyle = params.styles.borderColor;
        },

        DrawCell : function(data) {

            this.SetCanvasStyles(ctx);

                if( data.val === '' ){
                    ctx.fillStyle = params.styles.emptyCellBg;
                    ctx.fillRect(
                            data.cell * params.cellSize,
                            data.row * params.cellSize, 
                            params.cellSize, 
                            params.cellSize
                    );
                } else {
                 
                     if( params.image ) {
                        if( params.image ) { 
                            var val = data.val - 1,
                                x   = val % params.size,
                                y   = val / params.size << 0;

                            ctx.drawImage(imgResizeBuffer, 
                                x * params.cellSize,
                                y * params.cellSize, 
                                params.cellSize, 
                                params.cellSize,
                                data.cell * params.cellSize,
                                data.row * params.cellSize, 
                                params.cellSize, 
                                params.cellSize
                            );
                        }
                    } else {
                       strokeRect(data);
                    }
                }
              
               if( !params.image ) {
                   ctx.fillText(    
                           data.val,
                           (data.cell * params.cellSize) + (params.cellSize/2), 
                           (data.row * params.cellSize) + (params.cellSize/2) 
                   );
               }
        },

        DrawScene : function() {
            cell = 0;
            row = -1;

            this.QuickClear(canvas);

            for( var i = 0; i < listLength; i++ ){
                cell++;
                
                if( (i + 1) % params.size - 1 === 0 ){
                    cell = 0;
                    row++;
                };

                this.DrawCell({
                    cell: cell,
                    row: row,
                    val: gamelist[i].val
                });
            };
        },

        CellMoveAnimation: function(from, to) {
            var this_     = this,
                startX    = from.cell * sz,
                startY    = from.row * sz,
                endX      = to.cell * sz,
                endY      = to.row * sz,
                dir       = null,
                distance  = 0,
                step      = null,
                stepCounter = 0,
                stepsNum  = 5,
                cellBg    = imgResizeBuffer;

            if( startX !== endX ) {
                dir = 'moveX';
                distance = endX - startX;
                setPos(distance, 'x');
            } else {
                dir = 'moveY';
                distance = endY - startY;
                setPos(distance, 'y');
            };

            setAnimationX(canvas.offsetLeft + Math.min(startX, endX));
            setAnimationY(canvas.offsetTop + Math.min(startY, endY));

            step = distance / stepsNum << 0;


            /* If we use image for background - create buffer for draw cell image */
            if( params.image ) {
                var val = to.val - 1,
                    x   = val % params.size,
                    y   = val / params.size << 0;
                
                if( !cellBuffer ) {
                    cellBuffer = document.createElement('canvas');
                    cellBufferCtx = cellBuffer.getContext('2d');
                }

                cellBufferCtx.drawImage(cellBg, 
                    x * params.cellSize,
                    y * params.cellSize, 
                    params.cellSize, 
                    params.cellSize,
                    0,
                    0, 
                    params.cellSize, 
                    params.cellSize
                );

                cellBg = cellBuffer;
            };


            timer.start({
                name: 'cellAnimation',
                action: function() {
                   
                    /* Set correct cell moving direction */ 
                    if( stepCounter !== distance ) {
                        stepCounter += step;
                        dir === 'moveX' ? posX += step : posY += step;
                    } else {
                        this_.DrawScene();
                        animationCanvas.width = 0;
                        return false;
                    };

                    this_.QuickClear(animationCanvas);

                    this_.SetCanvasStyles(animationCtx);

                    /* Draw cell */
                    animationCtx.drawImage(cellBg, 0, 0, sz, sz, posX - szHalf, posY-szHalf, sz, sz);
                    animationCtx.strokeRect(posX-szHalf, posY-szHalf, sz, sz); 

                    /* Make previous cell clear */
                    ctx.fillStyle = params.styles.emptyCellBg;
                    ctx.fillRect(from.cell * sz,from.row * sz, sz, sz);
                    
                    /* Draw number */
                    if( !params.image ) {
                        animationCtx.fillText(to.val, posX, posY);
                    }
                }
            })

        }
    };
};

