var Game = function(params) {
    
    var that       = this,
        gamelist   = {}, 
        size       = params.size,
        listLength = size * size,
        row        = -1,
        cell       = 0,
        canvas     = null,
        ctx        = null,
        timerDOM    = null,
        gameClock  = null;
   
    params.timer = new Timer();

    /* START define canvas */
    canvas = document.createElement('canvas');
    canvas.className = 'BarleyBreak';
    params.appendTo.appendChild(canvas);
    ctx = canvas.getContext('2d');

    canvas.setAttribute('width', params.cellSize * params.size);
    canvas.setAttribute('height', params.cellSize * params.size);
 
    var SetPosition = function() {
        canvas.style.position = 'absolute';
        canvas.style.left = (params.appendTo.offsetWidth - canvas.width) / 2 + 'px';
        canvas.style.top = (params.appendTo.offsetHeight - canvas.height) / 2 + 'px';
    }

    
    /* If want our game will centralizing */ 
    if( params.position && params.position === 'center' ) {
        SetPosition();
    }
    /* END define canvas */


    /* Create canvas shadow */
    var shadow = document.createElement('div');
    shadow.className = 'game-shadow';
    shadow.style.left = canvas.offsetLeft + 'px';
    shadow.style.top = canvas.offsetTop + 'px';
    shadow.style.width = canvas.width + 'px';
    shadow.style.height = canvas.height + 'px';
    params.appendTo.appendChild(shadow);

    var SetShadowPosition = function() {
        shadow.style.position = 'absolute';
        shadow.style.left = (params.appendTo.offsetWidth - canvas.width) / 2 + 'px';
        shadow.style.top = (params.appendTo.offsetHeight - canvas.height) / 2 + 'px';
    }


    /* Create timer DOM */
    var CalcClockPosition = function() {
        if( timerDOM ) {
            timerDOM.style.left = (canvas.offsetLeft + (canvas.offsetWidth/2) - (timerDOM.offsetWidth/2)) + 'px';
            timerDOM.style.top = (canvas.offsetTop - timerDOM.offsetHeight) + 'px';
        }
    };
    
    var createClock = function() {
        timerDOM = stringToNODE(Tmpl.timer);
        params.appendTo.appendChild(timerDOM);

        CalcClockPosition();

        gameClock = new Clock(
            params.timer,
            document.getElementById('barleyTimerSeconds'), 
            document.getElementById('barleyTimerMinutes')
        );
    };

    if( params.time ) {
        createClock();
    }
    
    var GRAPHICS = new Graphics(params, canvas, ctx, gamelist),
        gameX    = null,
        gameY    = null;

    /* Mix cells in game grid */
    var Mix = function(){
        var cell        = gamelist[listLength - 1],
            neigborList = [],
            rand        = null,
            randCell    = null;

        for( var i = 0; i < params.mixDeep; i++ ) {
          neigborList = [];

          for( var n in cell.neigbor ) {
            if( cell.neigbor[n] ) {
                neigborList.push(cell.neigbor[n]);
            };
          };

          rand = Math.random() * (neigborList.length)  >> 0; 
          randCell = gamelist[neigborList[rand]]; 
          
          cell.empty = false;
          randCell.empty = true;

          cell.val = randCell.val;
          randCell.val = '';
          
          cell = gamelist[neigborList[rand]];
        };
    };

    /* Check do we win or no */
    var isWin = function() {
        if( gamelist[0].val === 1 ){
            for( var i = 0; i < listLength - 1; i++ ){
                if( gamelist[i].val - 1 !== i ){
                    return false;
                };
            };
        } else {
            return false;
        };
        return true;
    };


    /* Fill game list  */
    for( var i = 0; i < listLength; i++ ){
        cell++;
        
        if( (i + 1) % size - 1 === 0 ){
            cell = 0;
            row++;
        };

        gamelist[i] = {
            index: i,
            row: row,
            cell: cell,
            originX: row,
            originY: cell,
            val: i < listLength - 1 ? i + 1: '',
            neigbor: {
                top: row - 1 >= 0 ? i - size : null,
                right: cell + 1 <= size - 1 ? (i + 1) : null,
                bottom: row + 1 <= size - 1 ? (i + size) : null,
                left: cell - 1 >= 0 ? (i - 1) : null
            },
            empty: i < listLength - 1 ? false : true
        };
    };



    /* Mix all data */
    if( isWin() ){
        Mix();
    };

    /* Draw game scene */
    EVENTER.on('resource.load', function() {
        GRAPHICS.DrawScene();
    })

    /* Start game again when player close popup */
    Popup.onHide(function() {
        Mix();
        GRAPHICS.DrawScene();
    });

    /* Handler for game plot click */
    canvas.addEventListener('click', function(e) {
        gameX = e.pageX - getOffset(canvas).left;
        gameY = e.pageY - getOffset(canvas).top;

        var cell = gameX / params.cellSize << 0,
            row  = gameY / params.cellSize << 0,
            num  = params.size * row + cell,
            item = gamelist[num],
            empty = null;

            for( var n in item.neigbor ) {
                if( item.neigbor && item.neigbor[n] !== null ) {
                    if( gamelist[item.neigbor[n]].empty === true ){
                        empty = gamelist[item.neigbor[n]];

                        item.empty = true;
                        empty.empty = false;

                        empty.val = item.val;
                        item.val = '';
                       
                        GRAPHICS.CellMoveAnimation(item, empty);
                    };
                };
            };

            /* Show victory popup */
            if( isWin() ){
                EVENTER.trigger(Popup, 'win');
            };

    }, false);

    return {
        destroy: function() {

            //Clear DOM
            params.appendTo.innerHTML = '';
       
        },
        calcPosition: function(){
            SetPosition();
            SetShadowPosition();
            CalcClockPosition();
        },
        timer: params.timer,
        canvas: canvas,
        clock: gameClock
    }
};

