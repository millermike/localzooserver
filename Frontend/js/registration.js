window.addEventListener('load', function() {
    var regBlock = id('regcaptcha'),
        regSuccess = id('regMessageSuccess'),
        regError = id('regMessageError'),
        login = id('reglogin'),
        pass = id('regpassword'),
        repass = id('regpasswordrep'),
        save = id('regsave');

    //Get captcha data
    $.ajax({
        type: 'POST',
        url: '/Captcha',
        dataType: 'json',
        success: function(res) {

           regBlock.innerHTML = '';

            var captcha = new Captcha({
                res: res.data,
                parent: regBlock,
                cellSize: 30,
                fontSize: 14
            });

            save.addEventListener('click', function() {
                if( login.value && pass.value && repass.value ) {
                    $.ajax({
                        type: 'POST',
                        url: '/Registration',
                        dataType: 'json',
                        data: {
                            login: login.value,
                            password: pass.value,
                            repassword: repass.value,
                            captcha: captcha.get().toString()
                        },
                        success: function(data){
                            if( data.status !== 'error' ) {
                                regError.innerHTML = '';
                                regSuccess.innerHTML = 'Account was successfuly created.' 
                                
                                regBlock.innerHTML = '';

                                captcha = new Captcha({
                                    res: res.data,
                                    parent: regBlock,
                                    cellSize: 30,
                                    fontSize: 14
                                });

                                setTimeout(function() {
                                    window.location.pathname = '/';
                                }, 1000);

                            } else {
                                regSuccess.innerHTML = '';
                                regError.innerHTML = data.data;
                            }
                        }
                    });
                }
            });
        }
    });

});
