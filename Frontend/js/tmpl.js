var GAMESERVICE_TMPL = {
    game: function(data){
        var status = '';

        if( data.status === 'development' ) {
            status = '<div class="game-item__status">In development</div>';
        }

        return "<a class='game-item' href='#"+ data.name +"' data-game='"+ data.name +"'>" + status +
                   "<img src='"+ data.thumb +"' alt='"+ data.thumb_alt +"' title='"+ data.title +"'>"+
                   "<div class='game-item__info'>"+ data.title +"</div>"+
                 "</a>";
    }
}
