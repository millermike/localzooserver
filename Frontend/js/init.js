var GAMEBLOCK;
    transEndEventNames = {
        'Chrome' : 'webkitTransitionEnd',
        'Safari' : 'webkitTransitionEnd',
        
        'Firefox' : 'transitionend', 
        
        'IE' : 'transitionend',
        'Opera' : 'transitionend'
    };

window.addEventListener('load', function() {

    GAMEBLOCK = document.getElementsByClassName('content__game-block')[0];

    BrowserDetect.init();

    var loginButtonStatus, 
        loginPopup,
        gamesListWrapper = document.getElementsByClassName('content__list')[0],
        loginButton = document.getElementsByClassName('login__button')[0],
        loginSend = document.getElementById('signin'),
        loginInput = document.getElementById('login'),
        passwordInput = document.getElementById('password'),
        loginError = id('loginError'),
        hash = window.location.hash,
        isHash = !!hash;

    if( !isHash ) {
        $.ajax({
            type: 'POST',
            url: '/GetGamesList',
            dataType: 'json',
            success: function(data){
                createGamesList(data);
            }
        });
    } else {
        $.ajax({
            type: 'POST',
            url: '/GetGame',
            dataType: 'json',
            data: {
                name: hash.substr(1, hash.length)
            },
            success: function(data) {
                if( !!data.error ) {        
                    window.location.hash = '';
                    window.location.pathname = '/'
                } else {
                    openGame({
                        game: data.game.name,
                        data: data.game
                    });
                }
            }
        });
    }

    function createGamesList(data) {
        data.games.forEach(function(item) {
            var game = stringToNODE(GAMESERVICE_TMPL.game(item));
            gamesListWrapper.appendChild(game);
            setGameEvents(game, item); 
        });
    };

    function setGameEvents(node, params) {
        node.addEventListener('click', function(e) {

            //Hide login popup if it open 
            loginButtonStatus = 0;
            toggleLoginPopup();
          
            openGame({
                game: node.dataset.game,
                data: params
            });
        }, false);
    };

    function openGame(params) {
        var game = params.game,
            data = params.data;

        addClass(gamesListWrapper, 'content__list_move');

        gamesListWrapper.addEventListener(transEndEventNames[BrowserDetect.browser], function() {
            removeClass(spinner, ' spinner_hide');
            loadScript(data.startScript);
        }, false);
    };

    if( loginButton ) {
	    loginButton.addEventListener('click', function() {
            loginButtonStatus ^= 1;
            toggleLoginPopup();
	    }, false);
    }

    loginSend.addEventListener('click', function() {
        var o,
            login = loginInput.value,
            password = passwordInput.value;

        if( login && password ) {
            $.ajax({
                type: 'POST',
                url: '/Auth',
                dataType: 'json',
                data: 'login=' + login +'&pass='+ password,
                success: function(res) {
                    if( res.status === '200' ) {
                        window.location.pathname = '/';
                    } else {
                        loginError.innerHTML = res.data; 
                    }
                }
            });
        }
    }, false);

    //Show-Hide login popup
    function toggleLoginPopup() {
       if( loginButton ) {
	       if(!loginPopup) loginPopup = loginButton.parentNode;
	       loginPopup.setAttribute('visibility', loginButtonStatus);
       }
    }
    

}, false);


