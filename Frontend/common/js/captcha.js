function Captcha(o) {
    var cellIndex, cell, canvas, ctx, animcanvas, animctx, cellUnder,
        timer = new Timer(30);
        cells = [],
        cellSize = o.cellSize,
        fontSize = o.fontSize,
        border = 1,
        dragStart = false;
        cursorDiff = 0,
        res = o.res,
        cellAndBorder = cellSize + 1,
        cellSizeHalf = cellSize / 2,
        resLn = res.length,
        cWidth = cellSize * resLn + (border * resLn + 1);
    
    /* Constructor */
    canvas = document.createElement('canvas');
    ctx = canvas.getContext('2d');
    canvas.width = cWidth;
    canvas.height = cellSize;
    canvas.className = 'zoogames-captcha';
    o.parent.appendChild(canvas);

    animcanvas = document.createElement('canvas');
    animctx = animcanvas.getContext('2d');
    animcanvas.width = cWidth;
    animcanvas.height = cellSize;
    animcanvas.style.top = canvas.offsetTop + 'px';
    animcanvas.style.left = canvas.offsetLeft + 'px';
    animcanvas.className = 'zoogames-captcha-animation';
    o.parent.appendChild(animcanvas);
    
    ctx.textBaseline = animctx.textBaseline = "middle";
    ctx.textAlign = animctx.textAlign = 'center';
    ctx.font = fontSize + "px Arial";
    animctx.font = fontSize + "px Arial";
    
    /* Generate cells */
    res.forEach(function(cell, i) {
        var cell = new Cell({
            size: cellSize,
            val: cell,
            y: 0,
            x: (cellSize * i) + (border * (i + 1)),
            display: true
        });
        cells.push(cell);
        drawCell({cell: cell, ctx: ctx, bgColor: '#CCC', textColor: '#000'});
    });

    document.addEventListener('mousedown', function(e) {
        if( e.target === animcanvas ) {
            cellIndex = (e.offsetX || e.layerX) / cellAndBorder << 0;
            cell = cells[cellIndex];
            cell.display = false;

            clearCanvas(animctx);
            
            drawAllCells({ ctx: ctx, bgColor: '#CCC', textColor: '#000'});

            cell.display = true;
           
            drawCell({ cell: cell, ctx: animctx, bgColor: '#666', textColor: '#FFF'});
            
            dragStart = true;
        }
    }, false);

    animcanvas.addEventListener('mousemove', function(e) {
        if( dragStart ) {
            if( !cursorDiff ) cursorDiff = (e.offsetX || e.layerX)- cell.x; 

            cell.x = (e.offsetX || e.layerX) - cursorDiff;
            
            clearCanvas(animctx);

            drawCell({cell: cell, ctx: animctx, bgColor: '#666', textColor: '#FFF'});
        }
    }, false);
    
    document.addEventListener('mouseup', function(e) {
        var tempArr, cellUnder, tempCellValue, tempUnderValue, cellUnderIndex,
            dir, cellX, underX, tempAnimCell,
            step = 5;
        
        if( e.target === animcanvas && cell ) {
            cellUnderIndex = (e.offsetX || e.layerX) / cellAndBorder << 0;
            cellUnder = cells[cellUnderIndex];
        }

        dragStart = false;
        cursorDiff = 0;

        if( cell ) cell.x = (cellSize * cellIndex) + (border * (cellIndex + 1));

        if( cell && cellUnder ) {
            dir = cellUnder.x < cell.x;
           
            //Get array of moving cells 
            if( dir ) {
                tempArr = cells.slice(cellUnderIndex, cellIndex);
            } else {
                tempArr = cells.slice(cellIndex + 1, cellUnderIndex + 1);
            }

            //Draw dragged cell at new place
            cell.x = cellUnder.x;
            drawCell({
                cell: cell,
                ctx: ctx,
                bgColor: '#CCC',
                textColor: '#000',
                border: true
            });
           
            //Animate moving cells
            if( cell !== cellUnder ) { 
                timer.start({
                    name: 'cellMove',
                    action: function() {
                        var status = true;

                        clearCanvas(animctx);
                        
                        tempArr.forEach(function(cell) {
                            if( dir ) {
                                cell.x += step;
                                if( cell.x >= cellIndex * cellSize ) status = false;
                            } else {
                                cell.x -= step;
                                if( cell.x <= cellIndex * cellSize ) status = false;
                            }
                            drawCell({
                                cell: cell,
                                ctx: animctx,
                                bgColor: '#CCC',
                                textColor: '#000',
                                border: true
                            });
                        });

                        return status;
                    },
                    destroy: function() {

                        //Replace cells in array
                        if( dir ) {
                            cells = cells.slice(0, cellUnderIndex).concat(cells.splice(cellIndex, 1), cells.slice(cellUnderIndex, cells.length));
                        } else {
                            cells = cells.slice(0, cellIndex).concat(tempArr, cell, cells.slice(cellUnderIndex + 1, cells.length));
                        }

                        //Reset coordinates
                        var cells_ = [];
                        cells.forEach(function(cell, i) {
                            cell.x = (cellSize * i) + (border * (i + 1));
                            cells_.push(cell);
                        });
                        
                        cells = cells_;

                        //Redraw cell
                        drawAllCells({ctx: ctx, bgColor: '#CCC', textColor: '#000'});
                        clearCanvas(animctx);
                        cell = null;
                    }
                });
            } else {
                clearCanvas(animctx);
                drawAllCells({ctx: ctx, bgColor: '#CCC', textColor: '#000'});
                cell = null;
            }
        } else {
            clearCanvas(animctx);
            drawAllCells({ctx: ctx, bgColor: '#CCC', textColor: '#000'});
            cell = null;
        }
    }, false);


    function Cell(o) {
        this.width = cellSize,
        this.height = cellSize,
        this.value = o.val,
        this.x = o.x,
        this.y = o.y;
        this.display = o.display;
    };

    function drawCell(o) {
        o.ctx.fillStyle = o.bgColor;
        o.ctx.fillRect(o.cell.x, o.cell.y, o.cell.width, o.cell.height);
        
        if( o.cell.display ) {
            o.ctx.fillStyle = o.textColor;
            o.ctx.fillText(o.cell.value, o.cell.x + cellSizeHalf, cellSizeHalf);

            if( o.border ) {
                o.ctx.strokeStyle = '#999';
                o.ctx.strokeRect(o.cell.x, o.cell.y - border, o.cell.width, o.cell.height + (border * 2));
            }
        }
    } 

    function drawAllCells(o) {
        cells.forEach(function(cell) {
            o.cell = cell;
            drawCell(o);
        });
    }

    function clearCanvas(ctx) {
        ctx.clearRect(0,0, (cellSize + border) * res.length, cellSize);
    }

    return {
        get: function() {
            var result = [];
            cells.forEach(function(cell) {
                result.push(cell.value);
            });
            return result;
        }
    }
}
