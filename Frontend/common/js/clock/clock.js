/**
 *
 * @param {Object} timer Timer class object
 * @param {Node} secondsDOM DOM of seconds block 
 * @param {Node} minutesDOM DOM of minutes block 
 * @param {Node} hoursDOM DOM of hours block 
 *
 * */
var Clock = function(timer, secondsDOM, minutesDOM, hoursDOM, defaultTime) {
    
    var _t = new Date().getTime();

    var defaultTime = defaultTime || {};

    var _seconds = defaultTime.seconds || 0,
        _minutes = defaultTime.minutes || 0,
        _hours = defaultTime.hours || 0;

    var timerStatus = true;

    //Set start points
    if( secondsDOM ) secondsDOM.innerHTML = _seconds < 10 ? '0' + _seconds : _seconds;
    if( minutesDOM ) minutesDOM.innerHTML = _minutes < 10 ? '0' + _minutes : _minutes;
    if( hoursDOM ) hoursDOM.innerHTML = _hours < 10 ? '0' + _hours : _hours;
 
    function setTimer() { 
        timer.start({
            name: 'clock' + _t,
            _secMillSec: 1000,
            _startTime: _t,
            _currentTime: null,
            _timeDiff: null,
            _sysSeconds: 0,
            action: function() {
                this._currentTime = new Date().getTime();

                this._timeDiff = this._currentTime - this._startTime;

                if( this._timeDiff >= this._secMillSec * (this._sysSeconds + 1) ) {
                    this._sysSeconds++;

                    _seconds++;
                    if( _seconds === 60 ) _seconds = 0;
                    if( secondsDOM ) secondsDOM.innerHTML = _seconds < 10 ? '0' + _seconds : _seconds;
                }

                if( this._sysSeconds % 60 === 0 ) {
                    _minutes = this._sysSeconds / 60;
                    if( minutesDOM ) minutesDOM.innerHTML = _minutes < 10 ? '0' + _minutes : _minutes;
                } 

                if( this._sysSeconds % 3600 === 0 ) {
                    _hours = this._sysSeconds / 3600;
                    if( hoursDOM ) hoursDOM.innerHTML = _hours < 10 ? '0' + _hours : _hours;
                }

                return timerStatus; 
            }
        });
    };

    setTimer();

    return {
        stop: function() {
            timerStatus = false; 
        },
        resume: function() {
            _t = new Date().getTime();
            timerStatus = true;
            setTimer();   
        }
    } 
};
