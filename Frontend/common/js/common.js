function getOffsetSum(elem, border) {
    var top = 0,
        left = 0;

    while(elem) {
        if( elem ) {
            top += elem.offsetTop;
            left += elem.offsetLeft;
            if( border ) {
                top -= parseInt(document.defaultView.getComputedStyle(elem,null).border.split(' ')[0]);
                left -= parseInt(document.defaultView.getComputedStyle(elem,null).border.split(' ')[0]);
            };
        }
        elem = elem.offsetParent;
    };

    return {
        top: top,
        left: left
    };
}


function getOffset(elem) {
    var box = elem.getBoundingClientRect();

    return {
        top: box.top,
        left: box.left
    };
}

function id(name) {
    return document.getElementById(name);
}

/* Must be a one NODE */
function stringToNODE(text) {
    var temp = document.createElement('div');
    temp.innerHTML = text;

    return temp.firstChild;
}

function removeClass(node, cls) {
    node.className = node.className.replace(cls, '');
}

function addClass(node, cls) {
    node.className += ' ' + cls;
}

function replaceClass(node, r, cls) {
    node.className = nodeClassName.replace(r, cls);
}

function hasClass(node, cls) {
    return !!~node.className.indexOf(cls);
}

function JSONP(params) {
    var script = document.createElement('script');
    script.onload = function() {
        document.body.removeChild(script);
    };
    script.src = params.url;
    document.body.appendChild(script)
}

function loadCSS(url, callback) {
    var link = document.createElement("link");
    link.onload = function() {
        if(callback) callback(link);
    };
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}

function loadScript(url, callback) {
    var script = document.createElement("script");
    script.onload = function() {
        if( callback ) callback(script);
    };
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}
