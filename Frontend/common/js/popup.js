var Popup = null;

/* Create popup DOM element */
var p = document.createElement('div');
p.id = 'popup';
p.className = 'win-popup';
p.innerHTML = '<div id="popup_close" class="close">x</div>'+
              '<span>You win!</span>';
document.body.appendChild(p);

/* Create dark screen layout */
var dark = document.createElement('div');
dark.id = 'dark_screen';
dark.className = 'dark';
document.body.appendChild(dark);


/* Return popup methods */
Popup = (function() {
    var close = document.getElementById('popup_close'),
        _hide,
        _show,
        _onHideFn;

    _hide = function() {
        dark.style.display = 'none'
        p.style.display = 'none';
    };

    _show = function() {
        dark.style.display = 'block'
        p.style.display = 'block';
    };

    close.addEventListener('click', function() {
        _hide();
        if( _onHideFn ) _onHideFn();
    }, false);


    /* Popup API  */
    return{
        setContent: function(html) {
            p.childNodes[1].innerHTML = html;
        },
        onHide: function(fn) {
            _onHideFn = fn;
        },
        show: _show
    }

}());




