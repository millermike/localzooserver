function Game(params) {
    var colors = params.colors;

    this.SCORE = 0;

    this.scoreDOM = id('score');  

    this.scoreValDOM = id('scoreVal');  

    this.totalScoreDOM = id('totalScore');

    this.randomColls = [];

    this.map = {};

    this.lock = false;

    this.params = params;

    this.weapon = new Weapon(this);

    this.sounds = {
        delete: new Audio('/Balls/sounds/delete_ball.ogg'),
        bomb: new Audio('/Balls/sounds/bomb.mp3'),
        shot: new Audio('/Balls/sounds/shot.mp3')
    };
  
    var that = this; 
    
    //Generate map collumns height
    for( var i = 0; i < this.params.hor; i++ ) {
        this.randomColls.push((Math.random() * (this.params.maxVert - this.params.minVert) + this.params.minVert) << 0);
    }; 

    //Generate map
    for( var row = 0; row < params.hor; row++ ) {
        for( var col = 0; col < this.randomColls[row]; col++ ) {
            this.map[row + '_' + col] = new Cell({
                x: row,
                y: col,
                id: row + '_' + col,
                color: colors[Math.random() * colors.length << 0]
            });
        }
    }

    //Draw game plot
    this.CreatePlot();

    //Detect mouse position
    this.DetectMousePosition();

    //Use weapon
    this.WeaponUse();
}

/* Draw game plot */
Game.prototype.CreatePlot = function() {
    var that = this;

    this.plot = id('plot');
    this.plot.style.height = (this.params.vert * this.params.cell.height) + 'px'
        
    this.plot.style.width = (this.params.hor * this.params.cell.width) + 'px'
    
    Object.keys(this.map).forEach(function(cell) {
        that.DrawCell(that.map[cell], cell);
    }); 
};

/* Create game cell */
Game.prototype.DrawCell = function(p, key, arr, addToParent) {
    var cell = document.createElement('div');
    cell.className = 'cell cell_create_animation cell_color_' + p.color;
    cell.style.left = (this.params.cell.width * p.x) + 'px';
    cell.style.top = (this.params.cell.height * (this.params.vert - p.y - 1)) + 'px';
    cell.style.width = this.params.cell.width + 'px';
    cell.style.height = this.params.cell.height + 'px';
    cell.style.borderRadius = this.params.cell.width + 'px';
    cell.id = key;

    if( addToParent !== false ) {
        this.plot.appendChild(cell);
    }
    if( arr ) {
        arr[key].dom = cell;
    } else {
        this.map[key].dom = cell;
    }
};

/* Detect mouse position */
Game.prototype.DetectMousePosition = function() {
    var that = this,
        current,
        neigbors = [],
        that = this,
        diff = 15;

    /* Show score */
    that.MOVELISTENER = that.plot.addEventListener('mousemove', function(e) {
        that.scoreDOM.style.top = (e.pageY - getOffsetSum(that.plot).top - diff) + 'px';
        that.scoreDOM.style.left = (e.pageX - getOffsetSum(that.plot).left - diff) + 'px';
    });
    
    /**
     * Find cell neigbors
     * @params {Object} current Object whitch around cell must be found
     * @return {Object} Extended neigbors array
     */
    function findNeigbors(current) {
        if( current ) {
            var topN, rightN, bottomN, leftN;
           
            topN = that.map[current.x+ '_' +(current.y - 1)];
            bottomN = that.map[current.x+ '_' +(current.y + 1)];
            rightN = that.map[(current.x + 1)+ '_' +current.y];
            leftN = that.map[(current.x - 1)+ '_' +current.y];

            if( topN && neigbors.indexOf(topN.id) === -1) {
                if( topN.color === current.color ) {
                    neigbors.push(topN.id);   
                    findNeigbors(that.map[topN.id]);
                } 
            }
            if( rightN && neigbors.indexOf(rightN.id) === -1) {
                if( rightN.color === current.color) {
                    neigbors.push(rightN.id);  
                    findNeigbors(that.map[rightN.id]);
                }   
            }
            if( bottomN && neigbors.indexOf(bottomN.id) === -1) {
                if( bottomN.color === current.color ) {
                    neigbors.push(bottomN.id);
                    findNeigbors(that.map[bottomN.id]);
                }     
            }
            if( leftN && neigbors.indexOf(leftN.id) === -1) {
                if( leftN.color === current.color ) {
                    neigbors.push(leftN.id);
                    findNeigbors(that.map[leftN.id]);
                }
            };
        }

        return neigbors;
    };

    /**
     * Find cell above removing cells
     * @params {Number} i Cell iterator(x position)
     * @params {String} key Cell iterator(y position)
     */
    function getBallsAbove(i, key, cols, parent, ballsAbove, offset) {
        var up = that.map[key+'_'+(cols[key][0] - 0 + i)];

        if( up ) {
            if( cols[up.x].indexOf(""+up.y) === -1 ) { 
                if( cols[up.x].indexOf(""+ (up.y - 1)) !== -1 ) {
                    parent = up.x+ '.' +up.y; 

                    ballsAbove[parent] = {
                        balls: [up],
                        offset: offset
                    }
                } else {
                    ballsAbove[parent].balls.push(up);
                }
            } else {
                offset++;
            }

            i++;
            getBallsAbove(i, key, cols, parent, ballsAbove, offset);
        }
    };


    /**
     * Find empty collumns
     */
    function isAnyCollFree() {
        var status,
            emptyColls = [];

        for( var i = 0; i < that.params.hor; i++ ) {
            status = false;
            for( var j = 0; j < that.params.vert; j++ ) {
                if( that.map[i+ '_' + j] ) status = true;
            };
            if( !status ) {
                emptyColls.push(i);
            };
        };

        return emptyColls;
    };


    /**
     * Move empty collumns
     */
    function moveCollumns(colls) {
        var item, id, n, temp;
        
        if( colls.length > 1 ) {
            colls = colls.sort(function(a, b) {
                return b - a;
            }); 
        };

        colls.forEach(function(col, i) {
            temp = {};

            Object.keys(that.map).forEach(function(key) {
                item = that.map[key];
                
                if( item.x < col + i ) {
                    id = (item.x + 1)+ '_' +item.y; 

                    item.dom.style.left = (parseInt(item.dom.style.left) + that.params.cell.width) + 'px'; 

                    temp[id] = Object.create(item);
                    temp[id].x += 1;
                    temp[id].id = id;
                    temp[id].dom.id = id;
                } else {
                    temp[key] = Object.create(item);
                }
            });

            that.map = temp;
        }); 
    };


    /**
     * Create new collumn
     */
    function createCollumns(cols) {
        var wrapper, fragment;

        wrapper = document.createElement('div');
        wrapper.className = 'cells-wrapper';

        cols.forEach(function(col, iterator) {
            var hg = (Math.random() * (that.params.maxVert - that.params.minVert) + that.params.minVert) << 0;
            
            for( var i = 0; i < hg; i++ ) {
                var cell = new Cell({
                    x: iterator,
                    y: i,
                    id: iterator + '_' + i,
                    color: that.params.colors[Math.random() * that.params.colors.length << 0]
                });

                that.map[cell.id] = cell;

                that.DrawCell(cell, cell.id, that.map, false);

                wrapper.appendChild(cell.dom);
            }
        });

        that.plot.appendChild(wrapper);

        //Add cells to DOM
        that.lock = true;

        setTimeout(function(){
            addClass(wrapper, 'cell-wrapper-show');

            wrapper.addEventListener(transEndEventNames[BrowserDetect.browser], function() {
                fragment = document.createDocumentFragment();
                Array.prototype.slice.apply(wrapper.children).forEach(function(cell) {
                     fragment.appendChild(cell);
                });
                that.plot.appendChild(fragment);
                that.plot.removeChild(wrapper);
                that.lock = false;
            });
        }, 1);
    };


    /**
     * Move all balls above down
     */
    function moveBallsDown(ballsAbove, arr, cellHg) {
        var cell;

        Object.keys(ballsAbove).forEach(function(key) {
            ballsAbove[key].balls.forEach(function(item) {

                delete arr[item.id];

                cell = new Cell({
                    x: item.x,
                    y: (item.y - ballsAbove[key].offset),
                    id: item.x + '_' + (item.y - ballsAbove[key].offset),
                    color: item.color
                });

                cell.dom = item.dom;
                cell.dom.id = cell.id;
                arr[cell.id] = cell;

                cell.dom.style.top = parseInt(cell.dom.style.top) + (ballsAbove[key].offset * cellHg) + 'px';
            });
        });
    };

    /**
     * Sort cells by collumns
     */
    function sortCellsByCollumns(cells) {
        var cell_,
            cols = {};

        cells.forEach(function(cell) {
            cell_ = cell.split('_');
            if( !cols[cell_[0]] ) {
                cols[cell_[0]] = [cell_[1]];
            } else {
                cols[cell_[0]].push(cell_[1]);
            }
        });

        return cols;
    };

    /**
     * Find and move empty collumns, create new collumn and check on loose
     */
    function MoveEmptyCollsAndCreateNew(balls, hg) {
        var emptyColls;

        moveBallsDown(balls, that.map, hg);
        
        emptyColls = isAnyCollFree();

        if( emptyColls.length ) {
            moveCollumns(emptyColls);
            createCollumns(emptyColls);
        };
        
        isLoose(); 
    };

    /**
     * Is game finished
     */
    function isLoose() {
        var status = true;
            dir = [[0, 1], [0, -1], [1, 0], [-1, 0]];

        var iterator = 0;
        Object.keys(that.map).forEach(function(cell) {
            dir.forEach(function(d) {
                if( that.map[(that.map[cell].x + d[0]) + '_' + (that.map[cell].y + d[1])] ) {
                    if(that.map[(that.map[cell].x + d[0]) + '_' + (that.map[cell].y + d[1])].color === that.map[cell].color) {
                        status = false;
                    }
                }
            });
        }); 
        
        if( status ) alert('Вы проиграли.');
    };

    /**
     * Show cells score
     */
    function score() {
        if( neigbors.length < that.params.maxScore ) {
            that.CURRENT_SCORE = that.params.scores[neigbors.length];
        } else {
            that.CURRENT_SCORE = that.params.scores[that.params.maxScore];
        }

        that.scoreValDOM.innerHTML = that.CURRENT_SCORE;
        addClass(that.scoreDOM, 'score-show');
    }

    
    /**
     * Hide score block
     */
    function hideScorePopup() {
        that.plot.removeEventListener('mousemove', that.MOVELISTENER);
        removeClass(that.scoreDOM, 'score-show');
    }


    function countScore() {
        that.SCORE += that.CURRENT_SCORE;
        that.totalScoreDOM.innerHTML = that.SCORE;
    }

        
    /**
     * Find cell neigbors on mouseover
     */
    this.plot.addEventListener('mouseover', function(e) {
        neigbors = [];
        if( e.target.className.indexOf('cell') !== -1 ) {
            current = that.map[e.target.id];
            findNeigbors(current);
            neigbors.forEach(function(cell) {
                that.map[cell].dom.className += ' cell_selected';
            });

            if( neigbors.length > 1 ) score();
        }
    });
   
    /**
     * Reset cell neigbors
     */ 
    this.plot.addEventListener('mouseout', function(e) {
        if( e.target.className.indexOf('cell') !== -1 ) {
            neigbors.forEach(function(cell) {
                that.map[cell].dom.className = that.map[cell].dom.className.replace(' cell_selected', '');
            });
            hideScorePopup();
        }
    });
    
    this.plot.addEventListener('click', function(e) {
        if( !that.lock ) {
            if( neigbors.length &&e.target.className.indexOf('cell') !== -1 ) {
                
                var parent, offset, up, cell, cols,
                    ballsAbove = {};

                cols = sortCellsByCollumns(neigbors);
                
                Object.keys(cols).forEach(function(key) {
                    cols[key] = cols[key].sort(function(a, b) {return a - b});
                    offset = 0;
                    getBallsAbove(0, key, cols, parent, ballsAbove, offset);
                });

                //Remove balls
                neigbors.forEach(function(cell) {
                    that.plot.removeChild(that.map[cell].dom);
                    delete that.map[cell];
                });

                that.sounds.delete.play();
                
                hideScorePopup();
                countScore();
                
                MoveEmptyCollsAndCreateNew(ballsAbove, that.params.cell.height);

                that.weapon.getItem(neigbors.length);
            }
        }
    });

    //Public
    this.getBallsAbove = getBallsAbove;
    this.isAnyCollFree = isAnyCollFree;
    this.moveBallsDown = moveBallsDown;
    this.moveCollumns = moveCollumns;
    this.sortCellsByCollumns = sortCellsByCollumns;
    this.createCollumns = createCollumns;
    this.MoveEmptyCollsAndCreateNew = MoveEmptyCollsAndCreateNew;
    this.isLoose = isLoose;  
};

Game.prototype.WeaponUse = function() {
    var weaponDOM,
        that = this,
        weaponsObject = that.params.weapon,
        weapons = id('weapons'),
        weaponBlocks = document.getElementsByClassName('game-weapon-item');

    //Generate weapon
    Object.keys(weaponsObject).forEach(function(key) {
        that.weapon.create(key, weaponsObject[key].amount, weaponsObject[key].src);
    });

    //Init weapons on click
    Array.prototype.slice.apply(weaponBlocks).forEach(function(node) {
        node.addEventListener('click', function(e) {
            weaponDOM = e.target.parentNode;
            
            if( weaponDOM.getAttribute('weapon') && that.params.weapon[weaponDOM.getAttribute('weapon')].amount > 0 ) {
                that.weapon._lock(this);
                new that.weapon[this.getAttribute('weapon')](that, e, this);
            }
        }, false)
    });
}
