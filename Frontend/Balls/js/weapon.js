function Weapon(ctx) { 
    this.ctx = ctx;
};

Weapon.prototype.create = function(name, amount, src) {
    id('weapons').innerHTML += 
        "<span class='game-weapon-item' weapon='"+ name +"' >"+
           "<div class='weapon__count'>"+amount+"</div>"+
           "<img src='"+src+"' />"+
        "</span>";
};

Weapon.prototype._lock = function(el) {
    addClass(el, 'weapon_lock_yes');
};

Weapon.prototype._unlock = function(node) {
    removeClass(node, 'weapon_lock_yes');
};

Weapon.prototype._decreaseAmount = function(weapon, node) {
    weapon.amount -= 1;
    if( weapon.amount <= 0 ) addClass(node, 'hide');
    node.getElementsByClassName('weapon__count')[0].innerHTML = weapon.amount;
}

Weapon.prototype.addItem = function(type, num) {
   var node = document.querySelectorAll('[weapon='+type+']')[0];
   if( node ) {
       this.ctx.params.weapon[type].amount += num;
       node.getElementsByClassName('weapon__count')[0].innerHTML = this.ctx.params.weapon[type].amount;
       removeClass(node, ' hide');
   };
}; 

Weapon.prototype.getItem = function(balls) {
    var that = this,
        weapons = this.ctx.params.add;
    
    Object.keys(weapons).forEach(function(type) {
        if( weapons[type] === balls ) {
            that.addItem(type, 1);
        }
    });
};

Weapon.prototype._update = function(ctx, cols, cellsAround, ballsAbove, _weapon, params, parentNode) {
    var parent, offset, up;

    if( Object.keys(cols).length ) {
        Object.keys(cols).forEach(function(key) {
            cols[key] = cols[key].sort(function(a, b) {return a - b});
            offset = 0;
            ctx.getBallsAbove(0, key, cols, parent, ballsAbove, offset);
        });

        cellsAround.forEach(function(cell) {
            ctx.plot.removeChild(ctx.map[cell].dom);
            delete ctx.map[cell];
        });

        ctx.MoveEmptyCollsAndCreateNew(ballsAbove, ctx.params.cell.height);
        ctx.weapon._decreaseAmount(params, parentNode);
    }       
    
    ctx.plot.removeChild(_weapon);
    removeClass(id('game'), 'cursor__hide');
    ctx.weapon._unlock(parentNode);
};



/**
 * BOMB
 * -can kill range of balls
 */
Weapon.prototype.bomb = function(ctx, event, parentNode) {
    var bomb, prevCell,
        params = ctx.params.weapon.bomb,
        radius = params.radius,
        distance = radius * 2,
        cellsAround = [],
        diff = 20,
        arr = ctx.map;

    //Create bomb
    bomb = document.createElement('div');
    bomb.style.top = (event.target.offsetTop + diff) + 'px';
    bomb.style.left = (event.target.offsetLeft + diff) + 'px';
    bomb.style.backgroundImage = 'url('+ params.src +')';
    bomb.className = 'weapon__bomb';

    setTimeout(function() {
        ctx.plot.appendChild(bomb);
    }, 100)
    
    //Hide cursor when we move and drop bomb
    addClass(id('game'), 'cursor__hide');
    addClass(ctx.scoreDOM, 'opacity');
    
    ctx.plot.addEventListener('mousemove', function(e) {
        var x, y, cell;

        cellsAround.forEach(function(cell, i) {
            if( arr[cell] ) {
                removeClass(ctx.map[cell].dom, ' cell_opacity');
            }
        });
        cellsAround = [];

        bomb.style.left = (e.pageX - getOffsetSum(ctx.plot).left - diff) + 'px';
        bomb.style.top = (e.pageY - getOffsetSum(ctx.plot).top - diff) + 'px';

        y = (ctx.params.vert - 1) - (bomb.offsetTop / ctx.params.cell.height) << 0;
        x = (e.pageX - getOffsetSum(ctx.plot, true).left ) / ctx.params.cell.width << 0;
        
        if( ctx.map[x+'_'+y] ) { 
            cell = x+'_'+y;

            for( var i = 0; i < distance + 1; i++ ) {
                for( var j = 0; j < distance + 1; j++ ) {
                    if( ctx.map[(x - radius+i)+'_'+(y + radius -j)] ) {
                        cellsAround.push(ctx.map[(x -radius +i)+'_'+(y + radius -j)].id);
                    };    
                };
            };

            cellsAround.forEach(function(cell) {
                addClass(ctx.map[cell].dom, 'cell_opacity');
            });
        };
    }, false);

    /* Drop bomb */
    ctx.plot.addEventListener('mouseup', function(e) {
        var parent, offset, up, cell,
            ballsAbove = {},
            cols = ctx.sortCellsByCollumns(cellsAround);
    
        ctx.sounds.bomb.play();   

        ctx.weapon._update(ctx, cols, cellsAround, ballsAbove, bomb, params, parentNode);

        removeClass(ctx.scoreDOM, 'opacity');
        
        ctx.plot.removeEventListener('mousemove', arguments.callee, false);
        ctx.plot.removeEventListener('mouseup', arguments.callee, false);
    }, false);
};


/**
 * SHOT
 * - can kill one ball
 */
Weapon.prototype.shot = function(ctx, event, parentNode) {
    var shot,
        x, y, cell,
        params = ctx.params.weapon.shot,
        radius = params.radius,
        distance = radius * 2,
        cellsAround = [],
        diff = 20,
        arr = ctx.map;

    shot = document.createElement('div');
    ctx.plot.appendChild(shot);
    shot.className = 'weapon__shot';
    shot.style.backgroundImage = 'url('+ params.src +')';
    shot.style.top = (event.target.offsetTop + diff) + 'px';
    shot.style.left = (event.target.offsetLeft + diff) + 'px';
    
    addClass(id('game'), 'cursor__hide');
    addClass(ctx.scoreDOM, 'opacity');

    var m = ctx.plot.addEventListener('mousemove', function(e) {
        if( cell ) {
            removeClass(cell.dom, 'cell_opacity');
        };

        shot.style.left = (e.pageX - getOffsetSum(ctx.plot).left - diff) + 'px';
        shot.style.top = (e.pageY - getOffsetSum(ctx.plot).top - diff) + 'px';

        y = (ctx.params.vert - 1) - (shot.offsetTop / ctx.params.cell.height) << 0;
        x = (e.pageX - getOffsetSum(ctx.plot, true).left ) / ctx.params.cell.width << 0;
        
        if( ctx.map[x+'_'+y] ) {
            cell = ctx.map[x+'_'+y];
            addClass(ctx.map[x+'_'+y].dom, 'cell_opacity');
        };
    }, false);
    
    ctx.plot.addEventListener('mouseup', function(e) {
        var parent, offset, up,
            ballsAbove = {},
            cellsAround = cell ? [cell.id] : [],
            cols = ctx.sortCellsByCollumns(cellsAround);

        ctx.sounds.shot.play();   
        
        ctx.weapon._update(ctx, cols, cellsAround, ballsAbove, shot, params, parentNode);
        
        removeClass(ctx.scoreDOM, 'opacity');
        
        ctx.plot.removeEventListener('mousemove', arguments.callee, false);
        ctx.plot.removeEventListener('mouseup', arguments.callee, false);
    }, false);
};
