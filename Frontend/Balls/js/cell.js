function Cell(params) {
    this.x = params.x;
    this.y = params.y;
    this.color = params.color;
    this.id = params.id;
}
