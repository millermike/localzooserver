new Game({
    vert: 15, //Vertical game size
    maxVert: 10,
    minVert: 5,
    hor: 24, //Horizonal number of balls
    cell: {
        width: 30,
        height: 30
    },
    colors: ['red', 'green', 'yellow', 'blue', 'grey', 'orange'],
    maxScore: 9,
    scores: {
        2: 5,
        3: 8,
        4: 13,
        5: 21,
        6: 34,
        7: 55,
        8: 89,
        9: 100
    },
    //Amout of score need to add one item
    add: {
        bomb: 7,
        shot: 6
    },
    weapon: {
        shot: {
            amount: 2,
            src: '/Balls/img/target.png'    
        },
        bomb: {
            radius: 1,
            amount: 1,
            src: '/Balls/img/_bomb.png'      
        }        
    }
});

//Stop spinner
var spinner = document.getElementsByClassName('spinner')[0];
addClass(spinner, 'spinner_hide');
