(function() {
    var HTML = "<div class='game' id='game'>"+
                    "<div class='game-total-score'>Score:<span id='totalScore'>0</span></div>"+
                    "<div class='game-plot' id='plot'>"+ 
                        "<div class='game-weapon' id='weapons'></div>"+
                    "</div>"+
                    "<div class='game-score' id='score'>"+
                        "<div class='game-score-val' id='scoreVal'></div>"+
                        "<div class='game-score-tail'></div>"+
                    "</div>"+
               "</div>";

    GAMEBLOCK.innerHTML = HTML;
}());
