(function() {

    var scripts = [
        "common/js/common.js", 
        "common/js/events.js", 
        "common/js/timer.js",
        "Balls/loadhtml.js",
        "Balls/js/game.js",
        "Balls/js/weapon.js",
        "Balls/js/cell.js",
        "Balls/js/init.js"
    ];

    var styles = [
        "Balls/css/styles.css"
    ]
   
    var indexScript = 0,
        scriptLoading = function(indexScript) {
            loadScript(scripts[indexScript], function() {
                indexScript++;
                if( indexScript < scripts.length ) {
                    scriptLoading(indexScript); 
                }
            });
        }

    var indexStyle = 0,
        styleLoading = function(indexStyle) {
            loadCSS(styles[indexStyle], function() {
                indexStyle++;
                if( indexStyle < styles.length ) {
                    styleLoading(indexStyle); 
                } else {
                    scriptLoading(indexScript);
                }
            });
        }

    styleLoading(indexStyle);

}())

