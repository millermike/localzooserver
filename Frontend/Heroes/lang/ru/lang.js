﻿var Localization = {
  "game_name": "Герои Javascript и Магии",
  "login"    : "Логин",
  "password" : "Пароль",
  "rep_pass" : "Повторите пароль",
  "submit"   : "Войти",
  "register" : "Регистрация",
  "create"   : "Создать"
};