Heroes.MapGenerator = (function(){
  
  "use strict";
  
  var th         = this,
      mpmodel    = DATA.MAPMODEL,
      units      = DATA.UNITS,
      items      = Heroes.MAP.background,
      itemsLn    = items.length,

      STATIC_OBJ  = Heroes.MAP["static"],
      DYNAMIC_OBJ = Heroes.MAP["dynamic"],
	  RES_OBJ     = Heroes.MAP["resources"],
      BUILD_OBJ   = Heroes.MAP["buildings"],
	  MOBS_OBJ    = Heroes.MAP['mobs'],
     
      url_prefix = "", 
	  players    = Heroes.MAP.players,
      el         = null,
      params     = null,
      elCls      = "map-item",
      objCls     = "map-object",
      objPCLs    = "map-object-place",
      buildCls   = "map-building",
      buildPCLs  = "map-building-place",
      plCls      = "map-player";
	
  
  /**
   * Create DOM element for game object
   * @param Object width common element properties
   * @cls String class name of object DOM element
   * @center Boolean Do we need centalize the DOM element
   */
  this.createBlock = function(o, cls, center){
    var x   = o.x, 
        y   = o.y,
        rw  = o.data ? o.data.rw : o.rw,
        rh  = o.data ? o.data.rh : o.rh;
   
	if( o.data ){
        x = o.x - ( o.data.px || 0 );
        y = o.y - ( o.data.py || 0 );
	};

    el = document.createElement("div");
    el.className = cls;
   
	if( o.data && o.data["_id"] ) {
		el.id = o.data["_id"]
	};

    //Not for objects
    if( !o.t && o.unit !== undefined ){
      el.id = "p_"+o.unit;
    };
    
    if( center ) {
      x = o.x - (((rw || o.w) - Heroes.MAP.size.minW) / 2);
      y = o.y - (((rh || o.h) - Heroes.MAP.size.minH) / 2);
    };
   
   	var source = o.s || o.data.s; 	
    if( source ){
      if( source.indexOf(".png") != -1 ){
        params = "background-image:url("+(url_prefix+source)+");width:"+(rw || o.w)+"px;height:"+(rh || o.h)+"px;top:"+y+"px;left:"+x+"px";
      }else if( source.indexOf(".gif") != -1 ){
        el.innerHTML = "<img src='"+(url_prefix+source)+"' style='width:"+(rw || o.w)+"px;height:"+(rh || o.h)+"px;'/>";
        params = "top:"+y+"px;left:"+(x - (rw-o.data.rw)/2)+"px";
      };
    }else{
        console.log("No resource file");
    };
    
    el.setAttribute("style", params);
    DATA.CACHE.get("Map").appendChild(el);

    return el;
  };
  
  

  var setControllsGeometry = function(){

	var MapBottomNargin = 53,
		MapRightMargin = 28;

    /* Set MAP wrapper size */
    DATA.CACHE.get("Wrapper").style.width = Heroes.MAP.size.width + "px";
    DATA.CACHE.get("Wrapper").style.height = Heroes.MAP.size.height + "px";

    /* Set Game field size */
    DATA.CACHE.get("Game").style.width = (window.innerWidth - MapRightMargin - DATA.CACHE.get("Sidebar").offsetWidth) + "px";
    DATA.CACHE.get("Game").style.height = (window.innerHeight - MapBottomNargin)+ "px";
  };



	/**
	 * Check on correct 'wrapper' position relative game block
	 */
	 var wrapperPositionChecker = function(){
			//Check x position
			if( -1*DATA.CACHE.get("Wrapper").offsetLeft > DATA.CACHE.get("Wrapper").offsetWidth - DATA.CACHE.get("Game").offsetWidth ){
				DATA.CACHE.get("Wrapper").style.left = -1*(DATA.CACHE.get("Wrapper").offsetWidth - DATA.CACHE.get("Game").offsetWidth);
			};
			
			//Check y position
			if( -1*DATA.CACHE.get("Wrapper").offsetTop > DATA.CACHE.get("Wrapper").offsetHeight - DATA.CACHE.get("Game").offsetHeight ){
				DATA.CACHE.get("Wrapper").style.top = -1*(DATA.CACHE.get("Wrapper").offsetHeight - DATA.CACHE.get("Game").offsetHeight);
			};
	 };

	
	

  /**
   * Fill new map by "1"
   */
  ;(function(){
    var prop = 1,
        row  = [],
        size = Heroes.MAP.size;
    for( var i = 0; i < size.height / size.minH; i++ ){
      for( var j = 0; j < size.width / size.minW; j++ ){
        row.push({"v":prop});
      };
      if( row.length >= size.width / size.minW ) {
        mpmodel.push(row);
        row = [];
      };
    };
  }());
  
 
  
  /**
   * Set rights properties for map model
   */
  this.setPropToMap = function(o){
    var x   = o.x / Heroes.MAP.size.minW + (o.data && o.data.dx ? o.data.dx : 0),
        row = o.y / Heroes.MAP.size.minH + (o.data && o.data.dy ? o.data.dy : 0),
        w   = o.data ? o.data.w : o.w,
        h   = o.data ? o.data.h : o.h;

    if( mpmodel[row][x] != o.p ){
		
      //If not hero
      if( 0 !== o.p ){
        for( var i = 0; i < w / Heroes.MAP.size.minW; i+= 1 ){
          for( var j = 0; j < h / Heroes.MAP.size.minH; j+= 1 ){
            mpmodel[row+j][x + i] = {"v": o.p};
          };
        };
      };

    };
  };
  
  
  
	
	/* Set correct controll view */
  setControllsGeometry();
  
  /**
   * Create object action place div
   */
	
  this.createObjectActionPlace = function(o){
    if( null !== o.data.mX && null !== o.data.mY ){
	  var x = (o.x + o.data.mX)/ Heroes.MAP.size.minW;
	  var y = (o.y + o.data.mY)/ Heroes.MAP.size.minH;
      var e = document.createElement("div");

      e.className = objPCLs;
      e.setAttribute("style", "left:"+(o.x + o.data.mX)+"px;top:"+(o.y + o.data.mY)+"px");
      DATA.CACHE.get("Map").appendChild(e);
	  mpmodel[y][x].v = -4;
    };
  };
  
  
    //Set background
    for( var b = 0; b < itemsLn; b++ ){
      this.setPropToMap(items[b]);
    };
  

	
    /**
	 * Render static object
	 * @param o Object width data about it
	 */
	var renderStatic = function(o){
		th.createBlock(o, objCls);
		th.setPropToMap(o);
	};
	
    /**
	 * Render dynamic object
	 * @param o Object width data about it
	 */
	var renderDynamic = function(o){
		o.link = th.createBlock(o, objCls);
		th.setPropToMap(o);
		th.createObjectActionPlace(o);
		
		/* If we have a flags on object */
		if( o.color ){
			var flag = document.createElement("div");
			flag.className = "build_flag_top_"+ o.color;
			flag.setAttribute("style","top:"+ o.data.flag.y +"px;left:"+ o.data.flag.x +"px;");
			o.link.appendChild(flag);
		};
	};


    /**
	 * Render resources object
	 * @param o Object width data about it
	 */
	var renderResource = function(o){
		o.link = th.createBlock(o, objCls);
		th.setPropToMap(o);
		th.createObjectActionPlace(o);
	};


    /**
	 * Render building
	 * @param o Object width data about it
	 */
	var renderBuildings = function(o){
		var build_ = th.createBlock(o, buildCls);
		th.setPropToMap(o);
		th.createObjectActionPlace(o);
		
		//Create building bottom flag
		var build_flag = document.createElement("div");
		build_flag.className = "build_flag_"+o.color;
		build_flag.setAttribute("style","top:"+o.data.flags.bottom.y+"px;left:"+o.data.flags.bottom.x+"px;");
		build_.appendChild(build_flag);
		
		//Create building top flag
		//var build_flag = document.createElement("div");
		//build_flag.className = "build_flag_top_"+o.color;
		//build_flag.setAttribute("style","top:"+o.data.flags.top.y+"px;left:"+o.data.flags.top.x+"px;");
		//build_.appendChild(build_flag);
	};


	/**
	 * Render player
	 */
	var renderPlayer = function(o){ 
		var p = units.PLAYERS[o.data.color][o.data.name];
		p.data.s = Heroes.config.resources.players_[o.data.t];
		var p_el = th.createBlock(p.data, plCls, true);
		p.link = p_el;
		p.Draw(p);
	};
  


  
   //Set Own and Opponent players
   for( var p in players ){
		for( var pi in players[p] ){

			var player = new Heroes.Objects.Player(players[p][pi]);

			if( !units.PLAYERS[p] ){
				units.PLAYERS[p] = {};
			};

			units.PLAYERS[p][player.data.name] = player;
			units.PLAYERS[p][player.data.name].data.color = p;

			//Set default active hero
			if( players[p][pi].active ) {
				DATA.PLAYER_ACTIVE = units.PLAYERS[p][player.data.name];
			};
		};
    };
	
	
	/* Create static objects */
	for( var static_ = 0; static_ < STATIC_OBJ.length; static_++ ){
		var os     = STATIC_OBJ[static_];
		var param  = Heroes.StaticObjectsList[os.t];
		
		os.data    = param;
		DATA.UNITS.STATIC.push(os);
	};
	

	/* Create dynamic objects */
	for( var dynamic_ = 0; dynamic_ < DYNAMIC_OBJ.length; dynamic_++ ){
		var od  = DYNAMIC_OBJ[dynamic_],
			od_ = new Heroes.Objects[od.t];

		od.data = od_;
		od.ot = "Objects";
		od.data["_id"] = "o_" + new Date().getTime() + "_" + dynamic_;

		DATA.UNITS.OBJECTS.push(od);
	};


	/* Create mobs */
	for( var mobs_ = 0; mobs_ < MOBS_OBJ.length; mobs_++ ){
		var ob  = MOBS_OBJ[mobs_],
			ob_ = new Heroes.Objects[ob.t];

		ob.data = ob_;
		ob.ot = "Objects";
		ob.data["_id"] = "m_" + new Date().getTime() + "_" + mobs_;

		DATA.UNITS.OBJECTS.push(ob);
	};

	

	/* Create map resoures */
	for( var resources = 0; resources < RES_OBJ.length; resources++ ){
		var or  = RES_OBJ[resources],
			or_ = new Heroes.Objects[or.t](or.name);
			or.data = or_;

		or.data["_id"] = "res_" + new Date().getTime() + "_" + resources;

		or.ot = "Objects";
		DATA.UNITS.RESOURCES.push(or);
	};


	/* Create buildings objects */
	for( var buildings_ = 0; buildings_ < BUILD_OBJ.length; buildings_++ ){
		var ob  = BUILD_OBJ[buildings_],
			ob_ = new Heroes.Buildings[ob.t];
			ob.data = ob_;
		
		ob.data["_id"] = "buid_" + new Date().getTime() + "_" + buildings_;

		if( ob.color === DATA.PLAYER_COLOR ){
			ob.own = true;
		};	
		
		ob.ot = "Buildings";
		DATA.UNITS.BUILDINGS.push(ob);
	};
	

    
    
	/* Set how early we must start generate objects under the terra incognita */
	var genaration_radius_addon = 10;
	

	/* Clone objects */
	var OBJECTS_FOR_RENDER = $.extend(true, {}, DATA.UNITS);
	
	/* Create players array from object */
	;(function(){
		OBJECTS_FOR_RENDER.PLAYERS_ = [];
		for( var i in OBJECTS_FOR_RENDER.PLAYERS ){
			for( var j in OBJECTS_FOR_RENDER.PLAYERS[i] ){
				OBJECTS_FOR_RENDER.PLAYERS_.push(OBJECTS_FOR_RENDER.PLAYERS[i][j]);
			};
		};
		delete OBJECTS_FOR_RENDER.PLAYERS;
	}());
    
	
	/* Detect objects */
	var DetectObjects = function(obj){
		if( obj.o ){

			var hero_radius = obj.hero.terraRadius + genaration_radius_addon;
			var hero_radius_half = hero_radius / 2;
			var hero_x = Math.ceil(obj.hero.data.x / Heroes.MAP.size.minW);
			var hero_y = Math.ceil(obj.hero.data.y / Heroes.MAP.size.minH);
			
			var object_ = obj.o;
			var object_x = Math.ceil((object_.x || object_.data.x) / Heroes.MAP.size.minW);
			var object_y = Math.ceil((object_.y || object_.data.y)/ Heroes.MAP.size.minH);
			var object_w = Math.ceil(object_.data.w / Heroes.MAP.size.minW);
			var object_h = Math.ceil(object_.data.h / Heroes.MAP.size.minH);
			
			if( 
				object_x + object_w >= hero_x - hero_radius_half &&
					object_x < hero_x + hero_radius_half &&
						object_y + object_h >= hero_y - hero_radius_half &&
							object_y < hero_y + hero_radius_half
			){
				switch(obj.type){
					case "STATIC":
						renderStatic(object_);
						break;
					case "BUILDINGS":
						renderBuildings(object_);
						break;
					case "OBJECTS":
						renderDynamic(object_);
						break;
					case "PLAYERS_":
						renderPlayer(object_);
						break;
					case "RESOURCES":
						renderResource(object_);
						break;
				};
				
				obj.callback();
			};
		};
	};


	/* Generate objects */
	var GenerateObjects = function(){
		for( var p in DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR] ){
				var hero = DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR][p];
				
				//Loop all objects
				for( var type in OBJECTS_FOR_RENDER ){
                    for( var object = 0; object < OBJECTS_FOR_RENDER[type].length; object++ ){
                        DetectObjects({
                            o: OBJECTS_FOR_RENDER[type][object],
                            hero: hero,
                            type: type,
                            callback: function(){
                            
                                //Remove object from container
                                OBJECTS_FOR_RENDER[type][object] = null;
                            }
                        });
                    }; 
				
					//Clear container
					for( var object = 0; object < OBJECTS_FOR_RENDER[type].length; object++ ){
						if( null === OBJECTS_FOR_RENDER[type][object] ){
							OBJECTS_FOR_RENDER[type].splice(object, 1);
						};
					};
					
					//Delete objects container if it's empty
					if( 0 === OBJECTS_FOR_RENDER[type].length ){
						delete OBJECTS_FOR_RENDER[type];
					};
				};
		};	
	};
	
	GenerateObjects();
	
	
	
	//Set map properties
	DATA.GEOMETRY.SCREEN_W = DATA.CACHE.get("Game").offsetWidth;
	DATA.GEOMETRY.SCREEN_H = DATA.CACHE.get("Game").offsetHeight;
	


    /**  
	 * Map API 
	 */
	EVENTER.on("map.generate_objects", function(){
		GenerateObjects();
	});

	EVENTER.on("map.remove_object", function(data){
		for( var i = 0; i < data[0].length; i++ ){
			if( data[0][i].data["_id"] == data[1] ){
				data[0].splice(i, 1);
			};
		};
	});



  return {
    viewportSetGeometry: function(){
      setControllsGeometry();
			wrapperPositionChecker();
    }
  };
});
