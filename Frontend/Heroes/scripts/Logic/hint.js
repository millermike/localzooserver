/**
 * Objects hint 
 */
Heroes.Hint = (function(){
  
  "use strict";
	
	var x, y, els;
	
	els = {
		"build"  : DATA.UNITS.BUILDINGS,
		"obj"    : DATA.UNITS.OBJECTS,
		"stat"   : DATA.UNITS.STATIC,
		"res"    : DATA.UNITS.RESOURCES,
		"heroes" : []
	};
	
	/**
	 * Extend players by width and height parameters
	 */
    var team = DATA.UNITS.PLAYERS;
    for (var colr in team){
      for (var h in team[colr]){
        els.heroes.push(team[colr][h].data);
      };
    };
    for ( var person = 0; person < els.heroes.length; person++ ){  // add fields, need for condition, works fastest, then construction with ||
		els.heroes[person]['data'] = {
			w:els.heroes[person]['w'],
			h:els.heroes[person]['h']
		};
    };

	

	//Reset mouse right click context menu
	document.oncontextmenu = function(e){return false;};

	//Show hint event	
	document.addEventListener("mousedown", function(e){

		if( e.which === 3 && !Lib_.IsParentById(e.target, "popup_castle") ){
		
			x = e.pageX - DATA.CACHE.get("Wrapper").offsetLeft - DATA.CACHE.get("Game").offsetLeft;
			y = e.pageY - DATA.CACHE.get("Wrapper").offsetTop - DATA.CACHE.get("Game").offsetTop;

       
			if( e.target.className !== "terra_block" ){
				for( var i in els ){
					for( var j = 0; j < els[i].length; j++ ){
							if( 
								x >= els[i][j].x && 
								x <= els[i][j].x + els[i][j].data.w &&
								y >= els[i][j].y &&
								y <= els[i][j].y + els[i][j].data.h
							){
                  
								if( i === "build" ){
									DATA.POPUP.Hint.content(Heroes.Tmpl.HintBuild(els[i][j]));
								}else if( 
									i === "stat" ||
									i === "res"  ||
									i === "obj"
								){
									DATA.POPUP.Hint.content(Heroes.Tmpl.Hint(els[i][j].data.hint));
								}else if (i === "heroes"){
                  					DATA.POPUP.Hint.content(Heroes.Tmpl.HintHero(els[i][j]));
                				};
                
								DATA.POPUP.Hint.show();
								DATA.POPUP.Hint.position(e.pageX, e.pageY);
							};
					};
				};
			}else{
				DATA.POPUP.Hint.content(Heroes.Tmpl.Hint(Heroes.lang.en.Terra));
				DATA.POPUP.Hint.show();
				DATA.POPUP.Hint.position(e.pageX, e.pageY);
			};
			
			//For simple ground
      if( e.target.id === "Map" ){
				DATA.POPUP.Hint.content(Heroes.Tmpl.Hint(Heroes.lang.en.Map));
				DATA.POPUP.Hint.show();
				DATA.POPUP.Hint.position(e.pageX, e.pageY);
			};
			
			
		};
	});
	
	document.addEventListener("mouseup", function(e){

		if( 
			e.which === 3 && 
				!Lib_.IsParentById(e.target, "popup_castle") &&
					!Lib_.IsParentById(e.target, "popup_battle") &&
						e.target.id !== "dark_screen_layout"
		){
			DATA.POPUP.Hint.hide();
		};
	});
});
