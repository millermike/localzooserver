/**
 * Mini map class 
 */
Heroes.MiniMap = (function(){
  
  "use strict";
	
	var mini     = DATA.CACHE.get("miniMap_"),
			ctx      = mini.getContext("2d"),
			miniBG   = DATA.CACHE.get("miniMap_BG"),
			ctxBG    = miniBG.getContext("2d"),
			miniPL   = DATA.CACHE.get("miniMap_PL"),
			ctxPL    = miniPL.getContext("2d"),
			miniTR   = DATA.CACHE.get("miniMap_TERRA"),
			ctxTR    = miniTR.getContext("2d"),
			miniFR   = DATA.CACHE.get("miniMap_FRAME"),
			ctxFR    = miniFR.getContext("2d"),
			
			
			mini_w   = DATA.CACHE.get("miniMap_").offsetWidth,
			mini_h   = DATA.CACHE.get("miniMap_").offsetHeight,
			g_w      = DATA.CACHE.get("Wrapper").offsetWidth,
			k        = mini_w / g_w,
			mW       = Heroes.MAP.size.minW,
			mH       = Heroes.MAP.size.minH;
			
			
	var BG    = Heroes.MAP.background,
		STAT  = Heroes.MAP["static"],
		DIN   = Heroes.MAP["dynamic"],
	    BUILD = Heroes.MAP["buildings"],
		PL    = DATA.UNITS.PLAYERS,
		TERRA = DATA.TERRAINCOGNITA;
		
	
	//Create buffer	
	var bufferDOM = document.createElement("canvas");
			bufferDOM.setAttribute("width", mini.offsetWidth);
			bufferDOM.setAttribute("height", mini.offsetHeight);
	var buffer = bufferDOM.getContext("2d");
	
	
	//Draw scene
	var _DRAW = function(c){
		return c.drawImage(bufferDOM, 0, 0);
	};
	
	//Clear scene
	var _CLEAR = function(c){
		c.clearRect(0, 0, mini_w, mini_h);
		buffer.clearRect(0, 0, mini_w, mini_h)
	};
	
	
	
	//Draw map background once
	for( var b = 0; b < BG.length; b++ ){
		buffer.fillStyle = BG[b].cl;
		buffer.fillRect( Math.ceil(BG[b].x * k), Math.ceil(BG[b].y * k), Math.ceil(BG[b].w * k), Math.ceil(BG[b].h * k) );
	};
	_DRAW(ctxBG);
	
	
	
	//Draw players
	var _RENDER_PLAYERS = function(){
			_CLEAR(ctxPL);
			for( var p in PL ){
				for( var pi in PL[p] ){
					buffer.save();
					buffer.fillStyle = p;
					buffer.fillRect( PL[p][pi].data.x * k, PL[p][pi].data.y * k, PL[p][pi].data.w * k, PL[p][pi].data.h * k );
					buffer.restore();
				};
			};
			_DRAW(ctxPL);
	};
	_RENDER_PLAYERS();
	
	
	
	//Draw terra incognita
	var _RENDER_TERRA = function(){
		_CLEAR(ctxTR);
		buffer.fillStyle = "black";
		for( var t = 0; t < TERRA.length; t++ ){
			for( var tr = 0; tr < TERRA[t].length; tr++){
					if( TERRA[t][tr] === 1 ){
							buffer.fillRect( Math.ceil(tr * mW * k), Math.ceil(t * mH * k), Math.ceil(mW * k), Math.ceil(mH * k));
					};
			};
		};
		_DRAW(ctxTR);
	};
	_RENDER_TERRA();
	
	
	
	//Draw current position frame
	var g_w_  = DATA.CACHE.get("Game").offsetWidth * k;
	var g_h_  = DATA.CACHE.get("Game").offsetHeight * k;
	var map_l, map_t;
	var _RENDER_FRAME = function(){
			map_l = -1 * DATA.CACHE.get("Wrapper").offsetLeft * k << 0;
			map_t = -1 * DATA.CACHE.get("Wrapper").offsetTop * k << 0;
			_CLEAR(ctxFR);
				buffer.beginPath();
				buffer.rect(map_l, map_t, g_w_ , g_h_);
				buffer.strokeStyle = 'red';
				buffer.stroke();
			_DRAW(ctxFR);
	};
	_RENDER_FRAME();
	
	
	
	//Draw base objects
	var RENDER = function(){
			_CLEAR(ctx);
			
			//Draw static objects
			for( var s = 0; s < STAT.length; s++ ){
				if( STAT[s].data ){
					buffer.save();
					buffer.fillStyle = STAT[s].cl;
					buffer.fillRect( STAT[s].x * k, STAT[s].y * k, STAT[s].data.w * k, STAT[s].data.h * k );
					buffer.restore();
				};
			};

			//Draw dynamic
			for( var d = 0; d < DIN.length; d++ ){
				if( DIN[d].data ){
					buffer.save();
					buffer.fillStyle = "grey";
					buffer.fillRect( DIN[d].x * k, DIN[d].y * k, DIN[d].data.w * k, DIN[d].data.h * k );
					buffer.restore();
				};
			};
			
			//Draw buildings
			for( var bd = 0; bd < BUILD.length; bd++ ){
				if( BUILD[bd].data ){
					buffer.save();
					buffer.fillStyle = BUILD[bd].color;
					buffer.fillRect( BUILD[bd].x * k, BUILD[bd].y * k, BUILD[bd].data.w * k, BUILD[bd].data.h * k );
					buffer.restore();
				};
			};

			_DRAW(ctx);
	};
	RENDER();
	
	
	
	
	
	//Add mini-map draw listener
	EVENTER.on("draw", function(){
		RENDER();
	});
	
	//Add mini-map player draw listener
	EVENTER.on("drawPlayers", function(){
		_RENDER_PLAYERS();
	});
  	
	//Add mini-map terra incognita draw listener
	EVENTER.on("drawTerra", function(){
		_RENDER_TERRA();
	});
	
	//Add mini-map red frame listener
	EVENTER.on("drawFrame", function(){
		_RENDER_FRAME();
	});
});















