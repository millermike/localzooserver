window.addEventListener('load', function(){

    'use strict';

    var OPPONENT,
        OWN,
        CURRENT_PLAYER,
        CURRENT_UNIT,
        CURRENT_MOB,
        BATTLE,
        BATTLE_UNITS = [],
        ATTACK_UNITS = [],
        CELLS_TO_MOVE = [],
        CELLS_ID = [],
        ATTACKED_CELL_MOVE_TIME = 200,
        CELLS = {
            w: 15,
            h: 11
        },
        CELL = {
            w: 40,
            h: 49,
            vDiff: 12,
            mergeDiff: 20
        };


    /** 
     * Создаем массив всех юнитов на карте для быстрой проверки занятой ячейки 
     */
    function createMobsCollection() {
        Object.keys(OPPONENT).forEach(function(item) {
            BATTLE_UNITS.push(OPPONENT[item]);
        });
        Object.keys(OWN).forEach(function(item) {
            BATTLE_UNITS.push(OWN[item]);
        });
    };


    /**
     * Определяем группы которыми будем ходить 
     */
    function getUnitsForAttack(speed) {
            
        CURRENT_PLAYER = DATA.PLAYER_ACTIVE;
        
        Object.keys(OWN).forEach(function(unit) {
            if( OWN[unit].mob.speed >= speed ) {
                ATTACK_UNITS.push(OWN[unit]); 
            };
        });

        ATTACK_UNITS.sort(function(a, b) {
            return b.mob.speed - a.mob.speed; 
        });
    };


    /**
     * Подсчитываем радиус хода для текущего юнита 
     */
    function defineMobRange() {
        var speed = CURRENT_MOB.mob.speed,
            range = CURRENT_MOB.mob.speed * 2;
        
        var x = CURRENT_MOB.c - speed,
            y = CURRENT_MOB.row - speed;

        CELLS_TO_MOVE.wd = CELLS_TO_MOVE.hg = range;
        
        for( var i = 0; i < range; i++ ) {
            for( var j = 0; j < range; j++ ) {
                if( x + i >= 0 && y + j >= 0 && x + i < CELLS.w && y + j < CELLS.h ) {
                    CELLS_TO_MOVE.push({
                        x: x + i,
                        y: y + j 
                    });
                    CELLS_ID.push('x'+ (x + i) +'y' + (y + j));
                };
            };
        };
    };


    /**
     * Метод генерирующий блоки для определения дальности хода юнита
     */
    function drawMobRange() {
        var cell, left;

        CELLS_TO_MOVE.forEach(function(item) {
            left = item.y % 2 !== 0 ? (CELL.w * item.x) : CELL.w * item.x + (CELL.w / 2);
            
            cell = document.createElement('div');
            cell.className = 'battle-cell-color';
            cell.id = 'x'+ item.x +'y'+ item.y;
            cell.setAttribute('style', 
                'width:'+ CELL.w +'px;'+
                'height:'+ CELL.h +'px;'+
                'top:'+ (CELL.h - CELL.vDiff) * item.y +'px;'+
                'left:'+ left  +'px;'
            );
            DATA.CACHE.get('battle-grid').appendChild(cell);
        });
    };

    //Создаем сетку поля боя
    function getMapMatrix() {
        var row, matrix = [];

        for( var i = 0; i < CELLS_TO_MOVE.hg; i++ ) {
            row = [];
            for( var j = 0; j < CELLS_TO_MOVE.wd; j++ ) row.push(0);            
            matrix.push(row);
        };

        return matrix;
    };

    /**
     * Перемещаем моба в указаное место и возможно атакуем
     */
    function moveTo(event) {
        var startX, startY,
            sprite, cellPosition, toX, toY,
            finder, grid, path,
            bParams = CURRENT_MOB.mob['b_params'];


        var battleGridPosition = Lib_.getOffsetSum(DATA.CACHE.get('battle-grid'), true);
        var cursorX = event.pageX - battleGridPosition.left;
        var cursorY = event.pageY - battleGridPosition.top;
        var originY = cursorY / (CELL.h - CELL.vDiff) << 0;
        var originXPos = cursorX - (originY % 2 === 0 ? CELL.mergeDiff : 0);
        var originX = originXPos / CELL.w << 0;


        //Нельзя жмакать мимо поля
        if( originXPos >= 0 ) {

            //Проверяем не кликнули ли мы по юниту
            BATTLE_UNITS.forEach(function(mob) {
                if( mob.row === originY && mob.c === originX ) {
                    console.log('GET')
                }
            });
            console.log(originY, originX, BATTLE_UNITS);
        }

        //Если жмакнули по ячейки свободной для перехода
        if( ~CELLS_ID.indexOf(event.target.id) ) {
            startX = CURRENT_MOB.c;
            startY = CURRENT_MOB.row;

            sprite = new Heroes.Sprite({
                p: CURRENT_MOB.mob,
                el: CURRENT_MOB.dom,
                step: bParams.w,
                length: bParams.sprites
            });

            sprite.animate();

            cellPosition = /x(\d+)y(\d+)/.exec(event.target.id).splice(1, 2);
            toX = +cellPosition[0];
            toY = +cellPosition[1];

            if( startX > toX ) {
                Lib_.ReplaceClass(CURRENT_MOB.dom, 'unit_label_right', 'unit_label_left');
            } else {
                Lib_.ReplaceClass(CURRENT_MOB.dom, 'unit_label_left', 'unit_label_right');
            };
            
            finder = new PF.AStarFinder(); 
            grid = new PF.Grid(CELLS_TO_MOVE.wd, CELLS_TO_MOVE.hg, getMapMatrix());
            path = finder.findPath(CURRENT_MOB.c, startY, toX, toY, grid);

            $(CURRENT_MOB.dom).animate({
                left: toX * CELL.w + (bParams.dx || 0) + ((toY + 1) % 2 ? CELL.mergeDiff : 0),
                top: (toY + 1) * CELL.h - bParams.h - ((toY + 1) * CELL.vDiff) + (bParams.dy || 0)
            }, (path.length) * ATTACKED_CELL_MOVE_TIME, 'linear', function() {
                sprite.stop();
                sprite.setStartPosition(0, bParams.h);

                CURRENT_MOB.c = toX;
                CURRENT_MOB.row = toY;
            });
        } 

        //Если жмакнули по юниту
        else if( ~event.target.className.indexOf('battleUnit') ){
            console.log('ATTACK OR DO NOTHING');
        };
    };


    function attack() {
        var unitCounter = 0,
            mobCounter = 0;
       
        CURRENT_MOB = ATTACK_UNITS[unitCounter];
       
        defineMobRange();
        drawMobRange();

        $(document).on('click', '#battle-grid, #battle-units-dom', moveTo); 
    };
    

    EVENTER.on("player.turn", function(data){
        OPPONENT = data.battle.opponent;
        OWN = data.battle.own;
        BATTLE = data.battle;
        
        getUnitsForAttack(data.unitMaxSpeed); 
        createMobsCollection();
        attack();
    });

}, false);
