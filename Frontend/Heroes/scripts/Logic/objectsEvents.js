/**
 * Objects events hander 
 */
Heroes.ObjectEvent = (function(){
  
  "use strict";
	
	var x, 
		y,
		mW = Heroes.MAP.size.minW,
		mH = Heroes.MAP.size.minH;

	document.addEventListener("click", function(e){
			if( 
					e.target.className !== "terra_block" && 
					e.target.className !== "map-object-place" 
			){
					x = e.pageX - DATA.CACHE.get("Wrapper").offsetLeft - DATA.CACHE.get("Game").offsetLeft;
					y = e.pageY - DATA.CACHE.get("Wrapper").offsetTop - DATA.CACHE.get("Game").offsetTop;
					
					//Handler for buildings
					for( var j = 0; j < DATA.UNITS.BUILDINGS.length; j++ ){
							if( 
								x >= DATA.UNITS.BUILDINGS[j].x && 
								x <= DATA.UNITS.BUILDINGS[j].x + DATA.UNITS.BUILDINGS[j].data.w &&
								y >= DATA.UNITS.BUILDINGS[j].y &&
								y <= DATA.UNITS.BUILDINGS[j].y + DATA.UNITS.BUILDINGS[j].data.h
							){
									if( !DATA.PLAYER_FOCUS ){
											DATA.UNITS.BUILDINGS[j].data.openBuild();
									};
									
									//Make player unmovable
									DATA.PLAYER_FOCUS = false;
									
									DATA.PLAYER_ACTIVE = null;
									
									//Clear players path
									DATA.CACHE.get("Steps").innerHTML = "";
									
									//Make all heroes unselected
									EVENTER.trigger("heroDeactive");
							};
					};
					
			};
	});
	
	
	/**
	 * Mouse move event for detection number of steps in days to object
	 */
	document.addEventListener("mousemove", function(e){
	
		/* If we have an active hero */
		if( DATA.PLAYER_ACTIVE ){
	
			/* We minimize frequncy of path finding func calls */
			Lib_.ExecuteLimiter(function(){
				if( 
						e.target.className !== "terra_block" 
				){
						x = (e.pageX - DATA.CACHE.get("Wrapper").offsetLeft - DATA.CACHE.get("Game").offsetLeft) / mW << 0;
						y = (e.pageY - DATA.CACHE.get("Wrapper").offsetTop - DATA.CACHE.get("Game").offsetTop) /mH << 0;

						//Check for correct map point
						if( DATA.MAPMODEL[y] && DATA.MAPMODEL[y][x] ){
						
							if( DATA.MAPMODEL[y][x].v > -2 ){
								var steps = Heroes.FindPath(
										DATA.MAPMODEL, 
										{x: x, y: y},
										{x: DATA.PLAYER_ACTIVE.data.x / Heroes.MAP.size.minW, y:DATA.PLAYER_ACTIVE.data.y / Heroes.MAP.size.minH}
									).GetPath();
									
								var ln = steps.length + DATA.PLAYER_ACTIVE.startSteps - DATA.PLAYER_ACTIVE.moveValue;
								var days = ln / DATA.PLAYER_ACTIVE.startSteps;


								if( days >= 0 && days < 1 ){
									Lib_.ClearClass(DATA.CACHE.get("Map"));
									Lib_.ClearClass(DATA.CACHE.get("Steps"));
								};
								if( days >=1 && days < 2 ){
									Lib_.ClearClass(DATA.CACHE.get("Map"));
									Lib_.ClearClass(DATA.CACHE.get("Steps"));
									Lib_.AddClass(DATA.CACHE.get("Map"), "go2");
									Lib_.AddClass(DATA.CACHE.get("Steps"), "go2");
								};
								if( days >= 2 && days < 3 ){
									Lib_.ClearClass(DATA.CACHE.get("Map"));
									Lib_.AddClass(DATA.CACHE.get("Map"), "go3");
									Lib_.AddClass(DATA.CACHE.get("Map"), "go3");
									Lib_.AddClass(DATA.CACHE.get("Steps"), "go3");
								};
								if( days >= 3 ){
									Lib_.ClearClass(DATA.CACHE.get("Map"));
									Lib_.AddClass(DATA.CACHE.get("Map"), "go4");
									Lib_.AddClass(DATA.CACHE.get("Map"), "go4");
									Lib_.AddClass(DATA.CACHE.get("Steps"), "go4");
								};
								
							};
							
							/*if(  
								e.target.className.indexOf('map-object-place') !== -1 &&
									DATA.MAPMODEL[y][x].v == -4
							){
								var steps = new Heroes.FindPath(
									DATA.MAPMODEL, 
									{x: x, y: y},
									{x: DATA.PLAYER_ACTIVE.data.x / Heroes.MAP.size.minW, y:DATA.PLAYER_ACTIVE.data.y / Heroes.MAP.size.minH}
								).GetPath();
								
								var ln = steps.length + DATA.PLAYER_ACTIVE.startSteps - DATA.PLAYER_ACTIVE.moveValue;
								var days = ln / DATA.PLAYER_ACTIVE.startSteps;
								
								if( days >= 0 && days < 1 ){
									e.target.removeAttribute('data-obj-place');
								};
								if( days >=1 && days < 2 ){
									e.target.setAttribute('data-obj-place', 'goto2');
								};
								if( days >=2 && days < 3 ){
									e.target.setAttribute('data-obj-place', 'goto3');
								};
								if( days >=3 && days < 4 ){
									e.target.setAttribute('data-obj-place', 'goto4');
								};
							};*/
							
							//TO DO:
							//Do the same for units on stage
						};
				};
			},[], 250);
		}else{
			Lib_.ClearClass(DATA.CACHE.get("Map"));
			Lib_.AddClass(DATA.CACHE.get("Map"), "unselected-cursor");
		};
	});

});
