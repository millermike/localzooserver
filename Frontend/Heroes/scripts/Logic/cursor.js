Heroes.Cursor = (function(){
  
    "use strict";
	
	var hideCursor = function(){
		Lib_.AddClass(document.body, "no-cursor");
	};
	
	var showCursor = function(){
		Lib_.RemoveClass(document.body, "no-cursor");
	};
	
	/* API */
	EVENTER.on("cursor.hide", hideCursor);
	EVENTER.on("cursor.show", showCursor);
});












