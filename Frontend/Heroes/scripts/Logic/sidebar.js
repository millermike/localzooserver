Heroes.Sidebar = (function(){
  
  	"use strict";
	
	/* Generate player heroes tabs */
	var heroes   = DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR],
		tabList  = DATA.CACHE.get("SidebarPlayersTabsList"),
		heroTab  = DATA.CACHE.getClass(document, "sidebar-player-tab"),
		buildTab = DATA.CACHE.getClass(document, "sidebar-castle-tab"),
		TXT      = ""; 
			

	//Date vars
	var month = DATA.CACHE.get('DateMonth');
	var week = DATA.CACHE.get('DateWeek');
	var day = DATA.CACHE.get('DateDay');

			
	var GetIndicator = function(n, k){
		var ampl = n / k;
		var val = ampl <= 1 ? n * 100 / k : 100;
		var add = "";
		
		if( ampl > 1 && ampl < 2 ){
			add = "<div>+</div>";
		}else if( ampl > 2 && ampl < 3 ){
			add = "<div>++</div>";
		}else if( ampl > 3 ){
			add = "<div>+++</div>";
		};
		return [val, add];
	};
	
	
    //Set heroes steps and mana by default	
    for( var i in heroes ){
		var steps   = GetIndicator(heroes[i].moveValue, CONST.NORMALPATHLENGTH);
		var stepsEL = "<div class='heroes-steps-indicator' style='height:"+steps[0]+"%'>"+steps[1]+"</div>";

		var mana   = GetIndicator(heroes[i].mana, CONST.NORMALMANAVAL);
		var manaEL = "<div class='heroes-mana-indicator' style='height:"+mana[0]+"%'>"+mana[1]+"</div>";
		
		var active = heroes[i].data.name === DATA.PLAYER_ACTIVE.data.name ? "hero_active" : "";
		
		TXT += Heroes.Tmpl.HeroeTab(heroes[i], stepsEL, manaEL, active);
	};
	tabList.innerHTML = TXT;


	/* Set own castles */
	var b_list      = DATA.CACHE.get('SidebarCastlesTabsList'),
		own_counter = 0,
		addon_cls   = ""

    for( var build = 0; build < DATA.UNITS.BUILDINGS.length; build++ ){
		addon_cls   = "";

		if( DATA.UNITS.BUILDINGS[build].own ){
			++own_counter;

			if( own_counter == 1 && !DATA.PLAYER_ACTIVE ) {
				addon_cls = 'castle-active';
				DATA.BUILD_ACTIVE = DATA.UNITS.BUILDINGS[build];
			};

			b_list.innerHTML += '<div class="sidebar-castle-tab '+ addon_cls +'" id="'+ DATA.UNITS.BUILDINGS[build].data["_id"] +'">'+
				'<img class="sidebar-castle-tab-img" src="'+ DATA.UNITS.BUILDINGS[build].data.mini +'" />'+
			'</div>';
		};
	};	









	/* Set active hero info bar template */
	DATA.CACHE.get("PlayerArmyInfoWrapper").innerHTML = Heroes.Tmpl.HintHero(DATA.PLAYER_ACTIVE.data);

	
	//Indicators update function
	var update = function(data, c, t){
		var d         = GetIndicator(data[0], c),
		    tab       = DATA.CACHE.get("hero_"+data[1].data.name),
		    indicator = DATA.CACHE.getClass(tab, "heroes-"+t+"-indicator", 0);
				
		indicator.innerHTML    = d[1];
		indicator.style.height = d[0]+"%";
	};
	
	
	
	/* Change active hero */
	document.addEventListener("click", function(e){
		if( e.target.className.indexOf('sidebar-player-tab') !== -1 ){
			var tabid = e.target.parentNode.id;
			var id    = tabid.substr(5, tabid.length);
			
			if(DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR][id]){
				DATA.PLAYER_ACTIVE = DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR][id];
				
				//Set active class if we need
				if( e.target.parentNode.className.indexOf("hero_active") === -1 ){
				
					//Clear all heroes
					Lib_.RemoveClass(heroTab, "hero_active");
				
					e.target.parentNode.className += " hero_active";
				  	
					DATA.MAPMOVING.SETTOACTIVE(DATA.PLAYER_ACTIVE);
					
					DATA.PLAYER_MOVEMENT.moveStatus = true;
					
					DATA.PLAYER_FOCUS = true;

					/* Unselect buidings */
					DATA.BUILD_ACTIVE = null;
					Lib_.RemoveClass(buildTab, "castle-active");

					
					Heroes.CORE.ClearHeroSteps();

					//Set player info template to bottom sidebar
					DATA.CACHE.get("PlayerArmyInfoWrapper").innerHTML = Heroes.Tmpl.HintHero(DATA.PLAYER_ACTIVE.data);
				
					//Remove cursor unactive class
					Lib_.RemoveClass([DATA.CACHE.get("Map")], "unselected-cursor");
				};
			};
		};
	}, true);





	/* Change active castle */
	document.addEventListener("click", function(e){
		if( e.target.className.indexOf('sidebar-castle-tab-img') !== -1 ){
			var b = DATA.UNITS.BUILDINGS;

			for( var i = 0; i < b.length; i++ ){
				if( b[i].data["_id"] == e.target.parentNode.id ){

					/* Unselect active player */
					DATA.PLAYER_ACTIVE = null;
					Lib_.RemoveClass(heroTab, "hero_active");

					Lib_.RemoveClass(buildTab, "castle-active");
					e.target.parentNode.className += " castle-active";
					
					DATA.BUILD_ACTIVE = b[i];

					DATA.MAPMOVING.SETTOACTIVE(DATA.BUILD_ACTIVE);		
				};
			};
		};
	}, true);
	
	
	


	/* New day functionality */
	var img_object    = null,
	    day_block     = DATA.CACHE.get('NewDayDate'),
	    army          = DATA.CACHE.get('PlayerArmyBlock'),
	    new_day_block = DATA.CACHE.get('NewDay'),
	    d             = DATA.DATE,
	    NEWDAY        = true,
		SHOWTIMEOUT   = 3000;

	/**
	 * Set correct image to bottom of sidebar( Showing new day or new week )
	 * @param type String type of image (day || week)
	 */	
	var NewDayWeek = function(type){
		day_block.innerHTML = type+" " + d[type.toUpperCase()];
		img_object = new_day_block.getElementsByTagName('img')[0];
		img_object.src = "images/new_"+ type.toLowerCase() +".gif"
	};

	/* New day/week functionality */	
	DATA.CACHE.get('SidebarReload').addEventListener('click', function(){
		
		if( NEWDAY ){

			NEWDAY = false;

			/* Increase days */
			if( d.DAY > 6 ) {
				d.DAY = 1;

				if( d.WEEK > 3 ){
					d.WEEK = 1;
					++d.MONTH;
				}else{
					++d.WEEK;
					NewDayWeek('Week');
				};
			}else{
				++d.DAY;
				NewDayWeek('Day');
			};


			/* Reset heroes steps */
			EVENTER.trigger("movement.reset");


			/* Hide - Show */
			army.style.display = 'none';
			new_day_block.style.display = 'block';

			setTimeout(function(){
				NEWDAY = true;
				army.style.display = 'block';
				new_day_block.style.display = 'none';
				img_object.src = "";
				EVENTER.trigger("updateDate");
			}, SHOWTIMEOUT);


			/* Increase all resources */
			//Get all mines
			var o = DATA.UNITS.OBJECTS;
			for( var m = 0; m < o.length; m++ ){
				if( o[m].data.type === 'mine' && o[m].color === DATA.PLAYER_COLOR ){
					DATA.RESOURCES[o[m].name.toUpperCase()] += o[m].data.inc;
					EVENTER.trigger("resources.update", [o[m].name]);
				};
			};
		};

	}, false);



    /**
	 * API
	 */
	
	//Update hero steps
	EVENTER.on("updateSteps", function(data){
		update(data, CONST.NORMALPATHLENGTH, "steps");
	});
	
	//Update hero steps
	EVENTER.on("updateMana", function(){
		update(data, CONST.NORMALMANAVAL, "mana");
	});
	
	//Update hero steps
	EVENTER.on("heroDeactive", function(){
		Lib_.RemoveClass(heroTab, "hero_active");
		
		//Change cursor to simple
		Lib_.ToogleClass(DATA.CACHE.get("Map"), "unselected-cursor");
	});

	//Update current date
	EVENTER.on("updateDate", function(){
		day.getElementsByTagName('span')[0].innerHTML = DATA.DATE.DAY;
		week.getElementsByTagName('span')[0].innerHTML = DATA.DATE.WEEK;
		month.getElementsByTagName('span')[0].innerHTML = DATA.DATE.MONTH;
	});
});












