Heroes.MoveMap = (function(){
  
  "use strict";

	var MAPSPEED = Heroes.MAP.size.minW;

	var Movemenets = {
		"left": {
			id: "BorderLeft",
			dir: [1, 0]
		},
		
		"right": {
			id: "BorderRightBlack",
			dir: [-1, 0]
		},
		
		"top": {
			id: "BorderTop",
			dir: [0, 1]
		},
		
		"bottom": {
			id: "BorderBottom",
			dir: [0, -1]
		},
		
		"bottomRight": {
			id: "ColorCornerBottomRight",
			dir: [-1, -1]
		},
		
		"bottomLeft": {
			id: "ColorCornerBottomLeft",
			dir: [1, -1]
		},
		
		"topRight": {
			id: "ColorCornerTopRight",
			dir: [-1, 1]
		},
		
		"topLeft": {
			id: "ColorCornerTopLeft",
			dir: [1, 1]
		}
	};


	
	//Get all sides
	var Wrapper  = DATA.CACHE.get("Wrapper"),
			timer    = DATA.TIMER,
			side     = null,
			wrapperX = Wrapper.offsetLeft,
			wrapperY = Wrapper.offsetTop,
			wrapperW = Wrapper.offsetWidth,
			wrapperH = Wrapper.offsetHeight,
			gameW    = DATA.CACHE.get("Game").offsetWidth,
			gameH    = DATA.CACHE.get("Game").offsetHeight,
			mW       = Heroes.MAP.size.minW,
			mH       = Heroes.MAP.size.minH;

			
	var UpdateData = function(){
		wrapperX = Wrapper.offsetLeft,
		wrapperY = Wrapper.offsetTop,
		wrapperW = Wrapper.offsetWidth,
		wrapperH = Wrapper.offsetHeight,
		gameW    = DATA.CACHE.get("Game").offsetWidth,
		gameH    = DATA.CACHE.get("Game").offsetHeight;
	};
			
			
	/**
	 * Move map right
	 */
	var MoveR = function(){
		var start_y = 0;
		var h_ = (gameH + wrapperY) << 0;
		for( var y_ = start_y; y_ < h_ + start_y + 1; y_++){
			var p_x = ((-1*wrapperX + gameW)/mW << 0);
			if( DATA.CACHE.get("x"+p_x+"y"+y_) ) DATA.CACHE.get("x"+p_x+"y"+y_).style.display = "block";
		};
	};
	
	
	/**
	 * Move map left
	 */
	var MoveL = function(){
		var start_y = (-1*wrapperY)/mH << 0;
		var h_ = (gameH/mH) << 0;
		for( var y_ = start_y; y_ < h_ + start_y + 1; y_++){
			var p_x = ((-1*wrapperX)/mW << 0);
			if( DATA.CACHE.get("x"+p_x+"y"+y_) ) DATA.CACHE.get("x"+p_x+"y"+y_).style.display = "block";
		};
	};
	
	
	/**
	 * Move map bottom
	 */
	var MoveB = function(){
		var start_x = (-1*wrapperX)/mW << 0;
		var w_ = (gameW/mW) << 0;
		for( var x_ = start_x; x_ < w_ + start_x + 1; x_++ ){
			var p_y = ((-1*wrapperY + gameH)/mH << 0);
			if( DATA.CACHE.get("x"+x_+"y"+p_y) ) DATA.CACHE.get("x"+x_+"y"+p_y).style.display = "block";
		};
	};
			
			
	/**
	 * Move map top
	 */
	var MoveT = function(){
		var start_x = (-1*wrapperX)/mW << 0;
		var w_ = (gameW/mW) << 0;
		for( var x_ = start_x; x_ < w_ + start_x + 1; x_++ ){
			var p_y = ((-1*wrapperY)/mH << 0);
			if( DATA.CACHE.get("x"+x_+"y"+p_y) ) DATA.CACHE.get("x"+x_+"y"+p_y).style.display = "block";
		};
	};
			
			
  var Move = function(i, speed){  
		UpdateData();
	
    timer.start({
      name: "m_" + Movemenets[i].id,
      action: function(){
        if(
          wrapperX <= 0 &&
            wrapperY <= 0 &&
            wrapperX + wrapperW >= gameW &&
            wrapperY + wrapperH >= gameH
          ){
						wrapperX += Movemenets[i].dir[0] ? Movemenets[i].dir[0] * (speed || MAPSPEED) : 0;
						wrapperY += Movemenets[i].dir[1] ? Movemenets[i].dir[1] * (speed || MAPSPEED) : 0;
						
						if( wrapperY > 0 ) {
							wrapperY = 0;
							return false;
						};
						if( wrapperX > 0 ) {
							wrapperX = 0;
							return false;
						};
						if( wrapperX + wrapperW < gameW ) {
							wrapperX = gameW - wrapperW;
							return false;
						};
						if( wrapperY + wrapperH < gameH ) {
							wrapperY = gameH - wrapperH;
							return false;
						};

						
						//CODE OPTIMIZATION : hide terra blocks which we not use and show them in player running process
						//Make terra incognita blocks visible
						switch(i){
							case "right":
								MoveR(); break; 
							case "left":
								MoveL(); break; 
							case "top":
								MoveT(); break; 
							case "bottom":
								MoveB(); break; 
							case "bottomRight":
								MoveB(); MoveR(); break; 
							case "bottomLeft":
								MoveB(); MoveL(); break; 
							case "topRight":
								MoveT(); MoveR(); break; 
							case "topLeft":
								MoveT(); MoveL(); break; 
						};

						
						Wrapper.style.left = wrapperX + "px";
						Wrapper.style.top = wrapperY + "px";
						
						//Redraw mini-map
						Lib_.ExecuteLimiter(function(){
								EVENTER.trigger("draw");
								EVENTER.trigger("drawFrame");
						},[], 500);
        };
      }
    });
  };


  var MoveOnValue = function(dir, value, animate){
    var wrapperX = Wrapper.offsetLeft,
        wrapperY = Wrapper.offsetTop;

    wrapperX += Movemenets[dir].dir[0] ? Movemenets[dir].dir[0] * value : 0;
    wrapperY += Movemenets[dir].dir[1] ? Movemenets[dir].dir[1] * value : 0;

		if( animate ){
			$(Wrapper).animate({
				"left": wrapperX,
				"top": wrapperY
			}, 500);
		}else{
			Wrapper.style.left = wrapperX + "px";
			Wrapper.style.top = wrapperY + "px";
		};
		
		//Redraw mini-map
		Lib_.ExecuteLimiter(function(){
				EVENTER.trigger("draw");
				EVENTER.trigger("drawFrame");
		},[], 500);
  };

	
	var SetTo = function(p){
		var p_x = p.data.x || p.x,
			p_y = p.data.y || p.y,
			g_w = DATA.CACHE.get("Game").offsetWidth,
			g_h = DATA.CACHE.get("Game").offsetHeight;
		
		var prevposX = DATA.CACHE.get("Wrapper").offsetLeft, 
			curposX, 
			prevposY = DATA.CACHE.get("Wrapper").offsetTop, 
			curposY;
		
		if( ((p_x - (g_w/2)) + g_w) > DATA.CACHE.get("Wrapper").offsetWidth ){
			DATA.CACHE.get("Wrapper").style.left = -1*(DATA.CACHE.get("Wrapper").offsetWidth - g_w)+"px";
		}else{
			DATA.CACHE.get("Wrapper").style.left = -1*(p_x - (g_w/2))+"px";
		};
		
		if( ((p_y - (g_h/2)) + g_h) > DATA.CACHE.get("Wrapper").offsetHeight ){
			DATA.CACHE.get("Wrapper").style.top = -1*(DATA.CACHE.get("Wrapper").offsetHeight - g_h)+"px";
		}else{
			DATA.CACHE.get("Wrapper").style.top = -1*(p_y - (g_h/2))+"px";
		};
		
		if( ((p_x - (g_w/2)) + g_w) < DATA.CACHE.get("Game").offsetWidth ){
			DATA.CACHE.get("Wrapper").style.left = 0;
		};
		
		if( ((p_y - (g_h/2)) + g_h) < DATA.CACHE.get("Game").offsetHeight ){
			DATA.CACHE.get("Wrapper").style.top = 0;
		};
		
		curposX = DATA.CACHE.get("Wrapper").offsetLeft;
		curposY = DATA.CACHE.get("Wrapper").offsetTop;
		
		EVENTER.trigger("drawTerraArea", {
			x:-1*curposX + DATA.CACHE.get("Game").offsetWidth, 
			y:-1*curposY + DATA.CACHE.get("Game").offsetHeight, 
			dx: prevposX-curposX, 
			dy: prevposY-curposY,
			w: DATA.CACHE.get("Game").offsetWidth,
			h: DATA.CACHE.get("Game").offsetHeight
		});
	};
	
	
	
	for( var i in Movemenets ){
		side = DATA.CACHE.get(Movemenets[i].id);
		
		;(function(i){
			
			//Start moving map
			side.addEventListener("mouseover", function(){
				new Move(i);
			});
			
			//Stop map moving
			side.addEventListener("mouseout", function(){
				timer.remove("m_" + Movemenets[i].id);
			});
			
		}(i));
	};


  //Moving API
  return {

    /**
     * @param dir Number of map moving direction
     * 1 - Left
     * 2 - Right
     * 3 - Top
     * 4 - Bottom
     * @param speed Number - speed in pixel
     */
    Move: function(dir, speed){
        Move(dir, speed);
    },

    /**
     * Move map on define value
     */
    MoveOn: function(dir, value, animate){
        MoveOnValue(dir, value, animate);
    },
    
    /**
     * Update window objects geometric params
     */
    Update: function(){
			UpdateData();
    },
		
	/**
	 * Set map position according to active element( PLAYER OR BUILD )
	 */
	SETTOACTIVE: function(p){
		SetTo(p);
	}

  };
});
