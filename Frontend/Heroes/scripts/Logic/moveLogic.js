Heroes.MoveLogic = (function(event, nativeEvent){
  
  "use strict";
  
  var MAPMODEL       = DATA.MAPMODEL,
      PLAYER         = DATA.PLAYER_ACTIVE,
      TERRAINCOGNITA = DATA.TERRAINCOGNITA,
      th             = this,
      point          = MAPMODEL[event.y][event.x],
      clsCANTMOVE    = "terra_block",
	  AllObj         = [];
  
  
  
  this.MoveTo = function(collisionObject, callback){

      /**
       * First show path then go
       */
      if( !PLAYER.path.event ) PLAYER.path.event = event;
      
      if( PLAYER.path.event.x === event.x && PLAYER.path.event.y === event.y ){
        DATA.PLAYER_MOVEMENT.moveStatus = DATA.PLAYER_MOVEMENT.moveStatus;
      }else{
        DATA.PLAYER_MOVEMENT.moveStatus = true;
      };


      if( DATA.PLAYER_MOVEMENT.moveStatus ){
        
        DATA.PLAYER_MOVEMENT.moveStatus = false;
        
        //Set this part of object like a free ceil
        point.v = -1;
        
        //Draw player movement path
        PLAYER.path.steps = Heroes.DrawPath(event, PLAYER);
        
        //Set data of point where we need to go
        PLAYER.path.event = event;

        //Set previous value
        if( callback ) point.v = -4;
        
        if( PLAYER.moveValue === 0 ) {
          DATA.PLAYER_MOVEMENT.move = true;
          DATA.PLAYER_MOVEMENT.moveStatus = true;
        };
        
      }else{
        
        DATA.PLAYER_MOVEMENT.moveStatus = true;
        
        //Set flag - player can't move while not finish current steps
        DATA.PLAYER_MOVEMENT.move = false;

        //Say player - RUN
        callback ? PLAYER.Go(collisionObject, PLAYER, TERRAINCOGNITA, DATA, callback) : PLAYER.Go(collisionObject, PLAYER, TERRAINCOGNITA, DATA);
      };
  };
  


  //If player can move
  if( PLAYER && !!DATA.PLAYER_FOCUS && !!DATA.PLAYER_MOVEMENT.move && nativeEvent.target.className.indexOf(clsCANTMOVE) == -1 ){
   
    
      //If we're not on the same place where we click
      if( event.x !== PLAYER.data.x || event.y !== PLAYER.data.y ){


          /**
           * For all points where we can go
           */
          if( point.v >= -1 && point.v != 0 ){
              th.MoveTo();
          }


          /**
           * For objects which we must circumnavigating
           */
          else if( point.v == -4 ){

			//Concat all objects in one array
			AllObj = AllObj.concat(DATA.UNITS.OBJECTS, DATA.UNITS.BUILDINGS, DATA.UNITS.RESOURCES)
			
            //Get object type
            for( var i = 0; i < AllObj.length; i++ ){
              if( 
                AllObj[i].x + AllObj[i].data.mX === event.x * Heroes.MAP.size.minW &&
                  AllObj[i].y + AllObj[i].data.mY === event.y * Heroes.MAP.size.minH
              ){
                  ;(function(o){
                    th.MoveTo(AllObj[i], function(){

                      //Call object action method after finish
                      if( o.data.action ) o.data.action(o);
                      
                    });
                  }(AllObj[i]))
              };
            };
			
          };
      };
  };
});


/* NEW STRUCTURE */
//TO DO: REFACTOR
$(function(){

  	/* Reset heroes steps */
	EVENTER.on("movement.reset", function(){
		var p = DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR];
		for( var i in p ){
			p[i].moveValue = p[i].startSteps;
			p[i].data.steps = p[i].startSteps;
			EVENTER.trigger("updateSteps", [p[i].startSteps, p[i]]);
		};
		Heroes.CORE.ClearHeroSteps();
	}); 
});
