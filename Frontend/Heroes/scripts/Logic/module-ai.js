window.addEventListener('load', function(){

    'use strict';

    var ARMY, OPPONENT, BATTLE, PATH,
        ATTACK_UNITS = [],
        QUICKEST_UNIT,
        CURRENT_SLOT,
        OPPONENT_STRONGEST_UNIT,
        OPPONENT_STRONGEST_SLOT,
        OPPONENT_MAX_MOB_SPEED,
        START_ATTACK_TIMEOUT = 500,
        ATTACKED_CELL_MOVE_TIME = 50,
        UNIT_ATTACK_TIMEOUT = 300,
        BATTLE_MAP_CONFIG = {
            cells: 15,
            rows: 11,
            w: 40,
            h: 47,
            cellXMargin: 20,
            cellYMargin: 10
        },

        path, grid, finder;

    //Создаем сетку поля боя
    function getMapMatrix(data) {
        var row, matrix = [];

        for( var i = 0; i < BATTLE_MAP_CONFIG.rows; i++ ) {
            row = [];
            for( var j = 0; j < BATTLE_MAP_CONFIG.cells; j++ ) row.push(0);            
            matrix.push(row);
        };

        //TODO: own units has 0 in cell

        Object.keys(BATTLE).forEach(function(group) {
            Object.keys(BATTLE[group]).forEach(function(unit) {
                matrix[BATTLE[group][unit].row][BATTLE[group][unit].c] = 1;
            }); 
        });

        matrix[data.from[1]][data.from[0]] = 0;
        matrix[data.to[1]][data.to[0]] = 0;
       
        return matrix;
    };


    //Выбираем всех юнитов которые будут ходить( все у которых скорость выше скорости самого быстрого моба оппонента )
    function getUnitsForAttack() {
        OPPONENT_MAX_MOB_SPEED = 0;

        //Ищем самого быстрого юнита оппонента 
        Object.keys(OPPONENT).forEach(function(type) {
            if( OPPONENT[type].mob.speed > OPPONENT_MAX_MOB_SPEED ) {
                OPPONENT_MAX_MOB_SPEED = OPPONENT[type].mob.speed;
            };
        });

        //Ищем своих самых быстрых юнитов( со скоростью выше опонента )
        Object.keys(ARMY).forEach(function(type) {
            if( ARMY[type].mob.speed > OPPONENT_MAX_MOB_SPEED ) {
                ATTACK_UNITS.push(ARMY[type]);
            }
        });

        //Сортируем самых быстрых юнитов по скорости
        ATTACK_UNITS.sort(function(a, b) {
            return b.mob.speed - a.mob.speed;
        });
    };


    /**
     * Find strongest slot
     * @params {Array} units Array of units 
     */
    function _findStrongest(units) {
        var middleDmg, slots,
            slotCount = 0,
            unitDamage = 0;

        units.forEach(function(unit) {
            if( unit.mob.damage.length > 1 ) {
                middleDmg = (unit.mob.damage[0] + unit.mob.damage[1]) / 2;
            } else {
                middleDmg = unit.mob.damage[0];
            };
            if( unitDamage < middleDmg ) {
                unitDamage = middleDmg;
                OPPONENT_STRONGEST_UNIT = unit.mob.name;
            };    
        });

        slots = OPPONENT[OPPONENT_STRONGEST_UNIT].slots;

        Object.keys(slots).forEach(function(slot) {
            if( slots[slot].n > slotCount ) {
                slotCount = slots[slot].n;
                slots[slot].row = parseInt(slot);
                OPPONENT_STRONGEST_SLOT = slots[slot];
            };
        });
    };

    /**
     * Find strongest shooter or ground unit
     */
    function findOpponentUnitForAttack() {
        var shooters = [],
            grounds = [];

        Object.keys(OPPONENT).forEach(function(unit){ 
            if( OPPONENT[unit].mob.shots ) {
                shooters.push(OPPONENT[unit]);
            } else {
                grounds.push(OPPONENT[unit]);
            };
        });

        if( shooters.length ) {
            _findStrongest(shooters);
        } else {
            _findStrongest(grounds);
        };
    };

    function findUnit(slot, type) {
        var unit,
            armyObject = type === 'own' ? BATTLE.opponent : BATTLE.own;

        Object.keys(armyObject).forEach(function(key) {
            if( armyObject[key].c === slot.c && armyObject[key].row === slot.r) {
                unit = armyObject[key];
            };
        });
        return unit;
    };

    function isEmptyCell(cell) {
        var result = null;

        Object.keys(BATTLE.opponent).forEach(function(key) {
            if( BATTLE.opponent[key].c === cell[0] && BATTLE.opponent[key].row === cell[1]) {
                result = BATTLE.opponentt[key];
            };
        });
        Object.keys(BATTLE.own).forEach(function(key) {
            if( BATTLE.own[key].c === cell[0] && BATTLE.own[key].row === cell[1]) {
                result = BATTLE.own[key];
            };
        });

        return result;
    };

    function __getUnit__(attackedUnit, attackingUnit, circle) {
        var circle, keys, cell, 
            temp = [],
            __cell__ = null,
            __isPath__ = null,
            sides = [[0,1], [0,-1], [1,0], [-1,0]];
        
       if( !circle ) {
            circle = {
                "1,0"  : [1, 0],
                "0,1"  : [0, 1],
                "-1,0" : [-1, 0],
                "0,-1" : [0, -1],
                "-1,-1": [-1, -1],
                "-1,1" : [-1, 1]
            };
        }

        keys = Object.keys(circle);

        for(var i = 0; i < keys.length; i++ ) {
            if( 
                attackedUnit.c + circle[keys[i]][0] >= 0 && 
                attackedUnit.row + circle[keys[i]][1] >= 0 
            ) 
            {   
                __cell__ = [attackedUnit.c + circle[keys[i]][0], attackedUnit.row + circle[keys[i]][1]];
                
                if( __cell__ ) {
                    grid = new PF.Grid(BATTLE_MAP_CONFIG.cells, BATTLE_MAP_CONFIG.rows, getMapMatrix({
                        from:[attackingUnit.c, attackingUnit.row],
                        to:[__cell__[0], __cell__[1]]
                    }));
                    __isPath__ = finder.findPath(__cell__[0], __cell__[1], attackingUnit.c, attackingUnit.row, grid).reverse();

                    if( __isPath__.length ) {
                        path = __isPath__;
                        console.log(path);
                        break;
                    }
                }
            }

            if( i === keys.length - 1 ) {
                for( var j = 0; j < keys.length; j++ ) {
                    for( var s = 0; s < sides.length; s++ ) {
                        cell = (circle[keys[j]][0] + sides[s][0])+ "," +(circle[keys[j]][1] + sides[s][1]);
                        
                        if( !circle[cell] ) {
                            temp[cell] = [circle[keys[j]][0] + sides[s][0], circle[keys[j]][1] + sides[s][1]];      
                        }
                    }
                }

                circle = temp;

                __getUnit__(attackedUnit, attackingUnit, circle);
            }
        };
    }

    function attackStrongestUnit(callback) {
        var sprite, moveTime, to, toX, toY,
            lastCell,
            attackedUnit = findUnit(OPPONENT_STRONGEST_SLOT, 'opponent'), 
            attackingUnit = findUnit(CURRENT_SLOT, 'own');

        //Вычисляем путь к объекту
        console.time('PathToUnit');
        finder = new PF.AStarFinder(); 
        grid = new PF.Grid(BATTLE_MAP_CONFIG.cells, BATTLE_MAP_CONFIG.rows, getMapMatrix({
            from:[attackingUnit.c, attackingUnit.row],
            to:[attackedUnit.c, attackedUnit.row]
        }));
        path = finder.findPath(attackedUnit.c, attackedUnit.row, attackingUnit.c, attackingUnit.row, grid).reverse();

        //Если мы не можем добраться к юниту ищем кого бы ушатать вокруг чтобы добраться дл него
        if( !path.length ) {
            __getUnit__(attackedUnit, attackingUnit);
        };
        console.timeEnd('PathToUnit');
        
        //Вычисляем ячейку к которой надо двигаться и общее время движения к ней
        if( attackingUnit.mob.speed >= path.length ) {
            lastCell = path[path.length - 1];
            to = path[path.length - 2];
            moveTime = (path.length - 2) * ATTACKED_CELL_MOVE_TIME;
        } else {
            to = lastCell = path.slice(0, attackingUnit.mob.speed - 1).pop();
            moveTime = path.slice(0, attackingUnit.mob.speed - 1).length * ATTACKED_CELL_MOVE_TIME;
        };

        //Обновляем z-index юнита
        $(attackingUnit.dom).css('z-index', to[1] + 1);

        toX = (to[0] * BATTLE_MAP_CONFIG.w) + attackingUnit.mob['b_params'].dx + (to[1] % 2 ? 0 : BATTLE_MAP_CONFIG.cellXMargin);
        toY = (to[1] * BATTLE_MAP_CONFIG.h) - (to[1] * BATTLE_MAP_CONFIG.cellYMargin) - attackingUnit.mob['b_params'].h + BATTLE_MAP_CONFIG.h - BATTLE_MAP_CONFIG.cellYMargin;

        sprite = new Heroes.Sprite({
            p: attackingUnit.mob,
            el: attackingUnit.dom,
            step: attackingUnit.mob['b_params'].w,
            length: attackingUnit.mob['b_params'].sprites
        });
        
        sprite.setStartPosition(0, 0);
        sprite.animate();

        $(attackingUnit.dom).animate({
            left: toX,
            top: toY
        }, moveTime, 'linear', function() {
            sprite.stop();
            sprite.setStartPosition(0, attackingUnit.mob['b_params'].h);
            
            attackingUnit.c = to[1];

            //Если мы достигли цели
            if( attackedUnit.c === lastCell[0] && attackedUnit.row - 1 === lastCell[1] ) {

                //Расставляем лейблы юнитов в нужных положениях
                if( attackedUnit.c < attackingUnit.c ) {
                    attackedUnit.dom.dataset['number_position'] = 'left';
                    attackingUnit.dom.dataset['number_position'] = 'bottom';
                }
            };

            callback && callback();
        });
    };

    
    EVENTER.on('ai.turn', function(data) {
        var unit_counter = 0, 
            slot_counter = 0,
            unit, slot;
        
        ARMY = data.army;
        OPPONENT = data.opponent;
        BATTLE = data.battle;
      
        getUnitsForAttack(); 

        unit = ATTACK_UNITS[unit_counter];
        slot = unit.slots[slot_counter];
        
       function aiStep() {
            OPPONENT_STRONGEST_UNIT = unit.mob.name;
            CURRENT_SLOT = slot;
            
            findOpponentUnitForAttack();

            attackStrongestUnit(function() {
                slot = unit.slots[++slot_counter];
                
                if( !slot ) {
                    unit = ATTACK_UNITS[++unit_counter];
                    if( unit ) {
                        slot_counter = 0;
                        slot = unit.slots[slot_counter];
                    };
                };

                //Задержка перед ходом юнита
                setTimeout(function() {
                    slot ? aiStep() : EVENTER.trigger('ai.end', OPPONENT_MAX_MOB_SPEED);
                }, UNIT_ATTACK_TIMEOUT);
            });
        };

        //Задержка перед началом атаки
        setTimeout(aiStep, START_ATTACK_TIMEOUT);
    });
}, false);

