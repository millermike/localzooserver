﻿/**
 * Find shortest way between to points
 * @m Map array
 * @to Objects with (x, y)
 * @return Array of steps
 */
Heroes.FindPath = (function(map_, to, from){
  var th          = this,
      currentNode = null,
      g1          = null,
      g2          = null;

  //Convert our map to Array
  var m = [];
  for( var row = 0; row < map_.length; row++ ){
    for( var col = 0; col < map_[row].length; col++ ){
      if( map_[row][col].v == -2 ){
        m.push(1);
      }else if( map_[row][col].v == -3 ){
        m.push(1);
      }else if( map_[row][col].v == -4 ){
	  	m.push(1);
	  }
	  else{
        m.push(0);
      };
    };
  };  


  /**
   * Обеъект AStar для расчета кратчайшего пути от одной точки к другой, учинывая препятствия на пути
   * @constructor
   * @this {AStar}
   * @version 1.0
   * @param {Number} startX кооридната x стартовой точки 
   * @param {Number} startY кооридната y стартовой точки 
   * @param {Number} endX кооридната x стартовой точки 
   * @param {Number} endY кооридната y стартовой точки 
   * @param {Array} map карта, массив точек 
   * @param {Object} context объект для рисования 
   * @param {Number} cellSize размер ячейки 
   * @return {Array} path расчитаный путь 
   */
  function AStar (startX, startY, endX, endY, map) {
   
    //Инициализируем открытый и закрытый списки 
    this.openList = new Array();	
    this.closeList = new Array();
    
    //Инициализируем стартовый узел
    var startNode = new Node(startX, startY);
    
    //Добавляем начальный узел к открытому списку
    this.openList.push(startNode);
    
    //Делаем текущим первый узел из открытого списка и удаляем его оттуда.
    while (currentNode = this.openList.shift()) {
      
      //Перемещаем его в закрытый список
      this.closeList.push(currentNode);	    
      
        //Проверяем достигли ли мы уже конечной точки
        if(currentNode.x == endX && currentNode.y == endY) {
          var curr = currentNode,
              path = [];
          
          //Проходимся по всем родителям начиная от текущей точки, это и будет наш путь
          while(curr.parent) {
            path.push(curr);
            curr = curr.parent;
          };
          
          //Возвращаем найденный путь, предварительно поменяв порядок элементов на обратный (задом-наперед)
          return path;
        };
        
        //Для каждой из восьми соседних точек
        for (var x = currentNode.x - 1; x <= currentNode.x + 1; x++) {
          for (var y = currentNode.y - 1; y <= currentNode.y + 1; y++) {
            
            //Проверяем входит ли точка в пределы карты
            if (x >= 0 && y >= 0 && x < map_[0].length && y < map_.length) {
              
              //Если точка проходима и не находиться в закрытом списке
              var isAlreadyClose = this.isAlreadyList(this.closeList, x, y); 

              if (map[y*map_[0].length+x] != 1 && isAlreadyClose == -1) {		
                
                //Если точка еще не в открытом списке
                var isAlreadyOpen = this.isAlreadyList(this.openList, x, y); 
                
                if (isAlreadyOpen == -1) {
                  //Создаем соседнюю точку
                  var neighbor = new Node(x, y);						
                  //Делаем текущую клетку родительской для это клетки
                  neighbor.parent = currentNode;
                  //Рассчитываем H - эвристическое значение расстояния от ТЕКУЩЕЙ клетки до КОНЕЧНОЙ (только по вертикали или горизонтали, и не учитываются преграды на пути)
                  neighbor.calculateH(endX, endY);
                  //Рассчитываем G - стоимость передвижения из стартовой точки A к данной клетке, следуя найденному пути к этой клетке
                  neighbor.g = currentNode.g + neighbor.calculateG(currentNode.x, currentNode.y);
                  //Рассчитываем f как сумму g и h
                  neighbor.f = neighbor.g + neighbor.h;
                  //Добавляем к открытому списку
                  this.openList.push(neighbor);
                }
                
                //Если точка уже в открытом списке то проверяем, не дешевле ли будет путь через эту клетку
                else {
                  g1 = this.openList[isAlreadyOpen].g;
                  
                  //Пересчитываем G для ячейки из открытого списка относительно текущей ячейки
                  g2 = currentNode.g + this.openList[isAlreadyOpen].calculateG(currentNode.x, currentNode.y);
                  
                  //Для сравнения используем стоимость G
                  if (g1 > g2) {
                    
                    //Если это так, то меняем родителя клетки на текущую клетку
                    this.openList[isAlreadyOpen].parent = currentNode;
                    
                    //Устанавливаем новое пересчитанное g
                    this.openList[isAlreadyOpen].g = g2;
                  };
                };
              };
            };
          };
        };
        
        //Сортируем список по возрастанию F
        if (this.openList.length > 1) {
          this.openList.sort(this.sortList);
        };
    };
    return [];
  };
  
  AStar.prototype = {
    openList: null,
    closeList: null	
  };

  //Функция проверяет находиться ли уже узел с координатами x и y в открытом или закрытом списке
  AStar.prototype.isAlreadyList = function (list, x, y) {
    for (var i in list) {		
	  if (list[i].x == x && list[i].y == y){
        return i;
      };
    };
    return -1;
  };

  //Сортируем открытый список по возрастанию стоимости клетки (F)
  AStar.prototype.sortList = function(a, b) { 
    if (a.f > b.f)
      return 1;
    else if (a.f < b.f)
      return -1;
    else
      return 0;	
  };
  
  //Класс для узла.
  function Node(x, y) {
    this.x = x;
    this.y = y;
  };
  
  Node.prototype = {	
    /** @type {Number} g стоимость передвижения из одного узла к данному узлу */ 
    g: 0,
    /** @type {Number} h эвристическое значение расстояния от ТЕКУЩЕГО узла до КОНЕЧНОГО (только по вертикали или горизонтали, и не учитываются преграды на пути) */ 
    h: 0,
    /** @type {Number} f сумма h и g */ 
    f: 0,
    /** @type {Object} parent родительский узел */ 
    parent: null	
  };

  //Функция расчитывает H - эвристическое значение расстояния от ТЕКУЩЕЙ клетки до КОНЕЧНОЙ (только по вертикали или горизонтали, и не учитываются преграды на пути)  
  Node.prototype.calculateH = function (endX, endY) {
    this.h = (Math.abs(this.x - endX) + Math.abs(this.y - endY)) * 10;
  };

  //Расчитываем G - стоимость передвижения из стартовой точки A к данной клетке, следуя найденному пути к этой клетке  
  Node.prototype.calculateG = function (currentX, currentY) {		
    if (this.x != currentX && this.y != currentY) {
      return 14;
    };
    return 10;
  };

  
  //Get Array of steps
  this.FindSteps = function(){
    return new AStar(from.x, from.y, to.x, to.y, m);
  };


  
  /** 
   * Path find API
   */
  return {
    GetPath: function(){       
      return th.FindSteps();
    }
  };
});



