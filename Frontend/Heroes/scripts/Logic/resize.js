Heroes.Resize = (function(){

  "use strict"

  window.addEventListener("resize", function(){

    //Recount viewport geometry
    DATA.MAPINSTANCE.viewportSetGeometry();

    //Update game objects geometry parameters
    DATA.MAPMOVING.Update();

		//Show all terra on resize
		//TO DO: show only part of blocks( optimization )
		EVENTER.trigger("showAllTerra");
		
		//Redefine map properties
		DATA.GEOMETRY.SCREEN_W = DATA.CACHE.get("Game").offsetWidth;
		DATA.GEOMETRY.SCREEN_H = DATA.CACHE.get("Game").offsetHeight;

  });
});
