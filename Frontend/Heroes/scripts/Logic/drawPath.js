//Draw player path
Heroes.DrawPath = (function(e, PLAYER){
    
    var MAPMODEL        = DATA.MAPMODEL,
		STEPS_PATH      = "images/Sprites/steps.png",
        STEPS_Green     = 0,                           //Y position in sprite for green steps
        STEPS_Grey      = 32,                          //Y position in sprite for grey steps
        STEP_CLASS      = "step-move";
    


    //Get path steps
    var steps = new Heroes.FindPath(
      MAPMODEL, 
      {x: e.x, y: e.y},
      {x: PLAYER.data.x / Heroes.MAP.size.minW, y:PLAYER.data.y / Heroes.MAP.size.minH}
    ).GetPath();
      


    //Clear all steps
    DATA.CACHE.get("Steps").innerHTML = "";
  
  
  
    //Create step DIV
    var prevStep   = null;
    
    //Color of player steps (1 - Green, 0 - Grey)
    var stepsColor = 1;   
    
    //Number of possible player steps
    var stepsNum = PLAYER.moveValue;
    
    var stepsAll = steps.length;
    
        
    var createDIV = function(o, i, ln){
      
      var e = document.createElement("div");
      e.className = STEP_CLASS;
      e.setAttribute("style", "top:"+(o.y * Heroes.MAP.size.minH)+"px;left:"+(o.x * Heroes.MAP.size.minW)+"px;background-image:url("+STEPS_PATH+");");
      e.id = "s_"+o.y+"_"+o.x;

      if( stepsAll <= stepsNum ) stepsColor = 0;

      //Set step block BG
      this.setBG = function(x){
        var p = stepsColor ? STEPS_Grey : STEPS_Green;
        e.style.backgroundPosition = x+"px "+p+"px";
      };
      

      //Set finish position
      if( prevStep == null ) {
        this.setBG(224);
      }else {
        
        //Do not for first arrow
        if( o.y > prevStep.y && o.x == prevStep.x ){
          this.setBG(96);
        }
        else if( o.y < prevStep.y && o.x == prevStep.x ){
          this.setBG(0);
        }
        else if( o.x < prevStep.x && o.y == prevStep.y ){
          this.setBG(256);
        }
        else if( o.x > prevStep.x && o.y == prevStep.y ){
          this.setBG(288);
        }
        else if( o.y > prevStep.y && o.x > prevStep.x ){
          this.setBG(64);
        }
        else if( o.y > prevStep.y && o.x < prevStep.x ){
          this.setBG(32);
        }
        else if( o.y < prevStep.y && o.x < prevStep.x ){
          this.setBG(576);
        }
        else if( o.y < prevStep.y && o.x > prevStep.x ){
          this.setBG(608);
        };
        
        //If we have next step( diagonal merges )
        if( steps[i + 1] ){
          if( prevStep.y == o.y && prevStep.x > o.x && steps[i + 1].y > o.y ){
            this.setBG(320);
          }
          else if( prevStep.y == o.y && prevStep.x < o.x && steps[i + 1].y > o.y ){
            this.setBG(352);
          }
          else if( prevStep.x == o.x && o.y > prevStep.y && steps[i + 1].x < o.x && o.x > steps[ln].x){
            this.setBG(384);
          }
          else if( prevStep.x == o.x && o.y > prevStep.y && steps[i + 1].x > o.x && o.x < steps[ln].x){
            this.setBG(416);
          }
          else if( prevStep.y == o.y && prevStep.x > o.x && steps[i + 1].y < o.y && o.y > steps[ln].y ){
            this.setBG(448);
          }
          else if( prevStep.y == o.y && prevStep.x < o.x && steps[i + 1].y < o.y && o.y > steps[ln].y ){
            this.setBG(480);
          }
          else if( prevStep.x == o.x && o.y < prevStep.y && steps[i + 1].x < o.x && o.x > steps[ln].x){
            this.setBG(512);
          }
          else if( prevStep.x == o.x && o.y < prevStep.y && steps[i + 1].x > o.x && o.x < steps[ln].x){
            this.setBG(544);
          }
          else if( prevStep.y > o.y && o.x < prevStep.x && steps[i + 1].y == o.y && o.x > steps[ln].x){
            this.setBG(160);
          }  
          else if( prevStep.y > o.y && o.x > prevStep.x && steps[i + 1].y == o.y && o.x < steps[ln].x){
            this.setBG(192);
          };
        };
        
        //For correct view of first step
        if( i === steps.length - 1){
          if( o.x > o.parent.x && o.y < o.parent.y && o.x === steps[i - 1].x ){
            this.setBG(384);
          };
          if( o.x < o.parent.x && o.y < o.parent.y && o.x === steps[i - 1].x ){
            this.setBG(416);
          };
          if( o.x > o.parent.x && o.y < o.parent.y && o.y === steps[i - 1].y && o.x < steps[i - 1].x ){
            this.setBG(320);
          };
          if( o.x < o.parent.x && o.y < o.parent.y && o.y === steps[i - 1].y && o.x > steps[i - 1].x ){
            this.setBG(352);
          };
          if( o.x > o.parent.x && o.y > o.parent.y && o.x === steps[i - 1].x ){
            this.setBG(512);
          };
          if( o.x < o.parent.x && o.y > o.parent.y && o.x === steps[i - 1].x ){
            this.setBG(544);
          };
          if( o.x > o.parent.x && o.y > o.parent.y && o.y === steps[i - 1].y && o.x < steps[i - 1].x ){
            this.setBG(448);
          };
          if( o.x < o.parent.x && o.y > o.parent.y && o.y === steps[i - 1].y && o.x > steps[i - 1].x ){
            this.setBG(480);
          };
        };

      };
      
      //Remember previous point
      prevStep = o;
      
      DATA.CACHE.get("Steps").appendChild(e);
      
      //Decrease possible steps
      stepsAll--;
    };
    
    
    //Loop all steps  
    var ln = steps.length;
    for( var i = 0; i < ln; i++ ){
      createDIV(steps[i], i, ln - 1);
    };
    
    return steps;
});


