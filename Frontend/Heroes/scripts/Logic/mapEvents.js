Heroes.MapEvents = (function(){
  
  "use strict";
  
  
  var GetPointOnClick = function(callback){
    DATA.CACHE.get("Wrapper").addEventListener("click", function(e){
      callback({
      	x: Math.floor((e.pageX - DATA.CACHE.get("Game").offsetLeft - DATA.CACHE.get("Wrapper").offsetLeft) / Heroes.MAP.size.minW), 
      	y: Math.floor((e.pageY - DATA.CACHE.get("Game").offsetTop - DATA.CACHE.get("Wrapper").offsetTop) / Heroes.MAP.size.minH)
    	}, e);
    });
  };
  
  
  /**
   * Events API
   */
  return{
    Click: {
      GetPoint: function(callback){
        GetPointOnClick(callback);
      }
    }
  };
  
});


