window.addEventListener('load', function(){

	var res = ["tree", "hg", "stone", "sera", "crystals", "gems", "gold"];
	var panel      = DATA.CACHE.get('Resources');
	var itemsBlock = DATA.CACHE.getClass(panel, 'resources-items', 0);
	var blocks     = itemsBlock.getElementsByTagName('div');
	var sidebar    = DATA.CACHE.get('Sidebar');


    /* CONST */
    var TMPL_SHOW_DELAY = 6000;




	/* Set resources panel width */
	var SetResWidth = function(){
		var resources_width = window.outerWidth - sidebar.offsetWidth;
		itemsBlock.style.width = resources_width + "px";

		
	};

	/* Recount panel width on window resize */
	window.addEventListener('resize', SetResWidth , false);


	SetResWidth();



	/* Set all resources to panel */
	EVENTER.on("resources.init", function(){
		for( var i = 0; i < res.length; i++ ){
			DATA.CACHE.get("resource-" + res[i]).getElementsByTagName('span')[0].innerHTML = DATA.RESOURCES[res[i].toUpperCase()];
		};
	});

	/* Update resource */
	EVENTER.on("resources.update", function(res){

		DATA.CACHE.get("resource-" + res[0]).getElementsByTagName('span')[0].innerHTML = DATA.RESOURCES[res[0].toUpperCase()];


		/** 
		 * Show message in the bottom of sidebar 
		 */
		if( res[1] ){
			var wrapper = DATA.CACHE.get('PlayerArmyInfoWrapper');

			/* Only hide previous template */
			var prev = wrapper.childNodes[0];
			prev.style.display = 'none';

			/* Remove previous resource templates */
			if( wrapper.childNodes.length > 1 ){
				wrapper.removeChild(wrapper.childNodes[1]);	
			};

			/* Add template of resource */
			var temp = document.createElement('div');
			temp.innerHTML = Heroes.Tmpl.GetResource({
				name: res[0],
				amount: res[1]
			});
			wrapper.appendChild(temp);

			/* After delay show previous template */
			setTimeout(function(){
				if( temp.parentNode ){
					wrapper.removeChild(temp);
				};
				prev.style.display = 'block';
			}, TMPL_SHOW_DELAY);
		};
	});
	
}, true);
