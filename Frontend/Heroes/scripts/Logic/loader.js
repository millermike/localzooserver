window.addEventListener('load', function() {
    var resources = Heroes.config.resources,
        totalLength = 0,
        imgCounter = 0;
  
    Object.keys(resources).forEach(function(type) {
        totalLength += Object.keys(resources[type]).length;
        Object.keys(resources[type]).forEach(function(image) {
            var img = new Image();
            img.onload = function() {
                imgCounter++;
                if( imgCounter === totalLength ) {
                    EVENTER.trigger('load.finish');
                };
            };
            img.src = resources[type][image];
        });
    }); 

}, false);
