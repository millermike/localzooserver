window.addEventListener('load', function(){

    'use strict';

    var OPPONENT,
        PLAYER_ARMY,
        OPPONENT_ARMY,
        CURRENT_PLAYER = null,
        ATTACK_UNITS = [],
        BATTLE_UNITS = {
            'own': {},
            'opponent': {}
        },
        /**
         * cells - battle field width 
         * rows - battle field height
         * w, h - geometry size of cells in DOM
         */
        BATTLE_MAP_CONFIG = {
            cells: 15,
            rows: 11,
            w: 40,
            h: 47,
            cellYMargin: 10
        };



    /* Helpers BEGIN*/
    function capitalize(string){ 
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
    /* Helpers END */


    function renderUnit(data){
        var dom, label,
            bParams = data.item['b_params'],
            unitDomCell = DATA.CACHE.get('battle-' + data.row + '-' + data.cell);

        dom = document.createElement('div');
        dom.className = 'battleUnit unit_label_' + data.type;
        dom.id = data.id;

        if( bParams.stay ) {
            dom.setAttribute('style',
                'z-index:'+ (data.row + 1) +';'+  
                'background-image:url('+ bParams.sb +');' +
                    'background-position:'
                        + (bParams.stay.x * bParams.w) +'px '
                        + (bParams.stay.y * bParams.h) +'px;'
            );
        } else {
            dom.setAttribute('style', 'background-image:url('+ bParams.sb +');');
        };

        dom.style.width = bParams.w;
        dom.style.height = bParams.h;

        label = document.createElement('div');
        label.className = 'unit-number-label';
        label.innerHTML = data.n;
        dom.appendChild(label);

        dom.style.left = (BATTLE_MAP_CONFIG.w * data.cell + (bParams.dx || 0) + ((data.row + 1) % 2 ? 20 : 0)) + 'px';
        dom.style.top = ((BATTLE_MAP_CONFIG.h) * (data.row + 1) - data.item['b_params'].h - ((data.row + 1) * BATTLE_MAP_CONFIG.cellYMargin) + (bParams.dy || 0))  + 'px';

        DATA.CACHE.get('battle-units-dom').appendChild(dom);

        BATTLE_UNITS[data.own ? 'own' : 'opponent'][data.id] = {
            dom: dom,
            parent: unitDomCell,
            c: data.cell,
            row: parseInt(data.row),
            name: data.item.name,
            n: data.n,
            mob: data.item
        };
    };

    function drawHero(data) {
        var hero_position, hero_type, hero;

        if( data.HERO ) {
            hero_position = data.own ? '' : '_right';
            hero_type = data.own ? 'Own' : 'Opponent';
            hero = document.createElement('div');
            hero.className = 'battleHero' + hero_type + ' HERO_battle_' + data.HERO.data.color + hero_position;
            hero.innerHTML = '<div class="battle_hero_flag_'+ data.HERO.data.color +'"></div>';
            DATA.CACHE.get('battle-dom').appendChild(hero);
        };
    };

    function setUnits(data){
        var army = data.army;

        drawHero(data);

        Object.keys(army).forEach(function(item, alfa_iterator) {
            army[item].mob = new Heroes.Objects['Unit' + capitalize(item)]();
            army[item].slots.forEach(function(unit, iterator) {
                if( unit.r > BATTLE_MAP_CONFIG.rows - 1 ) {
                   console.error('Incorrect unit position by Y.\n Y position more then ' + (BATTLE_MAP_CONFIG.rows - 1)); 
                } else {
                    renderUnit({
                        id: new Date().getTime() + '' + iterator + alfa_iterator + 'battle_unit_' + (data.own ? 'right' : 'left'),
                        row: unit.r,
                        cell: unit.c,
                        n: unit.n, 
                        data: army,
                        type: data.own ? 'right' : 'left',
                        item: army[item].mob,
                        own: data.own     
                    });
                }
            });
        });
    };

    function whoIsTheFirst() {
        var players = [DATA.PLAYER_ACTIVE, OPPONENT],
            playerSpeed = 0,
            opponentSpeed = 0; 
            
        Object.keys(PLAYER_ARMY).forEach(function(unit) {
            if( playerSpeed < PLAYER_ARMY[unit].mob.speed ) {
                playerSpeed = PLAYER_ARMY[unit].mob.speed;
            };
        });
        Object.keys(OPPONENT_ARMY).forEach(function(unit) {
            if( opponentSpeed < OPPONENT_ARMY[unit].mob.speed ) {
                opponentSpeed = OPPONENT_ARMY[unit].mob.speed;
            };
        });

        if( playerSpeed > opponentSpeed ) {
            CURRENT_PLAYER = players[0];
        } else if( playerSpeed < opponentSpeed ) {
            CURRENT_PLAYER = players[1];
        } else {
            //TODO: ввести понятие нападающий
            CURRENT_PLAYER = players[0]; //Если скорости равны то ходит нападающий    
        };
    };

    function setMapObjects() { /* TODO: this */ };

    //Начать бой
    function startBattle() {
       if( CURRENT_PLAYER === OPPONENT ) {
            $(document).off('click', '#battle-grid');

            EVENTER.trigger('ai.turn', {
                army: OPPONENT.army, 
                opponent: DATA.PLAYER_ACTIVE.data.army, 
                battle: BATTLE_UNITS
            });

            EVENTER.on('ai.end', function(data) {
                EVENTER.trigger('player.turn', {
                    army: OPPONENT.army, 
                    opponent: DATA.PLAYER_ACTIVE.data.army, 
                    battle: BATTLE_UNITS,
                    unitMaxSpeed: data
                });
            });
        } else {
            EVENTER.trigger('player.turn', {
                army: OPPONENT.army, 
                opponent: DATA.PLAYER_ACTIVE.data.army, 
                battle: BATTLE_UNITS
            });
        };
    };

    //Определяем самого быстрого игрока
    function getQuickestUnit(army) {
        var quickest_unit,
            mob, speed = 0;

        Object.keys(army).forEach(function(unit) {
            if( army[unit].mob.speed > speed ) {
                speed = army[unit].mob.speed;
                quickest_unit = unit; 
            };
        });
        return quickest_unit;
    };


    /*  API  */
    EVENTER.on("battle.start", function(e){
        OPPONENT = e;
        PLAYER_ARMY = DATA.PLAYER_ACTIVE.data.army;
        OPPONENT_ARMY = OPPONENT.army;

        setUnits({
            HERO: DATA.PLAYER_ACTIVE, 
            army: PLAYER_ARMY, 
            own: true
        });

        setUnits({
            HERO: e.HERO, 
            army: OPPONENT_ARMY
        });

        setMapObjects();

        whoIsTheFirst();
        startBattle();
    });

}, false);


$(document).on('click', '#battle-grid', function(e) {
    playerMove(e); 
});
