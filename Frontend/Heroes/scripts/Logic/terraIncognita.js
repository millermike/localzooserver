Heroes.TerraIncognita = (function(){
  
  var blocksToRemove = [],
      mpmodel       = DATA.MAPMODEL,
      units         = DATA.UNITS,
      terra_stack   = DATA.TERRAINCOGNITA,
      player        = DATA.PLAYER_ACTIVE,
      player_p      = DATA.PLAYER_ACTIVE.data,
      terraBlockCls = "terra_block",
      radiusBlocID  = "player-radius",
      player_radius = player.terraRadius + 1,
      corder_rad    = null,
      terra_el      = null,
      mW            = Heroes.MAP.size.minW,
      mH            = Heroes.MAP.size.minH,
      tl            = (player_p.x / mW) - player_radius,
      tr            = (player_p.x / mW) + player_radius,
      tt            = (player_p.y / mH) - player_radius,
      tb            = (player_p.y / mH) + player_radius;

	//Pre define background position values for terra incognita blocks
	var terraSprites = {
		"full"        : [ {x:0, y:0}, {x:96, y:0}, {x:128, y:0}, {x:160, y:0} ],
		"left"        : [ {x:128, y:160} ],
		"right"       : [ {x:160, y:160}, {x:0, y:160} ],
		"top"         : [ {x:32, y:160}, {x:96, y:160} ],
		"bottom"      : [ {x:64, y:160}, {x:64, y:0}, {x:32, y:0} ],
		"bottomLeft"  : [ {x:0, y:128}, {x:32, y:128} ],
		"bottomRight" : [ {x:160, y:128} ],
		"topRight"    : [ {x:96, y:128} ],
		"topLeft"     : [ {x:128, y:128}, {x:0, y:96} ]
	};

	//Terra incognita redraw variables
	var playerRadius = null,
		  pRLEFT       = null,
		  pRTOP        = null,
		  pRRIGHT      = null,
		  pRBOTTOM     = null;
      
	//Three types of conters for different sizes of visible area
	var corners = {};
	
	
	//Set corner radius
	if( player_radius <= 2 ){
		corder_rad = 0;
	}else if( player_radius <= 3 ){
		corder_rad = 1;
	}else{
		corder_rad = 2;
	};


  /**
   * Get terra incognita bloc by ID
   */
  var Get = function(x, y){
    return DATA.CACHE.get("x"+x+"y"+y);
  };   
      
      
  /**
   * Set random background
   */
  var SetRandoBg = function(el, arr){
    var num = Math.floor(Math.random() * arr.length);
    el.style.backgroundPosition = arr[num].x+"px "+arr[num].y+"px";
  };
      
      
  /**
   * Set terra incognita block Background
   */    
  var SetTerraBlockBG = function(el, pos){
    if( el.constructor === String ){
      DATA.CACHE.get(el).style.backgroundPosition = pos[0].x+"px "+pos[0].y+"px";
    }else{
      el.style.backgroundPosition = pos[0].x+"px "+pos[0].y+"px";
    };
  };


	
  /**
   * TERRA INCOGNITA API
   */
	 
	EVENTER.on("drawTerraArea", function(data){
		var w_ = Math.ceil(data.w / mW),
				h_ = Math.ceil(data.h / mH),
				px = -1*Math.ceil(DATA.CACHE.get("Wrapper").offsetLeft / mW),
				py = -1*Math.ceil(DATA.CACHE.get("Wrapper").offsetTop / mH);
		
		
		//Show left and right sides
		if( data.dx > 0 ){
			var w = Math.ceil(data.dx / mW);
			var startX = data.x / mW << 0;
			for( var i = 0; i < w; i++ ){
				for( var j = 0; j < h_; j++ ){
					if( DATA.CACHE.get("x"+(startX-i)+"y"+(py + j)) ) DATA.CACHE.get("x"+(startX-i)+"y"+(py + j)).style.display = "block";
				};
			};
		};
		
		//Show top and bottom sides
		if( data.dy > 0 ){
			var h = Math.ceil(data.dy / mH);
			var startY = data.y / mH << 0;
			for( var i = 0; i < h; i++ ){
				for( var j = 0; j < w_; j++ ){
					if( DATA.CACHE.get("x"+(px + j)+"y"+(startY-i)) ) DATA.CACHE.get("x"+(px + j)+"y"+(startY-i)).style.display = "block";
				};
			};
		};
		
		EVENTER.trigger("drawFrame");
	});
	

	/* Show all terra incognita blocks */
	EVENTER.on("showAllTerra", function(data){
		var terra = document.getElementsByClassName('terra_block');
		for( var i = 0; i < terra.length; i++ ){
			terra[i].style.display = "block";
		};
	});
	
	
    //Remove all hidden blocks
    EVENTER.on("terra.clear", function(){
        for( var i = 0; i < blocksToRemove.length; i++ ) {
            DATA.CACHE.get('Terra').removeChild(blocksToRemove[i]);
            blocksToRemove.splice(i, 1);
        }
    });

  return {
    drawTerraIncognita: function(){
      var player     = DATA.PLAYER_ACTIVE,
          playerLink = player.link;

			
		//Define player radius block
    playerRadius = {
			w: (player.terraRadius*2)*mW,
			h: (player.terraRadius*2)*mH,
			x: ((((player.terraRadius*2)*mW) - player.data.w)/2),
			y: ((((player.terraRadius*2)*mH) - player.data.h)/2)
		};
	  
	  
	  //Pre-count players data 
	  var predefinePlayersData = {};
	  for( var p in DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR] ){
			predefinePlayersData[p] = {
				tl :(DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR][p].data.x / mW) - player_radius,
				tr :(DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR][p].data.x / mW) + player_radius,
				tt :(DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR][p].data.y / mH) - player_radius,
				tb :(DATA.UNITS.PLAYERS[DATA.PLAYER_COLOR][p].data.y / mH) + player_radius
			};
	  };
	  
	  
      //Fill terra incognita map
		;(function(){
        var prop = 1, row  = [], size = Heroes.MAP.size;
        for( var i = 0; i < size.height / size.minH; i++ ){
          for( var j = 0; j < size.width / size.minW; j++ ){
							prop = null;
							
							//Loop all own players
							for( var p in predefinePlayersData ){
								var p_ = predefinePlayersData[p];
								if( prop !== 0 ){
									j >= p_.tl && j <= p_.tr && i >= p_.tt && i <= p_.tb ? prop = 0 : prop = 1;
								};
							};
							
							row.push(prop);
					};
					if( row.length >= size.width / size.minW ) {
								terra_stack.push(row);
								row = [];
					};
			};
		}());
      

	  
	  
		//Loop all own players and cut corners
		for( var p in predefinePlayersData ){
			var p_ = predefinePlayersData[p];
			corners = {
				 topLeft:[
					 [{x:p_.tl, y:p_.tt}],
					 [{x:p_.tl, y:p_.tt},{x:p_.tl+1, y:p_.tt},{x:p_.tl, y:p_.tt+1}],
					 [{x:p_.tl, y:p_.tt},{x:p_.tl+1, y:p_.tt},{x:p_.tl, y:p_.tt+1},{x:p_.tl+2, y:p_.tt},{x:p_.tl+1, y:p_.tt+1},{x:p_.tl, y:p_.tt+2},]
				 ],
				 topRight:[
					 [{x:p_.tr, y:p_.tt}],
					 [{x:p_.tr, y:p_.tt},{x:p_.tr-1, y:p_.tt},{x:p_.tr, y:p_.tt+1}],
					 [{x:p_.tr, y:p_.tt},{x:p_.tr-1, y:p_.tt},{x:p_.tr, y:p_.tt+1},{x:p_.tr-2, y:p_.tt},{x:p_.tr-1, y:p_.tt+1},{x:p_.tr, y:p_.tt+2}]
				 ],
				 bottomLeft:[
					 [{x:p_.tl, y:p_.tb}],
					 [{x:p_.tl, y:p_.tb},{x:p_.tl+1, y:p_.b},{x:p_.tl, y:p_.tb-1}],
					 [{x:p_.tl, y:p_.tb},{x:p_.tl+1, y:p_.tb},{x:p_.tl, y:p_.tb-1},{x:p_.tl+2, y:p_.tb},{x:p_.tl+1, y:p_.tb-1},{x:p_.tl, y:p_.tb-2}]
				 ],
				 bottomRight:[
					 [{x:p_.tr, y:p_.tb}],
					 [{x:p_.tr, y:p_.tb},{x:p_.tr-1, y:p_.tb},{x:p_.tr, y:p_.tb-1}],
					 [{x:p_.tr, y:p_.tb},{x:p_.tr-1, y:p_.tb},{x:p_.tr, y:p_.tb-1},{x:p_.tr-2, y:p_.tb},{x:p_.tr-1, y:p_.tb-1},{x:p_.tr, y:p_.tb-2}]
				 ]
			};
			
			//Fill player area corners
			for( var c in corners ){
				for( var ci = 0; ci < corners[c][corder_rad].length; ci++ ){
					if( terra_stack[corners[c][corder_rad][ci].y] ){
					terra_stack[corners[c][corder_rad][ci].y][corners[c][corder_rad][ci].x] = 1;
					};
				};
			};
		};
	  
	  
	  
	  
		var g_x = -1 * DATA.CACHE.get("Wrapper").offsetLeft,
				g_y = -1 * DATA.CACHE.get("Wrapper").offsetTop;
				g_w = DATA.CACHE.get("Game").offsetWidth,
				g_h = DATA.CACHE.get("Game").offsetHeight;
				
      for( var terra = 0; terra < terra_stack.length; terra++ ){
        for( var terra_row = 0; terra_row < terra_stack[terra].length; terra_row++ ){
          if( terra_stack[terra][terra_row] == 1 ){
            
            terra_el           = document.createElement("div");
            terra_el.className = terraBlockCls;
            terra_el.id        = "x"+terra_row+"y"+terra;
            terra_el.setAttribute("style", "top:"+(terra*Heroes.MAP.size.minH)+"px;left:"+(terra_row*Heroes.MAP.size.minW)+"px");
            
						if( 
								terra_row*Heroes.MAP.size.minW < g_x - Heroes.MAP.size.minW ||
								terra_row*Heroes.MAP.size.minW > g_w + g_x ||
								terra*Heroes.MAP.size.minH < g_y - Heroes.MAP.size.minH ||
								terra*Heroes.MAP.size.minH > g_h + g_y
						){
							terra_el.style.display = "none";
						};
						
            //Set random background
            for( var bg = 0; bg < terraSprites.full.length; bg++ ){
            	SetRandoBg(terra_el, terraSprites.full);
            };
            

            //Right
            if( terra_stack[terra][terra_row + 1] == 0){
              SetRandoBg(terra_el, terraSprites.right);
            };

            //Left
            if( terra_stack[terra][terra_row - 1] == 0){
              SetRandoBg(terra_el, terraSprites.left);
            };

            //Bottom
            if( (terra - 1) >= 0 && (terra + 1) < mpmodel.length + 1 ){
              if( terra_stack[terra - 1][terra_row] == 0 ){
                SetRandoBg(terra_el, terraSprites.bottom);
              };
            };

            //Top
            if( (terra + 1) < mpmodel.length ){
              if( terra_stack[terra + 1][terra_row] == 0 ){
              	SetRandoBg(terra_el, terraSprites.top);
              };
            };

            //BOTTOM LEFT
            if( (terra - 1) >= 0 && (terra + 1) < mpmodel.length ){
              if( terra_stack[terra - 1][terra_row] == 0 && terra_stack[terra][terra_row + 1] == 0 ){
                SetRandoBg(terra_el, terraSprites.bottomLeft);
              };
            };

            //BOTTOM RIGHT
            if( (terra - 1) >= 0 && (terra + 1) < mpmodel.length ){
              if( terra_stack[terra - 1][terra_row] == 0 && terra_stack[terra][terra_row - 1] == 0 ){
                SetRandoBg(terra_el, terraSprites.bottomRight);
              };
            };

            //TOP RIGHT
            if( (terra + 1) < mpmodel.length ){
              if( terra_stack[terra + 1][terra_row] == 0 && terra_stack[terra][terra_row - 1] == 0 ){
                SetRandoBg(terra_el, terraSprites.topRight);
              };
            };

            //TOP LEFT
            if( (terra + 1) < mpmodel.length ){
              if( terra_stack[terra + 1][terra_row] == 0 && terra_stack[terra][terra_row + 1] == 0 ){
                //SetTerraBlockBG(terra_el, "Tshre11T");
                SetRandoBg(terra_el, terraSprites.topLeft);
              };
            };

            DATA.CACHE.get("Terra").appendChild(terra_el);
          }
        };
      };
    },
    
    
    drawTerraCorners: function(s){

      if(playerRadius){

        //Define visible are of hero
        pRLEFT   = s.x - ((playerRadius.w / mW)-(player.data.w/mW))/2;
        pRTOP    = s.y - ((playerRadius.w / mH)-(player.data.h/mH))/2;
        pRRIGHT  = pRLEFT + (playerRadius.w /mW);
        pRBOTTOM = pRTOP + (playerRadius.h /mH);


        for( var t = 0; t < terra_stack.length; t++ ){

					if( t >= pRTOP && t <= pRBOTTOM - 1 ){
					
						for( var tr = 0; tr < terra_stack[t].length; tr++ ){
						
							if( tr >= pRLEFT && tr <= pRRIGHT - 1 ){
							
								if( terra_stack[t][tr] === 1 ){

									//TOP
									if( Get(tr, t-1) ){
										SetTerraBlockBG(Get(tr, t-1), terraSprites.top);
									};

									//RIGHT
									if( Get(tr+1, t) ){
										SetTerraBlockBG(Get(tr+1, t), terraSprites.left);
									};

									//BOTTOM
									if( Get(tr, t+1) ){
										SetTerraBlockBG(Get(tr, t+1), terraSprites.bottom);
									};

									//LEFT
									if( Get(tr-1, t) ){
										SetTerraBlockBG(Get(tr-1, t), terraSprites.right);
									};

									//RIGHT BOTTOM
									if( Get(tr+1, t) && !Get(tr+1, t-1) ){
										SetTerraBlockBG(Get(tr+1, t), terraSprites.bottomRight);
									};

									//LEFT BOTTOM
									if( Get(tr-1, t) && !Get(tr-1, t-1) ){
										SetTerraBlockBG(Get(tr-1, t), terraSprites.bottomLeft);
									};


									//DIAGONAL BLOCKS
									if( Get(tr-1, t) && !Get(tr-1, t+1) ){
										SetTerraBlockBG(Get(tr-1, t), terraSprites.topLeft);
									};
									if( Get(tr, t-1) && !Get(tr-1, t-1)){
										SetTerraBlockBG(Get(tr, t-1), terraSprites.topRight);
									};
									if( Get(tr+1, t) && !Get(tr+1, t+1)){
										SetTerraBlockBG(Get(tr+1, t), terraSprites.topRight);
									};
									if( Get(tr, t-1) && !Get(tr+1, t-1)){
										SetTerraBlockBG(Get(tr, t-1), terraSprites.topLeft);
									};
									if( Get(tr, t+1) && !Get(tr+1, t+1) ){
										SetTerraBlockBG(Get(tr, t+1), terraSprites.bottomLeft);
									};
									if( Get(tr, t+1) && !Get(tr-1, t+1) ){
										SetTerraBlockBG(Get(tr, t+1), terraSprites.bottomRight);
									};


									//Remove terra incognita blocks
									Get(tr, t).style.display = "none";
									if( Get(tr, t) ) {
                                        //Get(tr, t).parentNode.removeChild(Get(tr, t));
                                        blocksToRemove.push(Get(tr, t))
                                    }
									terra_stack[t][tr] = 0;
								};
							};
							
							if( tr > pRRIGHT - 1 ) break;
						};
					};
					
					if(  t > pRBOTTOM - 1 ) break;
        };
    };
    }
  };


});

