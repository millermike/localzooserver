/**
 * Caching of DOM elements geting by ID
 * return Obejct {} object with API
 */
Heroes.Cache = (function(){
  
  "use strict";

	var c = {};

	return {
		get: function(id){
			if( c[id] ){
				return c[id];
			}else{
				return c[id] = document.getElementById(id);
			};
		},

    //Cache not using
		getClass: function(parent, cls, eq){
			if( eq !== undefined ){
				return parent.getElementsByClassName(cls)[eq];
			}else{
				return parent.getElementsByClassName(cls);
			};  
		}
	};
});
