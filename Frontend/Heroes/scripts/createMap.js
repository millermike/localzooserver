window.onload = function(){

	var data = {
		'level': null,
		'map': null
	};

	var LevelsBlock  = Lib_.GetId('Levels');
	var Levels       = LevelsBlock.getElementsByTagName("span");
	var SetMapLink   = Lib_.GetId('SetMap');
	var MapList      = Lib_.GetId('MapsList');
	var MapListTable = Lib_.GetId('MapsListTable');
	var MapListItems = MapListTable.getElementsByTagName('tr');

	

	var HideShowEl = function(e){
		if( window.getComputedStyle(e).display === 'none' ){
			e.style.display = 'block';
		}else{
			e.style.display = 'none';
		};
	};
	

	var HideEl = function(e){
		e.style.display = 'none';
	};
	

	var CreateMapsList = function(data){
        for( var m = 0; m < data.length; m++ ){
            var row = '<tr class="map-item" data-map="'+m+'">'+
                                    '<td>'+m+'</td>'+
                                    '<td class="w_250">'+data[m].name+'</td>'+
                                    '<td>'+data[m].size+'</td>'+
                                '</tr>';
            MapListTable.innerHTML += row;
        };
	};



	
	//Get game complexity level
	for( var l = 0; l < Levels.length; l++ ){
		;(function(i){
			Levels[l].onclick = function(){
				
				//Set data to object
				data.level = i;
				
				//Add active class
				Lib_.RemoveClass(Levels, "level-active");
				Lib_.ToogleClass(this, "level-active");
				
			};
		}(Levels[l].getAttribute("data-type")));
	};


	//Select map
	for( var l = 0; l < MapListItems.length; l++ ){
		;(function(i){
			MapListItems[l].onclick = function(){
				
				//Set data to object
				data.map = i;
				
				//Add active class
				Lib_.RemoveClass(MapListItems, "map-active");
				Lib_.ToogleClass(this, "map-active");
			};
		}(MapListItems[l].getAttribute("data-map")));
	};
	
	


	//Open maps list
	var getList = true;
	SetMapLink.onclick = function(){
		if( getList ){
			Lib_.AJAX().post("/maplist", function(data){
				  CreateMapsList(JSON.parse(data).data);
					getList = false;
			});
		};
	
		HideShowEl(MapList);
	};
};


