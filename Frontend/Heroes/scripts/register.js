window.onload = function(){
  
  //Get element by ID
  var getEl = function(id){ return document.getElementById(id); };
  
  var submit   = "submit_i",
  	  pass     = "password_i",
  	  pass_r   = "password_i_repeat",
  	  login    = "login_i",
  	  popup    = "ConfirmPopup",
  	  p_wrap   = "ConfirmWrapper",
  	  err      = "register_error",
	  reg_name = "Register-header";
  
  
  //Set localization
  ;(function(){
    Lib_.GetId(reg_name).innerHTML = Localization.register;
    Lib_.GetId(login).setAttribute("placeholder", Localization.login);
    Lib_.GetId(pass).setAttribute("placeholder", Localization.password);
	Lib_.GetId(pass_r).setAttribute("placeholder", Localization.rep_pass);
    Lib_.GetId(submit).setAttribute("value", Localization.create);
  }());
  
  
  getEl(submit).onclick = function(){
    if( getEl(pass).value === getEl(pass_r).value ){
        
	  Lib_.AJAX().post("/register/action", function(data){
        if( data === "200" ){
        	getEl(popup).innerHTML      = Lang_.user_created;
        	getEl(p_wrap).style.display = "block";
        	setTimeout(function(){
        		window.location = "/";
        	}, 1000);
        }else{
        	getEl(err).innerHTML = data;
        };
	  }, 'login=' + getEl(login).value + '&pass=' + getEl(pass).value);
        
    }else{
        getEl(err).innerHTML = Lang_.pass_n_eq;
    };
  };
  
};


