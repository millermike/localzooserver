/**
 * PLAYER CLASS
 * @param data Object of common player data
 */
Heroes.Objects.Player = (function(data){
  
  "use strict";

  var STEPPX            = 64,
      FRAMESLENGTH      = 7,
      PLAYERSPEED       = 150,
      TERRAINCOGNITARAD = 5,
      Sprite            = null,
	  PLAYER            = null;
  
  
  //Player predefined sprite positions
  var spritesPositions = {
    "bottom"      :{ "y": 446, "stop": { "x": 0, "y": 64 } },
    "top"         :{ "y": 384, "stop": { "x": 256, "y": 64 } },
    "left"        :{ "y": 512, "stop": { "x": 128, "y": 64 } },
    "right"       :{ "y": 0,   "stop": { "x": 384, "y": 64 } },
    "bottomRight" :{ "y": 320, "stop": { "x": 448, "y": 64 } },
    "bottomLeft"  :{ "y": 256, "stop": { "x": 190, "y": 64 } },
    "topRight"    :{ "y": 192, "stop": { "x": 320, "y": 64 } },
    "topLeft"     :{ "y": 128, "stop": { "x": 64, "y": 64 } }
  };
  
  var flagPosition = {
	"bottom"      : { "r": 3, "cls": "_90_hor" },
	"top"         : { "r": 37, "cls": "" },
	"left"        : { "r": 18, "cls": "_90_hor" },
	"right"       : { "r": 29, "cls": "" },
	"bottomLeft"  : { "r": 26, "cls": "_90_hor" },
	"bottomRight" : { "r": 25, "cls": "" },
	"topLeft"     : { "r": 14, "cls": "_90_hor" },
	"topRight"    : { "r": 36, "cls": "" },
  };
  
  //Current sprite position
  var currentSpritePosition = null;

  //Flag color
  var flag_ = null;
      
  /**
   * Player API
   */
  return {
    
    level: 1,

    experience: 0,

    visitedObjects: {},

	mana: data.mana,
		
    path: {
      steps: null,
      currentStep: null,
      event: null
    },
    
	startSteps: data.steps,
		
    moveValue: data.steps,
    
    //Player movement speed in ms
    speed: PLAYERSPEED,
    
    terraRadius: TERRAINCOGNITARAD,
    
    //Common player data
    data: data,


 	Draw: function(o){

		Sprite = new Heroes.Sprite({
			p: o.data,
			el: o.link,
			step: STEPPX,
			length: FRAMESLENGTH,
			d: DATA
		});

  		//Set player in start position 
  		Sprite.setStartPosition(spritesPositions.right.stop.x, spritesPositions.right.stop.y);

		//Set player color
		;(function(){
			flag_ = document.createElement("div");
			flag_.className = "flag_"+data.color;
			o.link.appendChild(flag_);
		}());
	},	


    /**
     * Player moving method
     */
    Go: function(CollisionObject, PLAYER, TERRAINCOGNITA, DATA, callback){


      //Reverse steps in array
      if( PLAYER.path.steps.length ) {
        PLAYER.path.steps.reverse(); 
      }else{
        DATA.PLAYER_MOVEMENT.move = true;
      };
      

      //Move the object
      var timer         = -this.speed,
          th            = this,
          toX           = null,
          toY           = null,
          prevStep      = null,
          steps         = PLAYER.path.steps,
          mW            = Heroes.MAP.size.minW,
          mH            = Heroes.MAP.size.minH,
          stepsLength   = steps.length > this.moveValue ? this.moveValue : steps.length;
      
      for( var i = 0; i < stepsLength; i++, this.moveValue--){
          timer += this.speed;

          ;(function(s, i){
            setTimeout(function(){
              
              if( null === prevStep ) prevStep = steps[i];
              
              //Save last current step
              this.currentStep = steps[i];
				  
              //Animate player
			  Sprite.animate();
			  
			  //Make cursor invisible
			  EVENTER.trigger("cursor.hide");
				  
              
              /**
               * Set animation sprite to current direction
               */
              if( i == 0 ) prevStep = steps[i].parent;

              //Animate to BOTTOM
              if( steps[i].y > prevStep.y && steps[i].x === prevStep.x ){
                Sprite.setPositionY(spritesPositions.bottom.y);
                currentSpritePosition = spritesPositions.bottom;
				flag_.className = "flag_"+data.color+flagPosition.bottom.cls;
				flag_.style.right = flagPosition.bottom.r+"px";
              }
              
              //Animate to TOP
              else if( steps[i].y < prevStep.y && steps[i].x === prevStep.x ){
                Sprite.setPositionY(spritesPositions.top.y);
                currentSpritePosition = spritesPositions.top;
				flag_.className = "flag_"+data.color+flagPosition.top.cls;
				flag_.style.right = flagPosition.top.r+"px";
              }
              
              //Animate to LEFT
              else if( steps[i].x < prevStep.x && steps[i].y === prevStep.y ){
                Sprite.setPositionY(spritesPositions.left.y);
                currentSpritePosition = spritesPositions.left;
				flag_.className = "flag_"+data.color+flagPosition.left.cls;
				flag_.style.right = flagPosition.left.r+"px";
              }
              
              //Animate to RIGHT
              else if( steps[i].x > prevStep.x && steps[i].y === prevStep.y ){
                Sprite.setPositionY(spritesPositions.right.y);
                currentSpritePosition = spritesPositions.right;
				flag_.className = "flag_"+data.color+flagPosition.right.cls;
				flag_.style.right = flagPosition.right.r+"px";
              }
              
              //Animate to BOTTOM RIGHT
              else if( steps[i].x > prevStep.x && steps[i].y > prevStep.y ){
                Sprite.setPositionY(spritesPositions.bottomRight.y);
                currentSpritePosition = spritesPositions.bottomRight;
				flag_.className = "flag_"+data.color+flagPosition.bottomRight.cls;
				flag_.style.right = flagPosition.bottomRight.r+"px";
              }
              
              //Animate to BOTTOM LEFT
              else if( steps[i].x < prevStep.x && steps[i].y > prevStep.y ){
                Sprite.setPositionY(spritesPositions.bottomLeft.y);
                currentSpritePosition = spritesPositions.bottomLeft;
				flag_.className = "flag_"+data.color+flagPosition.bottomLeft.cls;
				flag_.style.right = flagPosition.bottomLeft.r+"px";
              }
              
              //Animate to TOP RIGHT
              else if( steps[i].x > prevStep.x && steps[i].y < prevStep.y ){
                Sprite.setPositionY(spritesPositions.topRight.y);
                currentSpritePosition = spritesPositions.topRight;
				flag_.className = "flag_"+data.color+flagPosition.topRight.cls;
				flag_.style.right = flagPosition.topRight.r+"px";
              }
              
              //Animate to TOP LEFT
              else if( steps[i].x < prevStep.x && steps[i].y < prevStep.y ){
                Sprite.setPositionY(spritesPositions.topLeft.y);
                currentSpritePosition = spritesPositions.topLeft;
        				flag_.className = "flag_"+data.color+flagPosition.topLeft.cls;
        				flag_.style.right = flagPosition.topLeft.r+"px";
              };
              
              toX = steps[i].x* Heroes.MAP.size.minW - ((PLAYER.data.w - Heroes.MAP.size.minW) / 2);
              toY = steps[i].y* Heroes.MAP.size.minH - ((PLAYER.data.h - Heroes.MAP.size.minH) / 2);


              //Start animate player moving
              $(PLAYER.link).animate({
                "left": toX,
                "top": toY
              }, th.speed, 'linear', function(){
                
              //Redraw terra incognita
              if( DATA.INCOGNITA ) DATA.INCOGNITA.drawTerraCorners(s);
									
    				  //Generate map objects
    				  EVENTER.trigger("map.generate_objects");
               
              //Change player objects coordinates
              PLAYER.data.x = s.x * Heroes.MAP.size.minW;
              PLAYER.data.y = s.y * Heroes.MAP.size.minW;
				  
				  
          				//Redraw mini-map
          				Lib_.ExecuteLimiter(function(){
          					EVENTER.trigger("drawPlayers");
          					EVENTER.trigger("drawTerra");
          				},[], 500);
          									
          								
          				//Update user steps indicator
          				EVENTER.trigger("updateSteps", [--PLAYER.data.steps, PLAYER]);
									

                  //Remove previous steps
                  DATA.CACHE.get("Steps").removeChild(document.getElementById("s_"+s.y+"_"+s.x));


                  //Player can move again
                  if( i + 1 === stepsLength ){
                    
                    //Make cursor visible again
                    EVENTER.trigger("cursor.show");
					
                    DATA.PLAYER_MOVEMENT.move = true;
                    
                    Sprite.stop();
                    Sprite.setStartPosition(currentSpritePosition.stop.x, currentSpritePosition.stop.y);
                    EVENTER.trigger("terra.clear");

                    //Check detection
                    if( CollisionObject ){
                      if( s.x === ((CollisionObject.x + CollisionObject.data.mX) / mW) && s.y === ((CollisionObject.y + CollisionObject.data.mY) / mH) ){
                        if( callback ) callback();
                      };
                    };
                  };                
              });

              prevStep = steps[i];

            }, timer);        
          }(steps[i], i));
          
      };
    
    }
  };
});
