var Lib_ = (function(){
  
	var Execlock = 0;
	
  return{
		AJAX: function(){
			ajax = {};
			ajax.x = function(){try{return new ActiveXObject('Msxml2.XMLHTTP')}catch(e){try{return new ActiveXObject('Microsoft.XMLHTTP')}catch(e){return new XMLHttpRequest()}}};
			ajax.serialize = function(f){var g=function(n){return f.getElementsByTagName(n)};var nv=function(e){if(e.name)return encodeURIComponent(e.name)+'='+encodeURIComponent(e.value);else return ''};var i=collect(g('input'),function(i){if((i.type!='radio'&&i.type!='checkbox')||i.checked)return nv(i)});var s=collect(g('select'),nv);var t=collect(g('textarea'),nv);return i.concat(s).concat(t).join('&');};
			ajax.send = function(u,f,m,a){var x=ajax.x();x.open(m,u,true);x.onreadystatechange=function(){if(x.readyState==4)f(x.responseText)};if(m=='POST')x.setRequestHeader('Content-type','application/x-www-form-urlencoded');x.send(a)};
			ajax.get = function(url,func){ajax.send(url,func,'GET')};
			ajax.gets = function(url){var x=ajax.x();x.open('GET',url,false);x.send(null);return x.responseText};
			ajax.post = function(url,func,args){ajax.send(url,func,'POST',args)};
			ajax.update = function(url,elm){var e=$(elm);var f=function(r){e.innerHTML=r};ajax.get(url,f)};
			ajax.submit = function(url,elm,frm){var e=$(elm);var f=function(r){e.innerHTML=r};ajax.post(url,f,ajax.serialize(frm))};
			return ajax;
		},
		
		GetId: function(id){
			return document.getElementById(id);
		},
	  
	  GetIndex: function(child){
			var i = 0;
			while( (child = child.previousSibling) != null ) 
				i++;
			return i;
	  },
	  
	  ToogleClass: function(el, class_){
			if( el.className.indexOf(class_) === -1 ){
				el.className = el.className + " " + class_;
			}else{
				el.className = el.className.replace(" "+class_, "");
			};
	  },
	  
	  RemoveClass: function(els, class_){
		if( els.length ){
			for( var i = 0; i < els.length; i++ ){
				if( els[i].className.indexOf(class_) ){
					els[i].className = els[i].className.replace(" "+class_, "");
				};
			};
		}else{
			els.className = els.className.replace(" " + class_, "");
		};
	  },
	  
      ReplaceClass: function(node, r, cls) {
        node.className = node.className.replace(r, cls);
      },

      HasClass: function(node, cls) {
          return !!~node.className.indexOf(cls);
      },

	  AddClass: function(el, class_){
		if( el.className.indexOf(class_) === -1 ) el.className += " "+class_;
	  },
	  
	  ClearClass: function(el){
		el.removeAttribute("class");
	  },
		
    IsParentById: function(e, class_){
        var el_ = e;
        while( el_.parentNode ){
            el_ = el_.parentNode;
            if( el_.id === class_ ){
                return true;
            };
        };
    },

    ExecuteLimiter: function(fn, arg, time){
        if (!Execlock){
            Execlock = true;
            fn.apply(null, arg);
            timer = setTimeout(function(){
             Execlock = false;
            },time);
        };
    },

        getOffsetSum: function(elem, border) {
            var top = 0,
                left = 0;

            while(elem) {
                if( elem ) {
                    top += elem.offsetTop;
                    left += elem.offsetLeft;
                    if( border ) {
                        top -= parseInt(document.defaultView.getComputedStyle(elem,null).border.split(' ')[0]);
                        left -= parseInt(document.defaultView.getComputedStyle(elem,null).border.split(' ')[0]);
                    };
                }
                elem = elem.offsetParent;
            };

            return {
                top: top,
                left: left
            };
        }

  };
  
}());

