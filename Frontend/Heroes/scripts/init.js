
var DATA = {   
	EVENTS : new Heroes.Events(),
	TIMER  : new Heroes.Timer(60),
	CACHE  : new Heroes.Cache(),
	MAPMODEL : [],
	TERRAINCOGNITA : [],
	UNITS : {
	  PLAYERS:   {},
	  MOBS:      [],
	  STATIC:    [],
	  OBJECTS:   [],
	  BUILDINGS: [],
	  RESOURCES: []
	},
	PLAYER_COLOR: "red",
	PLAYER_ACTIVE: null,
	PLAYER_FOCUS: true,
	PLAYER_MOVEMENT: {
      move: true,
      moveStatus: true,
	},
	BUILD_ACTIVE: null,
	RESOURCES:{
		GOLD: 1200,
		TREE: 5,
		STONE: 6,
		CRYSTALS: 12,
		SERA: 4,
		GEMS: 10,
		HG: 2
	},
	POPUP:{},
	EV: {},
	GEOMETRY:{
		SCREEN_W: 0,
		SCREEN_H: 0
	},
	DATE: {
		DAY: 1,
  		WEEK: 1,
		MONTH: 1		
	}
};

var CONST = {
	NORMALPATHLENGTH:20,
	NORMALMANAVAL:40
};



//Game initialiation point
Heroes.init = (function(){

  "use strict";


  //On page load start call modules
  window.addEventListener("load", function(){

      EVENTER.on('load.finish', function() {

            //Create Objects Events
            DATA.O_EVENTS = new Heroes.ObjectEvent();

            //Work with cursors
            DATA.CURSOR = new Heroes.Cursor();
            
            //Create popups
            var PopUp = new Heroes.PopUp();
            DATA.POPUP.Common = PopUp.create({id: "common"});
            DATA.POPUP.Castle = PopUp.create({id: "castle"});
            DATA.POPUP.Small  = PopUp.create({id: "small"});
            DATA.POPUP.Info   = PopUp.create({id: "info"});
            DATA.POPUP.Hint   = PopUp.create({id: "hint"});
            DATA.POPUP.Battle = PopUp.create({id: "battle"});

            //Map moving
            DATA.MAPMOVING = new Heroes.MoveMap();

            //Generate map and objects
            DATA.MAPINSTANCE = new Heroes.MapGenerator();

            //Create Hint instance
            DATA.HINT = new Heroes.Hint();

            //Draw terra incognita
            DATA.INCOGNITA = new Heroes.TerraIncognita();
            DATA.INCOGNITA.drawTerraIncognita();

            /**
             * Game handler for click events
             */
            //TODO: Refactor this
            Heroes.MapEvents().Click.GetPoint(function(event, e){
              Heroes.MoveLogic(event, e);
            });

            //Create mini map object
            DATA.MINIMAP = new Heroes.MiniMap();

            /* Create resources panel */
            EVENTER.trigger("resources.init");

            //Window resize events caller
            DATA.RESIZE = new Heroes.Resize();

            //Sidebar instance
            DATA.SIDEBAR = new Heroes.Sidebar();

            //Set active hero
            DATA.MAPMOVING.SETTOACTIVE(DATA.PLAYER_ACTIVE);
    
           //Remove loader and paranja
           //setTimeout(function() {
               var paranja = DATA.CACHE.get('paranja');
               paranja.parentNode.removeChild(paranja); 
           //}, 1000);
      });
  
  }, false);

}());










