var Settings = function(){

	//Get element by ID
	var getEl = function(id){ return document.getElementById(id); };
	
	
	
	//Panel show/hide
	;(function(){
		var open  = false,
			panel = "SettingsPanel",
			arrow = "ArrowSlider";
			
		getEl(arrow).addEventListener("click", function(){
		    if( !open ){
		        open = true;
		        getEl(panel).style.top = "0px";
		    }else{
		        open = false;
		        getEl(panel).style.top = -getEl(panel).offsetHeight + "px";
		    };
		});
	}());
};
