Heroes.Buildings.HumanCastle = (function(){
  
  "use strict";

  //On object click event handler
  var onObjectClick = function(){ alert("Human Castle"); };
  
      
  /**
   * Object API
   */
  return {
    w     : 160,              	   //Width of an object on map plot
    h     : 160,              	   //Height of an object on map plot
    rw    : 168,              	   //Real width in px of object 
    rh    : 153,              	   //Real height in px of object
    mX    : 64,               	   //Margin of object ( detect click event) in X
    mY    : 128,              	   //Margin of object ( detect click event) in Y
	flags : {
		bottom :{ y: 127, x: 51},  //Describe castle bottom flag position
		top    :{ y: 5, x: 56}     //Describe castle top flag position
	},
    s      : Heroes.config.resources.buildings_["Castle"],      //Source to image for object
    mini   : Heroes.config.resources.buildings_["Castle_mini"],

	action : function(){
		DATA.POPUP.Castle.content(Heroes.Tmpl.Castle());
		DATA.POPUP.Castle.show(true);
    },
		
	openBuild: function(){
		DATA.POPUP.Castle.content(Heroes.Tmpl.Castle());
		DATA.POPUP.Castle.show(true);
	} 
  };
});

