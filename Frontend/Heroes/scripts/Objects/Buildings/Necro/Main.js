Heroes.Buildings.NecroCastle = (function(){
  
  "use strict";

  //On object click event handler
  var onObjectClick = function(){ alert("Necro Castle"); };
  
      
  /**
   * Object API
   */
  return {
    w  : 160,              																	//Width of an object on map plot
    h  : 192,              																	//Height of an object on map plot
    rw : 147,              																	//Real width in px of object 
    rh : 192,              																	//Real height in px of object
    mX : 64,               																	//Margin of object ( detect click event) in X
    mY : 160,              																	//Margin of object ( detect click event) in Y
		flags: {
			bottom:{ y: 173, x: 51},
			top:{ y: 3, x: 72}
		},
    s  : Heroes.config.resources.buildings_["Necro"],      //Source to image for object
    mini   : Heroes.config.resources.buildings_["Necro_mini"],
	action : function(){
      onObjectClick();
    },
		
		openBuild: function(){
			alert("open castle");
		} 
  };
});

