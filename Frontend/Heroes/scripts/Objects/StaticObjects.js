/** 
 * List od static objects
 * w  - objects width on game grid in px
 * h  - objects height on game grid in px
 * rw - objects real width( DOM element width )
 * rh - objects real height( DOM element height )
 * s  - path to image file
 * dx - left object margin on game grid in points( not px )
 * dy - top object margin on game grid in points( not px )
 */

Heroes.StaticObjectsList = {

  //Group of trees
	"TreeGroup01": {
		w  : 64, h  : 32,              
		rw : 47, rh : 47,                      
		s  : Heroes.config.resources.static_["T_S_1"],
		hint: Heroes.lang.en.Trees
	},
	"TreeGroup02": {
		w  : 64, h  : 64,              
		rw : 64, rh : 64,                      
		s  : Heroes.config.resources.static_["T_S_2"],
		hint: Heroes.lang.en.Trees
	},
	"TreeGroup03": {
		w  : 96, h  : 128, dx: 1,             
		rw : 116, rh : 128,                      
		s  : Heroes.config.resources.static_["T_B_1"],
		hint: Heroes.lang.en.Trees
	},
	"TreeGroup04": {
		w  : 96, h  : 96,            
		rw : 109, rh : 128,   
		px :0, py: 32,		
		s  : Heroes.config.resources.static_["T_B_2"],
		hint: Heroes.lang.en.Trees
	},
	"TreeGroup05": {
		w  : 128, h  : 96,            
		rw : 128, rh : 96,                      
		s  : Heroes.config.resources.static_["T_B_3"],
		hint: Heroes.lang.en.Trees
	},
	"TreeGroup06": {
		w  : 128, h  : 96,            
		rw : 128, rh : 96,                      
		s  : Heroes.config.resources.static_["T_B_4"],
		hint: Heroes.lang.en.Trees
	},
	"TreeGroup07": {
		w  : 96, h  :64,            
		rw : 128, rh : 96,        
		px : 32, py: 0,		
		s  : Heroes.config.resources.static_["T_B_5"],
		hint: Heroes.lang.en.Trees
	},
	"TreeGroup08": {
		w  : 128, h  : 64,            
		rw : 117, rh : 80,                      
		s  : Heroes.config.resources.static_["T_B_6"],
		hint: Heroes.lang.en.Trees
	},
	
	
	
	
	
	//Green landscape objects
	"Landscape01": {
		w  : 128, h  : 64,            
		rw : 128, rh : 64,                
		s  : Heroes.config.resources.static_["L_G_1"],
		hint: Heroes.lang.en.Ground	
	},
	"Landscape02": {
		w  : 64, h  : 64,            
		rw : 64, rh : 64,              
		s  : Heroes.config.resources.static_["L_G_2"],
		hint: Heroes.lang.en.Ground	
	},
	"Landscape03": {
		w  : 64, h  : 64,            
		rw : 64, rh : 64,              
		s  : Heroes.config.resources.static_["L_G_3"],
		hint: Heroes.lang.en.Ground	
	},
	"Landscape04": {
		w  : 64, h  : 32,            
		rw : 64, rh : 32,              
		s  : Heroes.config.resources.static_["L_G_4"],
		hint: Heroes.lang.en.Ground	
	},
	"Landscape05": {
		w  : 64, h  : 32,            
		rw : 64, rh : 32,              
		s  : Heroes.config.resources.static_["L_G_5"],
		hint: Heroes.lang.en.Ground	
	},
	"Landscape06": {
		w  : 64, h  : 32,            
		rw : 64, rh : 32,              
		s  : Heroes.config.resources.static_["L_G_6"],
		hint: Heroes.lang.en.Ground	
	},
	"Landscape07": {
		w  : 64, h  : 32,            
		rw : 64, rh : 32,              
		s  : Heroes.config.resources.static_["L_G_7"],
		hint: Heroes.lang.en.Ground	
	},
	
	
	
	
	//Plants objects
	"Plant_01": {
		w  : 32, h  : 32,            
		rw : 32, rh : 32,                
		s  : Heroes.config.resources.static_["PL_1"],
		hint: Heroes.lang.en.Plants	
	},
	"Plant_02": {
		w  : 32, h  : 32,            
		rw : 32, rh : 32,                
		s  : Heroes.config.resources.static_["PL_2"],
		hint: Heroes.lang.en.Plants		
	},
	"Plant_03": {
		w  : 32, h  : 64,            
		rw : 32, rh : 64,                
		s  : Heroes.config.resources.static_["PL_3"],
		hint: Heroes.lang.en.Plants		
	},
	"Plant_04": {
		w  : 32, h  : 32,            
		rw : 32, rh : 32,                
		s  : Heroes.config.resources.static_["PL_4"],
		hint: Heroes.lang.en.Plants		
	},
	"Plant_05": {
		w  : 32, h  : 32,            
		rw : 32, rh : 32,                
		s  : Heroes.config.resources.static_["PL_5"],
		hint: Heroes.lang.en.Plants		
	},
	"Plant_06": {
		w  : 32, h  : 32,            
		rw : 32, rh : 32,                
		s  : Heroes.config.resources.static_["PL_6"],
		hint: Heroes.lang.en.Plants		
	},
  
  
	//Pond objects
	"Pond_01": {
		w  : 160, h  : 96,            
		rw : 160, rh : 96,                
		s  : Heroes.config.resources.static_["POND_1"],
		hint: Heroes.lang.en.Pond	
	}
};















