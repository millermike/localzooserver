Heroes.Objects.Chest = function(){

  'use strict';


  /* CONST */
  var TMPL_SHOW_DELAY = 6000;


  var clickEvent = function(o){
	var e = DATA.CACHE.get(o.data["_id"]);
	
	//Remove node
	$(e).fadeOut(500, function(){
		e.parentNode.removeChild(e.nextSibling);
		e.parentNode.removeChild(e);
		EVENTER.trigger("map.remove_object", [DATA.UNITS.RESOURCES, o.data["_id"]]);

		//Remove from map grid
		var x = o.x / Heroes.MAP.size.minW;
		var y = o.y / Heroes.MAP.size.minH;
		DATA.MAPMODEL[y][x].v = -1;
	});


	DATA.POPUP.Common.content(Heroes.Tmpl.Chest(o));
	DATA.POPUP.Common.show();

	var blocks = DATA.CACHE.getClass(DATA.POPUP.Common.el, 'chest-get');
	for( var i = 0; i < blocks.length; i++ ){
		blocks[i].addEventListener('click', function(e){
			
			//Add active state
			for( var i = 0; i < blocks.length; i++ ){
				blocks[i].className = blocks[i].className.replace('chest_active', '');
			};
			e.target.className += ' chest_active';

		}, false);
	};


	/* Accept button handler */
	var btn =  DATA.CACHE.getClass(DATA.POPUP.Common.el, 'popup-info-button-accept', 0);
	btn.addEventListener('click', function(){ 
		var active = DATA.CACHE.getClass(DATA.POPUP.Common.el, 'chest_active', 0);

		if( active.className.indexOf('get-gold-icon') !== -1 ){
			DATA.RESOURCES['GOLD'] += o.amount.gold;
			EVENTER.trigger("resources.update", ['gold', o.amount.gold]);	
		}else{
			DATA.PLAYER_ACTIVE.experience += o.amount.exp;	
		};
		
		DATA.POPUP.Common.hide();
	}, false);
  };


  return {
    w   : 32,              //Width of an object on map plot
    h   : 32,              //Height of an object on map plot
    rw  : 33,              //Real width in px of object 
    rh  : 31,              //Real height in px of object
    mX  : 0,               //Margin of object ( detect click event) in X
    mY  : 0,               //Margin of object ( detect click event) in Y
    s   : Heroes.config.resources.res["CHEST"], //Source to image for object,
	hint : Heroes.Tmpl.Hint(Heroes.lang.en["ChestHint"]),
    action: function(o){
      clickEvent(o);
    }
  };
};
