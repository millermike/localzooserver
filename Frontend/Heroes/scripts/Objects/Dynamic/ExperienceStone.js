Heroes.Objects.ExperienceStone = (function(){
  
  "use strict";

  var exp = 1000;

  //On object click event handler
  var onObjectClick = function(o){ 
		if( !DATA.PLAYER_ACTIVE.visitedObjects[o.data._id] ){

      DATA.POPUP.Small.content(Heroes.Tmpl.ExperienceStone());
      DATA.POPUP.Small.show();

      //Incease Level and Hide popup
      DATA.CACHE.getClass(DATA.POPUP.Small.el, "popup-info-button-accept", 0).onclick = function(){
        DATA.PLAYER_ACTIVE.visitedObjects[o.data._id] = true;
        DATA.PLAYER_ACTIVE.experience += exp;
        DATA.POPUP.Small.hide();
      };
      
		}else{
			DATA.POPUP.Info.content(Heroes.Tmpl.ExperienceStoneVisited());
			DATA.POPUP.Info.show();
		};
  };
  
  
  /**
   * Object API
   */
  return {
    _id : new Date().getTime()+"_exp_stone",
    w  : 32,                                            //Width of an object on map plot
    h  : 64,                                            //Height of an object on map plot
    rw : 35,                                            //Real width in px of object 
    rh : 64,                                            //Real height in px of object
    mX : 0,                                             //Left margin of object ( for positioning active area of object )
    mY : 32,                                            //Top margin of object ( for positioning active area of object )
    s  : Heroes.config.resources.dynamic_["EXP_STONE"], //Source to image for object
		hint: Heroes.Tmpl.Hint(Heroes.lang.en.ExpStoneHint),
    action: function(o){
      onObjectClick(o);
    }
  };
});

Heroes.Objects.ExperienceStone.prototype = new Heroes.Objects.Abstract();
