/**
 * Game map resources class
 */
Heroes.Objects.Resources = function(type){

  'use strict';


  /* Resource object click event action */	
  var clickEvent = function(o){ 
	var e = DATA.CACHE.get(o.data["_id"]);
	
	//Remove node
	$(e).fadeOut(500, function(){
		e.parentNode.removeChild(e.nextSibling);
		e.parentNode.removeChild(e);
		EVENTER.trigger("map.remove_object", [DATA.UNITS.RESOURCES, o.data["_id"]]);

		//Remove from map grid
		var x = o.x / Heroes.MAP.size.minW;
		var y = o.y / Heroes.MAP.size.minH;
		DATA.MAPMODEL[y][x].v = -1;
	});


	//Update player resources
	DATA.RESOURCES[type.toUpperCase()] += o.amount;
	EVENTER.trigger("resources.update", [type, o.amount]);
  };



  /* Public */
  return {
	_id : new Date().getTime() + "_res_" + type,
    w   : 32,              //Width of an object on map plot
    h   : 32,              //Height of an object on map plot
    rw  : 32,              //Real width in px of object 
    rh  : 30,              //Real height in px of object
    mX  : 0,               //Margin of object ( detect click event) in X
    mY  : 0,               //Margin of object ( detect click event) in Y
    s   : Heroes.config.resources.res["RESOURCE_" + type.toUpperCase()], //Source to image for object,
	hint : Heroes.Tmpl.Hint(Heroes.lang.en["ResourceHint" + type]),
    action: function(o){
      clickEvent(o);
    }
  };
};
