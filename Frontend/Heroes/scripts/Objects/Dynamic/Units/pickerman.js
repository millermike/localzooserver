Heroes.Objects.UnitPickerman = function(){

	'use strict'

	var onObjectClick = function(o){};

	return {              
		w : 64,                                          
		h : 64,                                          
		rw : 47,                                          
		rh : 47,                                          
		mX : 0,                                          
		mY : 0,  

        name: 'pickerman',
        speed: 4,
        health: 10,
        attack: 4,
        shots: false,
        movement: 'ground',
        damage: [1,3],
        defence: 5,
        hexSize: 1,
        price: { gold: 60 }, 
		
        b_params: {
			w : 72,                                          
			h : 97,
            dx: -15,
            sprites: 6,    
            stay: { y: 1, x: 0 },
            move: { y: 0 },      
			sb: Heroes.config.resources.mobs_["PICKERMAN_BIG"]
		},	

		s : Heroes.config.resources.mobs_["PICKERMAN"],
		hint : Heroes.Tmpl.Hint(Heroes.lang.en.Pickerman),
		
		action : function (o) {
		  onObjectClick(o);
		},
	};
};

