Heroes.Objects.UnitCavalier = function(){

	'use strict'

	var onObjectClick = function(o){};

    //TODO: extra
    EVENTER.on('battle.move', function(e) {
        //Calculate each hex in movement and increase damage on 5%
    });

	return {              
		w : 64,                                          
		h : 64,                                          
		rw : 47,                                          
		rh : 47,                                          
		mX : 0,                                          
		mY : 0,  

        name: 'cavalier',
        speed: 8,
        health: 100,
        attack: 15,
        shots: false,
        movement: 'ground',
        damage: [15, 25],
        defence: 15,
        hexSize: 2,
        price: { gold: 1000 }, 
		
        b_params: {
			w : 116,                                          
			h : 116,
			dx: -22,
            sprites: 18,    
            stay: { y: 1, x: 0 },
            move: { y: 0 },      
            sb: Heroes.config.resources.mobs_["CAVALIER_BIG"]
		},	

		s : Heroes.config.resources.mobs_["CAVALIER"],
		hint : Heroes.Tmpl.Hint(Heroes.lang.en.Cavalier),
		
		action : function (o) {
		  onObjectClick(o);
		},
	};
};

