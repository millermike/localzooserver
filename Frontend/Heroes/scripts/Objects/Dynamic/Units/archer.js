Heroes.Objects.UnitArcher = function(){

	'use strict'

	var onObjectClick = function(o){};

	return {              
		w : 64,                                          
		h : 64,                                          
		rw : 47,                                          
		rh : 47,                                          
		mX : 0,                                          
		mY : 0,  

        name: 'archer',
        speed: 3,
        health: 10,
        attack: 6,
        shots: 12,
        movement: 'ground',
        damage: [2,3],
        defence: 3,
        hexSize: 1,
        price: { gold: 100 }, 
		
        b_params: {
			w : 48,                                          
			h : 79,
			sb: Heroes.config.resources.mobs_["ARCHER_BIG"]
		},	

		s : Heroes.config.resources.mobs_["ARCHER"],
		hint : Heroes.Tmpl.Hint(Heroes.lang.en.Archer),
		
		action : function (o) {
		  onObjectClick(o);
		},
	};
};

