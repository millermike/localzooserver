Heroes.Objects.UnitAngel = function(){

	'use strict';

	var onObjectClick = function(o){

		//Replace angel place on map by -1
		var x = o.x / Heroes.MAP.size.minW;
		var y = o.y / Heroes.MAP.size.minH;
		var w = o.data.w / Heroes.MAP.size.minW;
		var h = o.data.h / Heroes.MAP.size.minH;

		for( var i = 0; i < w; i++ ){
			for( var j = 0; j < h; j++ ){
				DATA.MAPMODEL[y + j][x + i].v = -1;	
			};
		};
	
		//Angel dom object
		var el = DATA.CACHE.get(o.data['_id']);
		el.parentNode.removeChild(el.nextSibling);
		el.parentNode.removeChild(el);	
 
		/* Open battle window */
		DATA.POPUP.Battle.content(Heroes.Tmpl.Battle());
		DATA.POPUP.Battle.show(true, true);

		/* Set battle units */
		EVENTER.trigger("battle.start", o);
	};

	
    return {
        w : 64,                                          
        h : 64,                                          
        rw : 47,                                          
        rh : 47,                                          
        mX : 0,                                          
        mY : 0,

        name: 'angel',
        speed: 25,
        health: 200,
        attack: 20,
        shots: false,
        movement: 'fly',
        damage: [50],
        defence: 20,
        hexSize: 1,
        price: { gold: 3000, ge: 1 }, 
        extra: {
            damage: ['devil', '150%']
        },
        
        b_params: {
            w : 106, //Physical size of sprite frame                                        
            h : 106,  
            dx: -34, //Dx margin to corrent center position of sprite,
            dy: 6,
            sb : Heroes.config.resources.mobs_["ANGEL_BIG"],
            sprites: 11,    
            stay: { y: 1, x: 0 },
            move: { y: 0 }        
        },
                                        
        s : Heroes.config.resources.mobs_["ANGEL"],
        hint : Heroes.Tmpl.Hint(Heroes.lang.en.Angel),
        
        action : function (o) { onObjectClick(o); }
    };
};
