Heroes.Objects.TreeLvlUp = (function(){
  
  "use strict";

	var crystals = 10;


  //On object click event handler
  var onObjectClick = function(o){ 
		if( !DATA.PLAYER_ACTIVE.visitedObjects[o.data._id] ){

			if( DATA.RESOURCES.CRYSTALS > crystals ){
		    	DATA.POPUP.Common.content(Heroes.Tmpl.LevelUpTree());
		    	DATA.POPUP.Common.show();
		    
		   		//Incease Level and Hide popup
		    	DATA.CACHE.getClass(DATA.POPUP.Common.el, "popup-info-button-accept", 0).onclick = function(){
		      		DATA.PLAYER_ACTIVE.visitedObjects[o.data._id] = true;
					DATA.PLAYER_ACTIVE.level++;
		      		DATA.RESOURCES.CRYSTALS -= crystals;
					EVENTER.trigger("resources.update", ['crystals']);
		      		DATA.POPUP.Common.hide();
		   		};
			}else{
				DATA.POPUP.Info.content(Heroes.Tmpl.LevelUpTreeLessCrystal());
				DATA.POPUP.Info.show();
			};

		}else{
			DATA.POPUP.Info.content(Heroes.Tmpl.LevelUpTreeVisited());
			DATA.POPUP.Info.show();
		};
  };
  
  
  /**
   * Object API
   */
  return {
	  _id : new Date().getTime()+"_tree_lvl",
    w  : 64,                                           //Width of an object on map plot
    h  : 96,                                           //Height of an object on map plot
    rw : 80,                                           //Real width in px of object 
    rh : 96,                                           //Real height in px of object
    mX : 32,                                           //Left margin of object ( for positioning active area of object )
    mY : 64,                                           //Top margin of object ( for positioning active area of object )
    s  : Heroes.config.resources.dynamic_["LVL_TREE"], //Source to image for object,
	hint : Heroes.Tmpl.Hint(Heroes.lang.en.TreeLvlUpHint),
    action: function(o){
      onObjectClick(o);
    }
  };
});

Heroes.Objects.TreeLvlUp.prototype = new Heroes.Objects.Abstract();
