Heroes.Objects.Portal = (function(){
  
  "use strict";

  //On object click event handler
  var onObjectClick = function(){ alert("PORTAL"); };
  
      
  /**
   * Object API
   */
  return {
    w  : 32,              //Width of an object on map plot
    h  : 64,              //Height of an object on map plot
    rw : 32,              //Real width in px of object 
    rh : 64,              //Real height in px of object
    mX : 0,               //Margin of object ( detect click event) in X
    mY : 32,              //Margin of object ( detect click event) in Y
    s  : "obj3",          //Source to image for object
    action: function(){
      onObjectClick();
    }
  };
});

Heroes.Objects.Portal.prototype = new Heroes.Objects.Abstract();