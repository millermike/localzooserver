Heroes.Objects.MineStone = function(){

  'use strict'


  var onObjectClick = function(o){

	// TO DO:
	// Think about frients mines
	if( o.color !== DATA.PLAYER_COLOR ){
  
		o.data.events.setFlag(DATA.PLAYER_COLOR, o);

		DATA.POPUP.Common.content(Heroes.Tmpl.MineWin({
			name: Heroes.lang.en.MineStone,
	   		text: Heroes.lang.en.MineStoneText,
			amount: 2,
			type: "stone"	
		}));
		DATA.POPUP.Common.show();

		var btn = DATA.CACHE.getClass(DATA.POPUP.Common.el, "popup-info-button-accept", 0);
		btn.addEventListener('click', function(){
			DATA.POPUP.Common.hide();
		}, false);
	}else{
		DATA.POPUP.Info.content(Heroes.Tmpl.MineWinYou());
		DATA.POPUP.Info.show();
	};
  };


  /**
   * PUBLIC
   */
  return {
	type: 'mine',
    inc: 2,
	w  : 96,                                           //Width of an object on map plot
    h  : 64,                                           //Height of an object on map plot
    rw : 96,                                          //Real width in px of object 
    rh : 64,                                           //Real height in px of object
    mX : 32,                                           //Left margin of object ( for positioning active area of object )
    mY : 32,                                           //Top margin of object ( for positioning active area of object )
	flag: {
		x: 61, y: 33
	},
    s  : Heroes.config.resources.dynamic_["MINE_STONE"], //Source to image for object,
	hint : Heroes.Tmpl.Hint(Heroes.lang.en.MineStone),
    action: function(o){
      onObjectClick(o);
    },
	events: Heroes.Objects.MineAbstract
  }
};
