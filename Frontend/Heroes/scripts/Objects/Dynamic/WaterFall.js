Heroes.Objects.Waterfall = (function(){
  
  "use strict";

  //On object click event handler
  var onObjectClick = function(){ alert("Waterfall"); };
  
  
  /**
   * Object API
   */
  return {
    w  : 64,              //Width of an object on map plot
    h  : 128,             //Height of an object on map plot
    rw : 96,              //Real width in px of object 
    rh : 128,             //Real height in px of object
    cw : 32,              //Width for calculation( for Gif fortmas )
    ch : 128,             //Height for calculation( for Gif fortmas )
    mX : 0,               //Left margin of object ( for positioning active area of object )
    mY : 96,              //Top margin of object ( for positioning active area of object )
    s  : "waterfall",     //Source to image for object
    action: function(){
      onObjectClick();
    }
  };
});

Heroes.Objects.Waterfall.prototype = new Heroes.Objects.Abstract();
