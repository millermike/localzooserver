var Animation = function(){
  
  //Create Timer for all animations
  if( !T ) var T = new Timer();

  /**
   * Get objects iterations
   * @param x1 Number - start x position
   * @param y1 Number - start y position
   * @param x2 Number - end x position
   * @param y2 Number - end x position
   * @return Object of x and y iterations
   */
  var CalcMove = function(x1, y1, x2, y2, speed){

    //Take distance to point
    var diffX = x2 - x1;
    var diffY = y2 - y1;

	//Get angle, hipotenuse and number of steps
    var angle = Math.atan2(diffX, diffY);
	var hipo = Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));
	var steps = (hipo / speed) << 0;
	
    return {
      vx: Math.sin(angle),
      vy: Math.cos(angle),
	  s: steps
    };
  };


  //Class API
  return{
    start: function(o){
      var x  = o.el.offsetLeft,
          y  = o.el.offsetTop,
          i  = CalcMove(x, y, o.toX, o.toY, o.speed),
		  id = new Date().getTime();
      
      T.start({
        name: id,
		steps: i.s,
        action: function(){
		  if( this.steps ){
            x += i.vx * o.speed;
            y += i.vy * o.speed;
			
            o.el.style.left = x << 0;
            o.el.style.top  = y << 0;
			
			this.steps--;
		  }else{
			return false;
		  };
        }
      });
      
      return {id: id};
    },
	
    stop: function(id){
      T.remove(id);
    }
  };
};
