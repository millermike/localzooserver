Heroes.Sprite = function(data){

  var timer = DATA.TIMER,
      posY  = 0,
      posX  = 0,
      stepsToReset = 20;

  
  return {
    animate: function(){
      timer.start({
        name: "playermove_"+data.p.unit,
        step: 0,
        frameWidth: data.length * data.p.w * stepsToReset,
        action: function(){
            if( this.step >= this.frameWidth) this.step = 0;
            this.step += data.step;
            data.el.style.backgroundPosition = this.step + "px "+ posY +"px";
        }
      });
      
    },
    
    stop: function(){
      data.el.style.backgroundPosition = "0px "+ posY +"px";
      timer.remove("playermove_"+data.p.unit);
      return posY;
    },
    
    setPositionY: function(y){
      posY = y;
    },
    
    setPositionX: function(x){
      posX = x;
    },
    
    setStartPosition: function(x, y){
      data.el.style.backgroundPosition = x + "px "+ y +"px";
    }
  };
};

