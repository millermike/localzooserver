/**
 * Preload all game images
 */
Heroes.Preload = (function(callback){
  var img, 
	  //url_prefix = "http://parikmaher.byethost18.com/",
	  url_prefix = "", 
      res     = Heroes.config.resources,
      resLn   = Object.keys(res).length,
      counter = 0;


  /* Count total images number */
  var totalLn = 0;
  for( var i in res ){
    for( var j in res[i] ){
    	totalLn++;
	};
  };


  var preload = document.getElementById("loader"); 
  var preload_str = document.getElementById("loader-string");
  var preload_g_str = document.getElementsByClassName("game-loader-string")[0];
  var perc_str = document.getElementById("loader-per");
  var preload_dark =  document.getElementById("loader-dark-screen");
  var step_per = 100 / totalLn;
  var total_per = 0;
	
  for( var i in res ){
    for( var j in res[i] ){
      img = new Image();
      img.src = url_prefix + res[i][j];
      img.onload = (function(){
        
		/* Set percents */
		total_per += step_per;
		if( total_per >= 99 ) total_per = 100;
		perc_str.innerHTML = total_per+"%";

		/* Set loader string length */
		preload_str.style.width = total_per+"%";

		counter++;

        if( counter === totalLn ) {
			callback();

			/* Small timeout for beatyfull showing*/
			setTimeout(function(){
				preload.parentNode.removeChild(preload);
				preload_dark.parentNode.removeChild(preload_dark)
			}, 500);
		};
      })(img, j);
    };
  };
});
