window.onload = function(){
  
  var el = document.getElementById("test");
  
  
  el.onclick = function(){
    
      new Animation({
        e: el,
        name: "mmm",
        time: 2,
        type: "linear",
        from: "left:10px; top:10px",
        to:   "left:200px; top:100px",
        frames: [
          "20% {left: 111px; top: 33px}",
          "40% {left: 411px; top: 33px}",
          "60% {left: 211px; top: 233px}",
          "80% {left: 911px; top: 333px}"
        ],
        start: function(){
          //alert(1);
        },
        end: function(){
          //alert(2);
        }
      }); 
      
  };
  
  document.getElementById("ttt").innerHTML = "alert(1)";
};







var Animation = (function(o){
  var style  = null,
      ident_ = null,
      data   = null,
      pref_  = ["webkit i", "Moz", "O", "ms i", ""], //Prefics name vs letter register
      type   = null;
  
  
  //Detect browser type
  for( var p = 0; p < pref_.length; p++ ){
    if( document.body.style[pref_[p].split(" ")[0]+"Transform"] === "" ) type = pref_[p];
  };
  
  
  //Remove animation styles
  ;(function(){
    var s = o.e.getElementsByTagName("style");
    for( var i = 0; i < s.length; i++ ){
      if( s[i].getAttribute("style-data") ) o.e.removeChild(s[i]);
    };
  }());

  
  //Create style block
  ;(function(){
    var prefData = type.split(" ");
    style = document.createElement("style");
    style.setAttribute("style-data", true);

    if( o.uniq ){
      ident_ = o.uniq;
    }else{
      if( o.e.id ) {
        ident_ = "#" + o.e.id;
      }else if( o.e.className ){
        ident_ = "." + o.e.className;
      }else{
        ident_ = o.e.nodeName.toLowerCase();
      };
    };
    
    //Add Start event for animation
    o.e.addEventListener(prefData[0]+"Animation"+(prefData[1] ? 'S' : 's')+"tart", function(){
     return o.start() || null;
    }, false);
    
    //Add End event for animation
    o.e.addEventListener(prefData[0]+"Animation"+(prefData[1] ? 'E' : 'e')+"nd", function(){
      if( !o.clear === false ) o.e.removeChild(style);
      return o.end() || null;
    }, false);
    
    //Add Iteration event for animation
    o.e.addEventListener(prefData[0]+"Animation"+(prefData[1] ? 'I' : 'i')+"teration", function(){
      return o.iteration() || null;
    }, false);
    
    
    //Config css data
    var frames = "";
    if( o.frames ){
      for( var f = 0; f < o.frames.length; f++ ){
        frames += o.frames[f];
      };
    };
    data = ident_+"{"+
             "-"+prefData[0].toLowerCase()+"-animation-name: "+o.name+";"+ 
             "-"+prefData[0].toLowerCase()+"-animation-duration: "+(o.time || 1)+"s;"+
             "-"+prefData[0].toLowerCase()+"-animation-timing-function: "+(o.type || 'linear')+";"+
             "-"+prefData[0].toLowerCase()+"-animation-fill-mode: both;"+
           "}"+
           "@-"+prefData[0].toLowerCase()+"-keyframes "+o.name+"{"+
             "from{"+
               o.from+
             "}"+
             frames+
             "to{"+
               o.to+
             "}"+
           "}";
         
    style.innerHTML = data;
    o.e.appendChild(style);
  }());
});