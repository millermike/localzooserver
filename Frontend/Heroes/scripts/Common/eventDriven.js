/**
 * Event class
 * @method listen - param: object, event name, function
 * @method trigger - param: object, event name, parameters( if you will not send objects event will trigger all listeners )
 */
Heroes.Events = function(){
  
  var listeners = {};
  
  return {
    listen: function(o, e, f){
      if( !listeners[e] ){
        listeners[e] = [f];
      }else{
        listeners[e].push(f);
      };
      
      if( !o._events_ ){
        o._events_ = {};
        o._events_[e] = [f];
      }else{
        if( !o._events_[e] ){
          o._events_[e] = [f];
        }else{
          o._events_[e].push(f);
        };
      };
    },
    
    trigger: function(o, e, param){
      if( o.constructor === String ){
        for( var all = 0; all < listeners[o].length; all++){
          listeners[o][all](param);
        };
      }else{
        if( o.constructor === Object ) o = [o];
        for( var ob = 0; ob < o.length; ob++ ){
          if(o[ob]._events_[e]){
            for( var i = 0; i < o[ob]._events_[e].length; i++ ){
              o[ob]._events_[e][i](param);
            };
          }else{
            console.log("No event");
          };
        };
      };
    }
  };
};
