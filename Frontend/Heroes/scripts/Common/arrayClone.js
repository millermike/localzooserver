/**
 * Array object clone
 * 
 * Example: var array = ArrayClone([....]);
 */

var ArrayClone = function(ext){
  
  var array = [];
  
  //Recursive loop
  this.loop = function(el, arr, prop){
    if( arr != null ){
      if( arr.constructor == Array ){
        var a = [];
        for( var i = 0; i < arr.length; i++ ){
          this.loop(a, arr[i]);
        };
        this.type(el, a, prop);
      }else if( arr.constructor == Object ){
        var b = {};
        for( var j in arr ){
          this.loop(b, arr[j], j);
        };
        this.type(el, b, prop);
      }else{
      this.type(el, arr, prop);
      };
    };
  };


  //Get object type
  this.type = function(pr, el, prop){
    if( pr.constructor == Array ){
      pr.push(el)
    }else if( pr.constructor == Object ){
      pr[prop] = el;
    };
  };


  //Start point
  for( var i = 0; i < ext.length; i++ ){
    this.loop(array, ext[i]);
  };
  
  return array;
};


