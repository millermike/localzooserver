/**
 * Popup Class
 */
Heroes.PopUp = (function(){
  
  "use strict";
	
	var bordersMargin = 0;
	var p             = null;
	var popups        = null;

	
	/* Create invisible block under popup */
	var inv = DATA.CACHE.get('invisible_block');
	if( !DATA.CACHE.get('invisible_block') ){
		inv = document.createElement('div');
		inv.id = 'invisible_block';
		document.body.appendChild(inv);
	};


	/* Dark screen */
	var dark = document.createElement('div');
	dark.id = 'dark_screen_layout';
	document.body.appendChild(dark);


    //Create Popup DOM element
	var createPopup = function(params){
		p           = document.createElement("div");
		p.innerHTML = "<div class='p_div popup_top'></div>"+
						"<div class='p_div popup_bottom'></div>"+
						"<div class='p_div popup_left'></div>"+
						"<div class='p_div popup_right'></div>"+
						
						"<div class='p_div popup_tp_right_corner'></div>"+
						"<div class='p_div popup_tp_left_corner'></div>"+
						"<div class='p_div popup_bt_right_corner'></div>"+
						"<div class='p_div popup_bt_left_corner'></div>"+
						
						"<div class='p_shadow'></div>"+
						"<div class='p_bg'></div>"+
						"<div class='popup_content'></div>";
								
		p.className = "popup";
		p.id        = "popup_"+params.id;
		document.body.appendChild(p);
		return p;
	};		
	
  
  var hide = function(){
		popups = DATA.CACHE.getClass(document, "popup");
		for( var i = 0; i < popups.length; i++ ){
			popups[i].style.display = "none";
		};
		inv.style.display = "none";
		dark.style.display = "none";
  };
  
  var show = function(p, h){
        p.style.display = "block";
		
		if( !h ){
			p.style.height = 0;
			p.style.height = (DATA.CACHE.getClass(p, "popup_content", 0).offsetHeight + bordersMargin)+"px";
		};

		inv.style.display = "block";
  };
	
  
   //Close window on keypress
	document.body.addEventListener("keyup", function(e){
		if( e.which === 27 ) hide();
	}, false);



  return {
		create: function(params){
				var p_ = createPopup(params);
				
				return {
					el: p_,
					
					show: function(hg, d){
						show(p_, hg);
						if( d ) dark.style.display = "block";
					},
					
					hide: function(d){
						hide();
						if( d ) dark.style.display = "none";
					},
					
					content: function(data){
						p_.getElementsByClassName("popup_content")[0].innerHTML = data;
					},
					
					position: function(x, y){
						p_.style.top = y;
						p_.style.left = x;
						
						if( p_.offsetTop + p_.offsetHeight > window.innerHeight ){
							p_.style.top = window.innerHeight - p_.offsetHeight;
						};
						
						if( p_.offsetLeft + p_.offsetWidth > window.innerWidth ){
							p_.style.left = window.innerWidth - p_.offsetWidth;
						};
					}
				};
		}
  };
});




