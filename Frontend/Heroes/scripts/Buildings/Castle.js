Heroes.Buildings.Castle = (function(){
  
  "use strict";

  //On object click event handler
  var onObjectClick = function(){ alert("Castle"); };
  
      
  /**
   * Object API
   */
  return {
    w  : 128,              //Width of an object on map plot
    h  : 160,              //Height of an object on map plot
    rw : 160,              //Real width in px of object 
    rh : 160,              //Real height in px of object
    mX : 64,               //Margin of object ( detect click event) in X
    mY : 128,              //Margin of object ( detect click event) in Y
    s  : "castle",         //Source to image for object
    action : function(){
      onObjectClick();
    }
  };
});

