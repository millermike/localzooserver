window.onload = function(){

	var NewGame   = Lib_.GetId('NewGame');
	var StepOne   = Lib_.GetId('MenuStepOne');
	var StepTwo   = Lib_.GetId('MenuStepTwo');
	var ExitTo    = Lib_.GetId('ExitStepTwo');
	var Single    = Lib_.GetId('SingleGame');
	var LoadGLink = Lib_.GetId('LoadGame');
	var LoadList  = Lib_.GetId('ListOfGames');
	
	
	//Single play flag
	var SinglePlay = false;
	
	
	//Hide load list
	var HideShowEl = function(e){
		if( window.getComputedStyle(e).display === 'none' ){
			e.style.display = 'block';
		}else{
			e.style.display = 'none';
		};
	};
	
	//Hide load list
	var HideEl = function(e){
		e.style.display = 'none';
	};
	
	
	
	//Show new game menu
	NewGame.onclick = function(){
		StepOne.style.display = 'none';
		StepTwo.style.display = 'block';
		SinglePlay = true;
		
		//Hide load list
		HideEl(LoadList);;
		
		//Exit second step
		ExitTo.onclick = function(){
			StepOne.style.display = 'block';
			StepTwo.style.display = 'none';
			SinglePlay = false;
		};
	};


	//Start game
	Single.onclick = function(){
		if( SinglePlay ){
			Lib_.AJAX().post("/play", function(data){
				if( JSON.parse(data).data && JSON.parse(data).data === 'ok'){
					window.location = "/game";
				};
			}, 'play=true');
		};
	};
	
	//Open list of saved games
	LoadGLink.onclick = function(){
		HideShowEl(LoadList);
	};
};


