/* Game login page */
window.onload = function(){
  
  var submit   = "submit_i",
      login    = "login_i",
      pass     = "password_i",
      err      = "login_error",
	  log_name = "Login-header",
	  reg_link = "reg_a";
      
      
  //Set localization
  ;(function(){
    Lib_.GetId(log_name).innerHTML = Localization.game_name;
    Lib_.GetId(login).setAttribute("placeholder", Localization.login);
    Lib_.GetId(pass).setAttribute("placeholder", Localization.password);
    Lib_.GetId(submit).setAttribute("value", Localization.submit);
    Lib_.GetId(reg_link).innerHTML = Localization.register;
  }());
      

  var SendData = function(){
	  Lib_.AJAX().post("/login", function(data){
		    var configs = JSON.parse(data);
		    configs.error ? Lib_.GetId(err).innerHTML = configs.error : window.location = "/";
	  }, 'login=' + Lib_.GetId(login).value + '&pass=' + Lib_.GetId(pass).value);
  };

  Lib_.GetId(submit).onclick = function(){
    SendData();
  };
  
  document.body.onkeypress = function(e){
	  if( e.keyCode === 13 ){
	  		SendData();
	  };
  };
};


