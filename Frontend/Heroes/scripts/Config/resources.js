{
  "resources":{
    "bg1": "images/Bg/tgrs001.png",
    "bg2": "images/Bg/tgrs022.png",
    "bg3": "images/Bg/tgrs023.png",
    "bg4": "images/Bg/tgrs021.png",
    "bg5": "images/Bg/tgrs020.png",
    "bg6": "images/Bg/tgrs030.png",
    "bg7": "images/Bg/tgrs010.png",
    "bg8": "images/Bg/tgrb000.png",
    "bg9": "images/Bg/tgrb001.png",
    "bg10": "images/Bg/tgrb002.png",
    "bg11": "images/Bg/tgrb003.png",
    "bg12": "images/Bg/tgrb004.png",
    "bg13": "images/Bg/tgrs011-r.png",
    "bg14": "images/Bg/tgrs001-r.png",
    "bg15": "images/Bg/tgrs020-b.png",
    "bg16": "images/Bg/tgrs021-b.png",
    "bg17": "images/Bg/tgrs001-r-b.png",
    "bg18": "images/Bg/tgrs001-l-b.png",
    
    "obj1"        : "images/Objects/AVXcrsd0.png",
    "obj2"        : "images/Objects/tree.png",
    "obj3"        : "images/Objects/AVXmn1y3.png",
    "waterfall"   : "images/Objects/waterfall.gif",
    "tree2"       : "images/Objects/tree2.png",
    
    "player_grey" : "images/Sprites/player_grey.png",
    "player_steps": "images/Sprites/steps.png",
    "player_terra": "images/Sprites/terraIncognita.png"
  },
  
  
  //Images of static objects
  "static":{
   "L_G_0": "images/Objects/STATIC/GR_L_1.png"
  }
}
