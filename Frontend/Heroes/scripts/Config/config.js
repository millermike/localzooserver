//Game namespace
var Heroes = {};
    
Heroes.Objects   = {};
Heroes.Buildings = {};
Heroes.lang      = {};

Heroes.config = {
  resources:{
    "common_": {
      "map": "images/Maps/MAP0.jpg",
      "bg1": "images/Bg/tgrs001.png",
      "bg2": "images/Bg/tgrs022.png",
      "bg3": "images/Bg/tgrs023.png",
      "bg4": "images/Bg/tgrs021.png",
      "bg5": "images/Bg/tgrs020.png",
      "bg6": "images/Bg/tgrs030.png",
      "bg7": "images/Bg/tgrs010.png",
      "bg8": "images/Bg/tgrb000.png",
      "bg9": "images/Bg/tgrb001.png",
      "bg10": "images/Bg/tgrb002.png",
      "bg11": "images/Bg/tgrb003.png",
      "bg12": "images/Bg/tgrb004.png",
      "bg13": "images/Bg/tgrs011-r.png",
      "bg14": "images/Bg/tgrs001-r.png",
      "bg15": "images/Bg/tgrs020-b.png",
      "bg16": "images/Bg/tgrs021-b.png",
      "bg17": "images/Bg/tgrs001-r-b.png",
      "bg18": "images/Bg/tgrs001-l-b.png",

    },
    
    //Images of static objects
    "static_": {
       "L_G_1": "images/Objects/STATIC/GR_L_1.png",
	   "L_G_2": "images/Objects/STATIC/GR_L_2.png",
	   "L_G_3": "images/Objects/STATIC/GR_L_3.png",
	   "L_G_4": "images/Objects/STATIC/GR_L_4.png",
	   "L_G_5": "images/Objects/STATIC/GR_L_5.png",
	   "L_G_6": "images/Objects/STATIC/GR_L_6.png",
	   "L_G_7": "images/Objects/STATIC/GR_L_7.png",
	   
       "T_S_1": "images/Objects/STATIC/TREE_S_1.png",
	   "T_S_2": "images/Objects/STATIC/TREE_S_2.png",
	   "T_B_1": "images/Objects/STATIC/TREE_B_1.png",
	   "T_B_2": "images/Objects/STATIC/TREE_B_2.png",
	   "T_B_3": "images/Objects/STATIC/TREE_B_3.png",
	   "T_B_4": "images/Objects/STATIC/TREE_B_4.png",
	   "T_B_5": "images/Objects/STATIC/TREE_B_5.png",
	   "T_B_6": "images/Objects/STATIC/TREE_B_6.png",
	   
	   "PL_1": "images/Objects/STATIC/PLANT_1.png",
	   "PL_2": "images/Objects/STATIC/PLANT_2.png",
	   "PL_3": "images/Objects/STATIC/PLANT_3.png",
       "PL_4": "images/Objects/STATIC/PLANT_4.png",
       "PL_5": "images/Objects/STATIC/PLANT_5.png",
       "PL_6": "images/Objects/STATIC/PLANT_6.png",
     
       "POND_1": "images/Objects/STATIC/POND_B_1.png"
    },
    
    //Dynamic objects images
    "dynamic_": {
      'LVL_TREE': "images/Objects/DYNAMIC/LVL_TREE.png",
      'EXP_STONE': "images/Objects/DYNAMIC/KnowlegeUp.gif",
	  'MINE_GOLD': "images/Objects/MINES/gold.png",
	  'MINE_STONE': "images/Objects/MINES/stone.png",
	  'MINE_TREE': "images/Objects/MINES/tree.png",
	  'MINE_HG': "images/Objects/MINES/hg.png",
	  'MINE_SULFUR': "images/Objects/MINES/sulfur.png",
	  'MINE_GEMS': "images/Objects/MINES/gems.png",
	  'MINE_CRYSTAL': "images/Objects/MINES/crystal.png",
    },
    
    //Buildings
    "buildings_": {
    	"Castle": "images/Objects/CASTLES/Castle/Main.png",
		"Castle_mini": "images/Objects/CASTLES/Castle/mini.png",
		"Necro": "images/Objects/CASTLES/Necro/Main.png",
		"Necro_mini": "images/Objects/CASTLES/Necro/mini.png"
    },
    
    //Players sprites
    "players_": {
      "player_steps": "images/Sprites/steps.png",
      "player_terra": "images/Sprites/terraIncognita.png",
      "grey": "images/Sprites/player_grey.png"
    },

	//Map resources sprites
	"res": {
		"RESOURCE_TREE": "images/Objects/RESOURCES/tree.png",
		"RESOURCE_STONE": "images/Objects/RESOURCES/stone.png",
		"RESOURCE_GEMS": "images/Objects/RESOURCES/gems.gif",
		"RESOURCE_GOLD": "images/Objects/RESOURCES/gold.gif",
		"RESOURCE_CRYSTAL": "images/Objects/RESOURCES/crystal.gif",
		"RESOURCE_SULFUR": "images/Objects/RESOURCES/sulfur.png",
		"CHEST": "images/Objects/RESOURCES/chest.gif"
	},

	'mobs_': {
		'ANGEL' : 'images/Objects/UNITS/Angel/map_mini.gif',
		'ANGEL_BIG' : 'images/Objects/UNITS/Angel/angel.png',
		'PICKERMAN_BIG' : 'images/Objects/UNITS/Pickerman/pickerman.png',
		'ARCHER_BIG' : 'images/Objects/UNITS/Archer/archer.png',
		'CAVALIER_BIG' : 'images/Objects/UNITS/Cavalier/cavalier.png'
	},

    'battle': {
        'player_red': 'images/Objects/BATTLES/HEROES/HERO_red.png',
        'bg1': 'images/Objects/BATTLES/BG/green_1.png',
        'cells': 'images/Objects/BATTLES/BG/cells.png',
        'cells_fill': 'images/Objects/BATTLES/BG/bg_cell_color.png'
    },

	//Interface textures
	"interface":{
		"uniq_bg": "images/uniq_background.png",
		"popup_shadow": "images/Popup/shadow.png",
		"popup_p_stats": "images/Popup/heroes/stats.png",
		"popup_r_border": "images/Popup/right_border.png",
		"popup_l_border": "images/Popup/left_border.png",
		"popup_b_border": "images/Popup/bottom_border.png",
		"popup_t_border": "images/Popup/top_border.png",
		"corner_b_r": "images/color-corner-bottom-right.png",
		"corner_b_l": "images/color-corner-bottom-left.png",
		"corner_t_r": "images/color-corner-top-right.png",
		"corner_t_l": "images/color-corner-top-left.png",
		"border_right": "images/border-right.png",
		"border_left": "images/border-left.png",
		"border_bottom": "images/border-bottom.png",
		"border_top": "images/border-top.png",
		"arrow_down": "images/arrow_down.png",
		"mini_map": "images/Common/mini_map.png",
		"sidebar": "images/Common/player_info_back.png",
		"arrow_up": "images/Common/arrow_up.png",
		"arrow_up_min": "images/Common/arrow_up_min.png"
	},

    "cursor": {
        "go": "images/Cursors/go.cur",
        "common": "images/Cursors/common.cur",
        "build": "images/Cursors/Building.cur",
        "goto": "images/Cursors/goto.cur",
        "hero": "images/Cursors/hero.cur"
    }
  }
};




