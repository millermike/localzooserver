Heroes.lang.en = {
	"Ok"           : "Ok",
	"TreeLevelUp"  : "You can increase you level. You just need pay 10 crystals. Are you really want it?",
  	"LevelPlus"    : "+1 Level",
  	"ExpStone"     : "Gods in a good mood today. Stone of experience just presented you 1000 of experience points.",
	"ExpStoneHint" : "Stone of experience",
	"TreeLvlUpHint": "Tree of knowlege",
	"Terra"        : "Terra incognita",
	"Trees"        : "Trees",
	"Ground"       : "Landscape",
  	"Plants"       : "Plants",
  	"Pond"         : "Pond",
	"Map"          : "You can dig here",
	
	"ResourceHinttree"   : "Wood",
	"ResourceHintsulfur" : "Sulfur",
	"ResourceHintstone"  : "Stone",
	"ResourceHintgold"   : "Gold",
	"ResourceHintcrystal": "Crystal",
	"ResourceHintgems"    : "Gems",
	"ResourceHinthg"     : "Mercury",

	"MineGold" : "Gold Mine",
	"MineStone" : "Ore Pit",
	"MineSulfur" : "Sulfur Mounds",
	"MineGems" : "Gem Pond",
	"MineCrystal" : "Crystal Mine",
	"MineTree" : "Sawmill",
	"MineHg" : "Alchemist Lab",

	"MineGoldText": "You took over the gold mine. It will supply the 1000 units has a gold every day",
	"MineCrystalText": "You took over the crystal mine. It will supply the 2 units has a crystal every day",
	"MineStoneText": "You took over the ore pit. It will supply the 2 units has a stone every day",
	"MineSulfurText": "You took over the sulfur mounds. It will supply the 2 units has a sulfur every day",
	"MineGemsText": "You took over the gem pond. It will supply the 2 units has a gems every day",
	"MineTreeText": "You took over the sawmill. It will supply the 2 units has a tree every day",
	"MineHgText": "You took over the alchemist lab. It will supply the 2 units has a mercury every day",

	"ChestHint": "Treasure chest",

	'Angel': 'Angels',
    'Pickerman': 'Pickerman',
    'Archer': 'Archer'
};
