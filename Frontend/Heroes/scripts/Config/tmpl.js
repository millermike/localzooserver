Heroes.Tmpl = {
  
	"Hint" : function(data){
		return "<div>"+data+"</div>";
	},
	
	"HintBuild": function(data){
		return "<table cellspace='0' cellpadding='0' class='build_hint_table'>"+
							"<tr>"+
								"<td rowspan='2' class='build_thumb_citadel'></td>"+
								"<td class='build_name'>"+data.name+"</td>"+
							"</tr>"+
							"<tr>"+
								"<td class='build_icons'>"+
									"<div class='castle_type'></div>"+
									"<div class='castle_build_level'></div>"+
									"<div class='castle_money'><span>1000</span></div>"+
								"</td>"+
							"</tr>"+
					 "</table>"+
					 "<table cellspace='0' cellpadding='0' class='build_hint_table'>"+
							"<tr>"+
								"<td class='build_first_row'>"+
									"<div class='build_side_left side_empty'></div>"+
									"<div class='build_col col_empty'></div>"+
									"<div class='build_col col_empty'></div>"+
									"<div class='build_col col_empty'></div>"+
									"<div class='build_side_right side_empty'></div>"+
								"</td>"+
							"</tr>"+
					 "</table>"+
					 "<table cellspace='0' cellpadding='0' class='build_hint_table build_hint_second_row'>"+
							"<tr>"+
								"<td class='build_second_row'>"+
									"<div class='build_col col_empty'></div>"+
									"<div class='build_col col_empty'></div>"+
									"<div class='build_col col_empty'></div>"+
									"<div class='build_col col_empty'></div>"+
								"</td>"+
							"</tr>"+
					 "</table>";
	},
	
	"HeroeTab": function(data, steps, mana, active){
		return  '<div class="sidebar-player-tab '+active+'" id="hero_'+data.data.name+'">'+
							'<div class="sidebar-player-tab-steps">'+steps+'</div>'+
							'<div class="sidebar-player-tab-icon" style="background-image:url(images/HEROES/'+data.data.type+'/'+data.data.name+'.png)"></div>'+
							'<div class="sidebar-player-tab-mana">'+mana+'</div>'+
						'</div>';
	},
  "HintHero":function(hero){
    return '<table class="hero-hint-table" cellpadding="0" cellspacing="0" width="170" style="margin:-10px -16px -10px -10px;">'+
       '<tr>'+
          '<td width="170" height="64">'+
            '<table width="170" cellpadding="0" cellspacing="0">'+
       '<tr>'+
                '<td rowspan="3" width="58" height="64">'+
                  '<img border="0"width="58" height="64" style="display:block;" src="images/HEROES/castle/'+hero.name+'.png" />'+ 
                '</td>'+
                '<td height="20" >'+'<div class="hero_hint_text">'+hero.name+'</div>'+'</td>'+ 
              '</tr>'+

              '<tr>'+
          '<td width="112" height="28">'+
            '<img border="0"width="112" height="28" class="hero-hint-stats-img" src="images/Popup/heroes/stats.png">'+
          '</td>'+
        '</tr>'+
        '<tr>'+
          '<td width="112" height="17">'+
            '<table cellpadding="0" cellspasing="0" width="112" class="hero-hint-stats-table">'+
              '<tr>'+
                '<td width="1"></td>'+
                '<td width="25" height="17">'+
                  '<img border="0"width="25" height="17" style="display: block;" src="images/Popup/heroes/cell.png"/>'+
                '</td>'+
                 '<td width="2">'+
                '</td>'+
                '<td width="25" height="17">'+
                  '<img border="0"width="25" height="17" style="display: block;" src="images/Popup/heroes/cell.png"/>'+
                '</td>'+
                '<td width="3">'+
                '</td>'+
                '<td width="25" height="17">'+
                  '<img border="0"width="25" height="17" style="display: block;" src="images/Popup/heroes/cell.png"/>'+
                '</td>'+
                '<td width="3">'+
                '</td>'+
                '<td width="25" height="17">'+
                  '<img border="0"width="25" height="17" style="display: block;" src="images/Popup/heroes/cell.png"/>'+
                '</td>'+
                '<td width="3">'+
                '</td>'+
              '</tr>'+
            '</table>'+
          '</td>'+
        '</tr>'+
        
      '</table>'+
    '</td>'+
  '</tr> '+
  
  '<tr>'+
    '<td width="170" height="3"></td>'+
  '</tr>'+
  <!-- first row with army -->
  '<tr>'+
    '<td width="170" height="36">'+
      '<table cellspacing="0"cellpadding="0" width="170">'+
        '<tr>'+
          '<td width="4" height="36" rowspan="2"></td>'+
          '<td width="25" height="36">'+
            '<table width="25" height="36" cellpadding="0" cellspacing="0">'+
              '<tr>'+
                '<td width="25" height="17">'+
                  '<img width="25" height="17" class="hero-hint-lider" border="0" src="images/Popup/heroes/hero-info-stats-moral.png" />'+
                '</td>'+
              '</tr>'+
              '<tr>'+
                '<td width="25" height="19">'+
                  '<img width="25" height="19" class="hero-hint-moral" border="0" src="images/Popup/heroes/hero-info-stats-hz.png" />'+
                '</td>'+
              '</tr>'+
            '</table>'+
          '</td>'+
          '<td width="6" height="36"></td>'+
          '<td width="36" height="36">'+
            '<img width="36" height="36" border="0" style="display:block;" src="images/icons/units/mini/army-icon-empty.png" />'+
          '</td>'+
          
          '<td width="36" height="36">'+
            '<img width="36" height="36" border="0" style="display:block;" src="images/icons/units/mini/castle/army-icon-pikineur.png" />'+
         '</td>'+
          
         '<td width="36" height="36">'+
            '<img width="36" height="36" border="0" style="display:block;" src="images/icons/units/mini/army-icon-empty.png" />'+
         '</td>'+
          
          '<td width="30" height="36">'+
            '<table width="30" height="36" cellpadding="0" cellspacing="0">'+
'              <tr>'+
'                <td width="30" height="20">'+
'                  <img width="30" height="20" border="0" style="display: block" src="images/Popup/heroes/stats-knolwege(not-sure).png">'+
'                </td>'+
'              </tr>'+
'              <tr>'+
'                 <td width="30" height="16">'+
'                  <img width="30" height="16" border="0" style="display: block" src="images/Popup/heroes/stats-knolwege(not-sure)-btm.png">'+
'                </td>'+
'              </tr>'+
'            </table>'+
'          </td>'+
'        </tr>'+
'      </table>'+
'    </td>'+
'  </tr>'+
  <!--row for display army amount-->
  '<tr>'+
    '<td width="170" height="11">'+
 '     <table width="170" height="11" cellpadding="0" cellspacing="0">'+
        '<tr>'+
          '<td width="32" height="11"></td>'+
'          <td width="36" height="11"><div class="hero-stat-first" style="width:100%;text-align: center"></div></td>'+
'          <td width="36" height="11"><div class="hero-stat-second" style="width:100%;text-align: center"></div></td>'+
'          <td width="36" height="11"><div class="hero-stat-trird" style="width:100%;text-align: center"></div></td>'+
'          <td width="30" height="11"></td>'+
'        </tr>'+
'      </table>'+
'    </td>'+
'  </tr>'+
  <!--second army row-->
'  <tr>'+
'    <td width="170" height="36">'+
'      <table width="170" cellpadding="0" cellspacing="0">'+
'        <tr>'+
'          <td width="14" height="36"></td>'+
'          <td width="36" height="36">'+
'              <img width="36" height="36" border="0" style="display:block;" src="images/icons/units/mini/army-icon-empty.png" />'+
'          </td>'+
'          <td width="36" height="36">'+
'              <img width="36" height="36" border="0" style="display:block;" src="images/icons/units/mini/army-icon-empty.png" />'+
'          </td>'+
'          <td width="36" height="36">'+
'              <img width="36" height="36" border="0" style="display:block;" src="images/icons/units/mini/army-icon-empty.png" />'+
'          </td>'+
'          <td width="36" height="36">'+
'              <img width="36" height="36" border="0" style="display:block;" src="images/icons/units/mini/army-icon-empty.png" />'+
'          </td>'+
'          <td width="12" height="36"></td>'+
'        </tr>'+
'      </table>'+
'    </td>'+
'  </tr>'+
  <!--row for display second army amount-->
  '<tr>'+
'    <td width="170" height="11">'+
'      <table width="170" cellpadding="0" cellspacing="0">'+
'        <tr>'+
'          <td width="14" height="11"></td>'+
'          <td width="36" height="11">'+
'            <div class="hero-stat-fourth" style="width:100%;text-align: center"></div>'+
'          </td>'+
'          <td width="36" height="11">'+
'            <div class="hero-stat-fifth" style="width:100%;text-align: center"></div>'+
'          </td>'+
'          <td width="36" height="11">'+
'            <div class="hero-stat-sixth" style="width:100%;text-align: center"></div>'+
 '         </td>'+
'          <td width="36" height="11">'+
'            <div class="hero-stat-seventh" style="width:100%;text-align: center"></div>'+
'          </td>'+
'          <td width="12" height="11"></td>'+
'        </tr>'+
'      </table>'+
'    </td>'+
'  </tr>'+
'</table>';
            
  },
	
  "Castle": function(data){
	  return "<div>CASTLE STRUCTURE</div>";
  },
	
  "LevelUpTree": function(){
	  return "<div class='popup-info-header'>Tree of Knowledge</div>"+
			 "<div class='clr'></div>"+
			 "<div class='popup-info-main-txt'>Upon your approach, the tree opens its eyes in delight. 'Ahh, an adventurer! Allow me to teach you a little of what I have learned over the ages.'</div>"+
			 "<div class='clr'></div>"+
			 "<div class='popup-info-icon knowlege-tree-icon'><span>+1 Level</span></div>"+
			 "<div class='clr'></div>"+
			 "<div class='popup-info-button-accept'></div>";
  },

  "MineWin": function(data){
	  return "<div class='popup-info-header'>"+ data.name +"</div>"+
			 "<div class='clr'></div>"+
			 "<div class='popup-info-main-txt'>"+ data.text +"</div>"+
			 "<div class='clr'></div>"+
			 "<div class='popup-info-icon mine-img "+ data.type +"-big-icon'><span>"+ data.amount +" / day</span></div>"+
			 "<div class='clr'></div>"+
			 "<div class='popup-info-button-accept'></div>";
  },

  "MineWinYou": function(){
  	return "<div class='you-mine'>It already belongs to you</div>";
  },
               
  "LevelUpTreeLessCrystal": function(){
	  return "<div class='crystal_not_enough'>You have not enough crystals.</div>";
  },

  "LevelUpTreeVisited" : function(){
	  return "<div class='crystal_visited'>You are already visited this place.</div>";
  },
  
  "ExperienceStone" : function(){
	  return "<div class='popup-info-main-txt'>"+Heroes.lang.en.ExpStone+"</div>"+
			 "<div class='clr'></div>"+
			 "<div class='popup-info-button-accept'></div>";
  },
                             
  "ExperienceStoneVisited" : function(){
	  return "<div class='exp_stone_visited'>Stone tell you NO.</div>";
  },

  "GetResource": function(data){
	  return "<div class='sidebar-resource-get'>"+
		  		"<div class='sidebar-resource-name-get'>You find a small quantity of "+ data.name +"</div>"+
				"<img src='images/Objects/RESOURCES/"+ data.name +"-big.png' />"+
				"<div>"+ data.amount +"<div>"+
		  	 "</div>";
  },

  "Chest": function(){
	  return "<div class='popup-info-header'>Casket</div>"+
			 "<div class='clr'></div>"+
			 "<div class='popup-info-main-txt'>Searching the neighborhood you will find a hidden treasure. You can keep it, or give the peasants in exchange for experience. Which would you choose?</div>"+
			 "<div class='clr'></div>"+
			 "<div class='get-gold-icon chest-get'><div>500</div></div>"+
			 "<div class='get-chest-or'>or</div>"+
			 "<div class='get-experience-icon chest-get chest_active'><div>1000</div></div>"+
			 "<div class='clr'></div>"+
			 "<div class='popup-info-button-accept'></div>";
  },


  /* Battle template */
  "Battle": function () {
  	return "<div class='game-battle' id='battle-dom'></div>"+
           "<div class='game-battle-units' id='battle-units-dom'></div>"+
           "<div class='game-battle-grid' id='battle-grid'>";
  }
};
