//app.js must be at one level with css file
//Example: node app.js -in:styles.css -out:out.css

var fs = require('fs');

var IN, OUT;

//Get params
process.argv.forEach(function(arg) {
    if( arg.indexOf('-in:') !== -1 ) {
        IN = arg.split('-in:')[1];
    } else if( arg.indexOf('-out:') !== -1 ) {
        OUT = arg.split('-out:')[1];
    };
});

fs.readFile(IN, 'utf-8', function(err, chunk) {
    if( !err ) {
        var background, cssPrefix;
       
        while( background = chunk.match(/background-image\:\s{0,}\w+\((?!data:image)(\S+)(\.\w+)\)/) ) {
            var path,
                dirSplit = background[1].split('../'),
                dirUp = dirSplit.length - 1,
                file = dirSplit.pop();

            console.log(background[1] + background[2])
        
            try {
                var base64 = fs.readFileSync(background[1] + background[2], 'base64');
                chunk = chunk.replace(''+ background[0], 'background-image: url(data:image/'+background[2].substr(1, background[2].length)+';base64,'+base64 +')');
            }
            catch(e) {
                console.log('Can\'t find image. Error:', e);
                chunk = chunk.replace(''+ background[0], '');
            }
        };
       
        fs.writeFile(OUT, chunk, function(err) {
            if( err ) console.log('Can\'t update css file'); 
        });
    }
    else {
        console.log('Can\'t read file');
    }
});
