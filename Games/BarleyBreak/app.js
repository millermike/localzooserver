/* Game API */
var actions = {
    init: function(data) {
        return {
            status: 200
        };
    }
};


/* Default message handler */
process.on('message', function(data) {
    var methodStatus, o;
   
    if( actions[data.method] ) {
        methodStatus = actions[data.method](data.params);
    
        if( methodStatus ) {
            o = methodStatus;
        } else {
            o = {status: false};
        }
    } else {
        o = {status: false};
    }
    
    process.send(JSON.stringify(o));
});



