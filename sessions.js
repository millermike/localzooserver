var common = require('./common/base.js');

var SESSION_FIELD = 'zoosession';

/* Create session for auth user */
exports.create = function(req, res, user, callback) {
    var status,
        hash = common.md5(new Date().getTime().toString() + Math.random() * 1000);

    res.setHeader('Set-Cookie', SESSION_FIELD+ '=' +hash);

    g.COLLECTIONS['session'].insert({id: hash, auth: 1, user: user._id}, function(err, doc) {
        if( !err ) {
            status = true;
            console.log(g.color.green('Temp session was created'));
        } else {
            status = false;
            console.log(g.color.red('Temp session ca\'t be created'));
        }
        callback(status);
    });
}

/* Handle requests and check session. Create temp session */
exports.handler = function(req, res, callback) {
    var hash,
        sessionD = g.Q.defer(),
        cookie = req.headers.cookie;

    req._cookie = cookie ? parseCookie(cookie) : {};

    if( !req._cookie[SESSION_FIELD] ) {

        hash = common.md5(new Date().getTime().toString() + Math.random() * 1000);

        res.setHeader('Set-Cookie', SESSION_FIELD+ '=' +hash);

        g.COLLECTIONS['session'].insert({id: hash, temp: 1}, function(err, doc) {
            if( !err ) {
                console.log(g.color.green('Temp session was created'))
            }
            else{
                console.log(g.color.red('Temp session ca\'t be created'))
            }
            sessionD.resolve(null);
        });
    } else {
        g.COLLECTIONS['session'].findOne({id: req._cookie[SESSION_FIELD]}, function(err, doc) {
            if( !err ) {
                sessionD.resolve(doc);
            } else {
                sessionD.resolve(null);
            }
        });
    } 
        
    sessionD.promise.then(function(data) {
        callback(req, res, data);
    });
}

/* Cookie parser 
 *
 * @params {String} cookie Cookies string
 * @return {Object} Cookies object
 *
 * */
function parseCookie(cookie) {
    var cookieItem,
        cookieObject = {},
        arr = cookie.split(';');

    arr.forEach(function(item) {
        cookieItem = item.split('=');
        cookieObject[cookieItem[0].trim()] = cookieItem[1];
    });

    return cookieObject;
}
