var query = require('querystring'),
    url = require('url'),
    fs = require('fs'),
    ejs = require('ejs');

var INDEXFILE = 'index.html',
    EXPIRETIME = 259200;

var readFile = function(name, type) {
    var fileDef = g.Q.defer();

    fs.readFile(name, type, function(err, chunk) {
        if( err ) {
            console.log(g.color.red('Can\'t read '+ name +' file'));
            fileDef.resolve(null);
        } else {
            fileDef.resolve(type === 'binary' ? chunk : chunk.toString());
        }
    });

    return fileDef.promise;
}


exports.init = function(req, res) {
    var controller,
        status = 200,
        paramsDefer = g.Q.defer();

    /* PARSE PARAMS */
    if( req.method === 'POST' ) {
        req.on('data', function(chunk) {
            req.params = query.parse(chunk.toString());
        })
        .on('end', function(e) {
            paramsDefer.resolve();
        });
    } else if( req.method === 'GET' ) {
        req.params = url.parse(req.url, true);
        paramsDefer.resolve();
    };

    /* MAKE ROUTINGS */
    paramsDefer.promise.then(function() {
        
        //Always get page by controller
        if( req.url.indexOf('.html') !== -1 ) {
            req.url = req.url.split('.html')[0];
        };

        //Cut get params
        if( req.url.indexOf('?') !== -1 ) {
            req.url = /(.+)(\?.+)/g.exec(req.url)[1];
        }

        controller = g.Controllers[req.url.substr(1)];
        
        if( controller ) {
            g.sessions.handler(req, res, function(req, res, session) {
                req.controller = {
                    action: req.url.substr(1) 
                };
                controller.init(req, res, session);
            });           
        } else {
            if( req.url === '/' ) {
                g.sessions.handler(req, res, function(req, res, session) {
                    req.controller = {
                        action: 'index' 
                    };
                    g.Controllers['index'].init(req, res, session);
                }) 
            } else {
                g.sessions.handler(req, res, function(req, res, session) {
                    
                    //Files types filter
                    if( !/\.css|js|html|ttf|woff|jpg|gif|ico|jpeg|png|manifest|cur|ogg|mp3|wav|vimrc/.test(req.url) ) {
                        req.url += '.html';
                    }

                    var ignoreStatus = false;
                    g.IGNORE.forEach(function(item) {
                        if( req.url.indexOf(item) !== -1 ) {
                            ignoreStatus = true; 
                        }
                    });
                    
                    if( !ignoreStatus ) {
                        readFile(g.FRONTEND_PATH + req.url, 'binary').then(function(data) {
                            if( data ){
                                if( /\.js/.test(req.url) ) {
                                    res.setHeader('Content-Type', 'application/x-javascript');
                                }
                                else if( /\.ogg|\.mp3|\.wav/.test(req.url) ) {
                                    res.setHeader('Accept-Ranges', 'bytes');
                                }
                                else if( /\.css/.test(req.url) ) {
                                    res.setHeader('Content-Type', 'text/css');
                                }

                                res.setHeader('Content-Length', data.toString().length);
                               
                                //TODO: переделать на версионное кеширование 
                                var currentTime = new Date().getTime(),
                                    maxTime = 2592000000;
                                if( !res.getHeader('Cache-Control') ) { 
                                    res.setHeader('Expires', new Date(currentTime + maxTime).toGMTString());
                                    res.setHeader('Cache-Control', 'max-age='+ maxTime +', must-revalidate');
                                };
                                
                                res.statusCode = status;  
                                res.end(data, 'binary');
                            } else {
                                res.statusCode = 404;
                                res.end();
                            }
                        }).done(); 
                    } else {
                        res.statusCode = 404;
                        res.end();
                    }
                });
            };

        }
    });

}
