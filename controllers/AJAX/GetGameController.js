exports.init = function(req, res) {

    g.COLLECTIONS['games'].findOne({name: req.params.name}, function(err, doc) {
        if( err || !doc ) {
            res.statusCode = 200;
            data = JSON.stringify({ "error": "No game." });
        } else {
            res.statusCode = 200;
            data = JSON.stringify({ "game": doc });
        }
        res.end(data);
    });

}
