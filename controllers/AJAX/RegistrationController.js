var common = require('../../common/base.js');

exports.init = function(req, res) {
    var p = req.params
        isError = false,
        errorMessage = '',
        registerD = g.Q.defer();

    if( p.login && p.password && p.repassword ) {

        //Check on correct symbols
        if( !/\W/.test(p.login) ) {

            //Is same login already exist
            g.COLLECTIONS['profiles'].findOne({login: p.login}, function(err, doc) {
                if( !err ) {
                    if( !doc ) {

                        if( p.password == p.repassword ) {
                            if( !/\W/.test(p.password) && !/\W/.test(p.repassword) ) {

                                //Check captcha
                                g.COLLECTIONS['session'].findOne({id: req._cookie.zoosession}, function(err, doc) {
                                    if( doc.regcaptcha.toString() ==  p.captcha ) { 
                                        g.COLLECTIONS['profiles'].insert({
                                            login: p.login,
                                            password: common.md5(p.password)
                                        }, function(err, doc) {
                                            if( err ) {
                                                isError = true;
                                                errorMessage = 'Profile creating error';
                                            }
                                            registerD.resolve();    
                                        });    
                                    } else {
                                        isError = true;
                                        errorMessage = 'Captcha data is incorrect';
                                        registerD.resolve();    
                                    }
                                });

                            } else {
                                isError = true;
                                errorMessage = 'Incorrect symbols in password';
                                registerD.resolve();
                            }
                        } else {
                            isError = true;
                            errorMessage = 'Password error';
                            registerD.resolve();
                        }

                    } else {
                        isError = true;
                        errorMessage = 'User already exist';
                        registerD.resolve();
                    }
                } else {
                    isError = true;  
                    errorMessage = 'User creating error'; 
                    registerD.resolve(); 
                }
            });            
        } else {
            isError = true;
            errorMessage = 'Incorrect symbols';
            registerD.resolve();
        } 


    } else {
        isError = true;
        errorMessage = 'Please fill all fields';
        rgisterD.resolve();
    }

    registerD.promise.then(function() {
        var result = {};
        
        res.statusCode = 200;
        
        if( !isError ) {
            result.status = '200';
        } else {
            result.status = 'error';
            result.data = errorMessage;
        };

        res.end(JSON.stringify(result));
    }); 
}
