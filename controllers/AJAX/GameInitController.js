var cp = require('child_process');

var START_SCRIPT_NAME = 'app.js';

exports.init = function(req, res) {
   
    res.writeHead(200);
   
    /* Create game process */
    g.CHILD_PROCESSES[req.params.gameName] = cp.fork(g.GAMES_PATH + '/' + req.params.gameName + '/' + START_SCRIPT_NAME);    

    /* Call game init method */
    g.CHILD_PROCESSES[req.params.gameName].send({method: 'init'});
    g.CHILD_PROCESSES[req.params.gameName].on('message', function(data) {
        res.end(JSON.stringify(data));
    });
}
