/** Client request example
 *  data: 'gameName=BarleyBreak&method=finish&params={"status": "ok"}'
 */

exports.init = function(req, res) {

    /* Game answer to us */
    g.CHILD_PROCESSES[req.params.gameName].on('message', function(data) {
        res.writeHead(200);
        res.end(data);
    });

    /* Send data to game process */
    g.CHILD_PROCESSES[req.params.gameName].send(req.params);
}

