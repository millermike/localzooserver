var captcha = require('../../common/captcha');

exports.init = function(req, res) {
    var c = captcha.generate(6),
        answer = c.concat([]).sort(function(a, b) {return a-b});

    g.COLLECTIONS['session'].update({id: req._cookie.zoosession}, {$set:{ regcaptcha: answer} }, function(err, doc) {
        res.statusCode = 200;
        
        if( !err ) {
            res.end(JSON.stringify({
                status: 200,
                data: c
            }));
        } else {
            res.end(JSON.stringify({
                status: 'error'
            }));    
        }
    });

}
