var common = require('../../common/base.js'),
    session = require('../../sessions');

exports.init = function(req, res) {
    var isError = false,
        loginD = g.Q.defer();

    if( req.params.login ) {
        g.COLLECTIONS['profiles'].findOne({login: req.params.login}, function(err, doc) {
            if( err || !doc ) {
                isError = true;
                loginD.resolve();
            } else {
                if( req.params.pass ) {
                    if( common.md5(req.params.pass) === doc.password ) {
                        session.create(req, res, doc, function(status) {
                            if( !status ) isError = true;
                            loginD.resolve(); 
                        });
                    } else {
                        isError = true;
                    }
                } else {
                    isError = true;
                }

                loginD.resolve();
            }
        });    
    } else {
        isError = true;
    }

    loginD.promise.then(function() {
        var result = {};
        
        res.statusCode = 200;
        
        if( !isError ) {
            result.status = '200';
        } else {
            result.status = 'error';
            result.data = 'Login error';
        };

        res.end(JSON.stringify(result));
    }); 
}
