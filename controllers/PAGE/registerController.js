var fs = require('fs'),
    ejs = require('ejs'),
    base = require('../../common/base.js');

exports.init = function(req, res, session) {
    base.readFile(g.FRONTEND_PATH + '/register.html', 'utf-8').then(function(data) {
        var o = ejs.render(data, {
            data: session
        });
        res.setHeader('Content-Type', 'text-plain');
        res.end(o, 'utf-8');
    })
}
