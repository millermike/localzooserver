var fs = require('fs'),
    ejs = require('ejs'),
    base = require('../../common/base.js');

exports.init = function(req, res, session) {
    base.readFile(g.FRONTEND_PATH + '/index.html', 'utf-8').then(function(data) {
        var view;

        //Get user data
        if( session && session.auth ) {
            g.COLLECTIONS['profiles'].findOne({_id: session.user}, function(err, doc) {
                if( err ) {
                    console.log('Can\'t get user info');
                } else {
                    view = ejs.render(data, {
                        data: {
                            user: {
                                name: doc.login      
                            }      
                        }
                    });
                    res.setHeader('Content-Type', 'text-plain');
                    res.end(view, 'utf-8');
                } 
            });
        } else {
            view = ejs.render(data, {
                data: {}
            });
            res.setHeader('Content-Type', 'text-plain');
            res.end(view, 'utf-8');
        }
    })
}
