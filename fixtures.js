var fs = require('fs');


exports.load = function() {
    var fixtureDefer = g.Q.defer();

    if( ISFIXTURES ) {
        
            //Load games list fixtures
            fs.readFile('./fixtures/GamesList.txt', 'utf-8', function(err, data) {
                if( err ) {
                    fixtureDefer.reject(err);
                    console.log(g.color.red('Can\'t read GamesList.txt textures file'));
                } else {
                    
                    //Fill collection by data
                    var collectionsStr = data.split('===='),
                        games = [];
                    
                    collectionsStr.forEach(function(item) {
                        games.push(JSON.parse(item));
                    });

                    g.COLLECTIONS['games'].insert(games, function(err, data) {
                        if( err ) {
                            console.log(g.color.red('Games was added with errors'));
                        } else {
                            console.log(
                                g.color.yellow('Fixtures:'),
                                g.color.green('Games was successfully added to list.')
                            );
                            fixtureDefer.resolve();
                        }
                    }); 
                }
            });

    } else {
        fixtureDefer.resolve();
    }

    return fixtureDefer.promise;
}
