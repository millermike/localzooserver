exports.load = function() {
    var d = g.Q.defer(),
        sessionD = g.Q.defer(),
        gamesD = g.Q.defer(),
        profileD = g.Q.defer(),

        dropSessionD = g.Q.defer(),
        dropGamesD = g.Q.defer(),
        dropProfilesD = g.Q.defer();

    g.Db.createCollection('sessions', function(err, col) {
        g.COLLECTIONS['session'] = col;
        sessionD.resolve();
    });

    g.Db.createCollection('games', function(err, col) {
        g.COLLECTIONS['games'] = col,
        gamesD.resolve();
    });

    g.Db.createCollection('profiles', function(err, col) {
        g.COLLECTIONS['profiles'] = col,
        profileD.resolve();
    });
   
    if( ISDROPABLE ) {
        g.Db.dropCollection('sessions', function() {
            dropSessionD.resolve();
        });
        g.Db.dropCollection('games', function() {
            dropGamesD.resolve();
        });
        g.Db.dropCollection('profiles', function() {
            dropProfilesD.resolve();
        });
    } else {
        dropSessionD.resolve();
        dropGamesD.resolve();
        dropProfilesD.resolve();
    }

    g.Q.allSettled([
            sessionD.promise, 
            gamesD.promise, 
            profileD.promise,
            dropSessionD.promise,
            dropGamesD.promise,
            dropProfilesD.promise
    ]).then(function() {
        console.log(g.color.yellow('Collections:'), g.color.green(' was generated'))
        d.resolve();
    });

    return d.promise;
}
