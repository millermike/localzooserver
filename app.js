var http = require('http'),
    mongo = require('mongodb'),
    routes = require('./routes'),
    fs = require('fs'),
    fixtures = require('./fixtures'),
    collections = require('./collections');

//Globals namespace
g = {};

//Global modules
g.Q = require('q');
g.sessions = require('./sessions');
g.color = require('colors');

//Global variables
g.Db = null;
g.Controllers = {};
g.Server = null;
g.PATH = __dirname;
g.CHILD_PROCESSES = {};
g.FRONTEND_PATH = g.PATH + '/Frontend';
g.GAMES_PATH = g.PATH + '/Games';
g.COLLECTIONS = {};

//Ignore list
g.IGNORE = [
    '/BarleyBreak/index.html'
];

//CONST
PORT = 8088,
HOST = '0.0.0.0';
MONGOHOST = 'localhost';
MONGOPORT = 27017;
PAGES_DIR = './pages';
CONTROLLERS_AJAX_DIR = './Controllers/AJAX';
CONTROLLERS_PAGE_DIR = './Controllers/PAGE';
CONTROLLER_NAME_POSTFIX = 'Controller.js';
DATABASENAME = 'zoogames';
ISFIXTURES = process.argv.indexOf('-fixtures') !== -1 ? true : false;
ISDROPABLE = process.argv.indexOf('-drop') !== -1 ? true : false;


/* Create and connect to database */
var dbDefer = g.Q.defer();
g.Db = new mongo.Db(DATABASENAME, new mongo.Server(MONGOHOST, MONGOPORT));
g.Db.open(function(err, db) {
    if( err ) {
        dbDefer.reject(err);
        console.log(g.colors.red('Database connect error'));
    } else {
        g.Db = db;
        console.log(g.color.green('Database was successfully created'));
        dbDefer.resolve();
    }
});


/* Init all AJAX controllers */
var controllersAjaxDefer = g.Q.defer();
fs.readdir(CONTROLLERS_AJAX_DIR, function(err, items) {
    if( err ) {
        console.log(g.color.red('Error while reading contollers files'));
        controllersAjaxDefer.reject(err);
    } else {
        items.forEach(function(item) {
            var name = item.split(CONTROLLER_NAME_POSTFIX).shift();
            g.Controllers[name] = require(CONTROLLERS_AJAX_DIR + '/' + item);
            console.log(g.color.green('Ajax Controller "'+ item +'" was created.'));
            controllersAjaxDefer.resolve();
        });
    }
});


/* Init all PAGES controllers*/
var controllersPageDefer = g.Q.defer();
fs.readdir(CONTROLLERS_PAGE_DIR, function(err, items) {
    if( err ) {
        console.log(g.color.red('Error while reading pages files'));
        controllersPageDefer.reject(err);
    } else {
        items.forEach(function(item) {
            var name = item.split(CONTROLLER_NAME_POSTFIX).shift();
            g.Controllers[name] = require(CONTROLLERS_PAGE_DIR + '/' + item);
            console.log(g.color.green('Page "'+ item +'" was created.'));
            controllersPageDefer.resolve();
        });
    }
});


/* Start server */
g.Q.allSettled([dbDefer.promise, controllersAjaxDefer.promise, controllersPageDefer.promise]).then(function() {
    
    collections.load()
        .then(function() {
            return fixtures.load()
        })
        .then(function() {
            g.Server = http.createServer(function(req, res) {
                routes.init(req, res);
            });

            g.Server.listen(PORT, HOST);

            console.log(g.color.green('Server was started.'))
        });
});



